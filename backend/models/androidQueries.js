const async = require("async");
const Pool = require("pg").Pool;

const moment = require("moment-timezone");
const jakartaDateTime = moment().tz("Asia/Jakarta");
const formattedJakartaDateTime = jakartaDateTime.format("YYYY-MM-DD HH:mm:ss");

const fs = require("fs");
const fastcsv = require("fast-csv");
const { date } = require("joi");

const pool = new Pool({
  user: process.env.DEV_DATABASE_USERNAME_CANTEEN,
  host: process.env.DEV_DATABASE_HOST_CANTEEN,
  database: process.env.DEV_DATABASE_NAME_CANTEEN,
  password: process.env.DEV_DATABASE_PASSWORD_CANTEEN,
  port: process.env.DEV_DATABASE_PORT_CANTEEN,
});

// Dashboard

async function tap(param) {
  try {
    let paramValue;
    let paramQuery;

    if (param.rfid.length === 10) {
      paramValue = param.rfid;
      paramQuery = `
      SELECT 
        mu.muse_id, 
        mu.muse_user_nik, 
        mu.muty_type_id, 
        mu.muse_rfid, 
        mu.muse_name, 
        mu.muse_created_at, 
        mu.muse_updated_at, 
        mu.muse_isactive, 
        mu.muse_store, 
        mcd.mcdb_flag_source 
      FROM 
        public.mst_user mu
      JOIN 
        mst_canteen_db mcd
      ON 
        mu.muse_store = mcd.mcdb_store_code
      WHERE 
        mu.muse_rfid = $1`;
    } else {
      paramValue = param.rfid;
      paramQuery = `  
      SELECT 
        mu.muse_id, 
        mu.muse_user_nik, 
        mu.muty_type_id, 
        mu.muse_rfid, 
        mu.muse_name, 
        mu.muse_created_at, 
        mu.muse_updated_at, 
        mu.muse_isactive, 
        mu.muse_store, 
        mcd.mcdb_flag_source 
      FROM 
        public.mst_user mu
      JOIN 
        mst_canteen_db mcd
      ON 
        mu.muse_store = mcd.mcdb_store_code
      WHERE 
        mu.muse_user_nik = $1`;
    }
    var data = await pool.query(paramQuery, [paramValue]);
    console.log("data user = ", data.rows);
    // console.log("data = ", data);
    if (data.rows.length > 0) {
      var check_param = await f_check_hour(data.rows[0].muse_rfid);
      if (check_param <= 0) {
        var insertQuery = `
        INSERT INTO public.trx_meal_orders
        (muse_id, muse_rfid, tmor_store_code, tmor_order_date, tmor_order_datetime, tmor_created_at, tmor_updated_at)
        SELECT $1, $2, mcdb_store_code, NOW(), NOW(), NOW(), null
        FROM mst_canteen_db
        WHERE mcdb_flag_source = $3
        returning tmor_order_id;
        `;

        var values = [
          data.rows[0].muse_id,
          data.rows[0].muse_rfid,
          param.store,
        ];

        try {
          // await pool.query(insertQuery, values);
          var IdData = await pool.query(insertQuery, values);

          const museName = data.rows[0].muse_name.trim();
          const museNik = data.rows[0].muse_user_nik;
          const orderId = IdData.rows[0].tmor_order_id;

          const outputResponse = {
            status: true,
            message: "Welcome, " + museName + "!",
            data: {
              museName: museName,
              tmor_order_datetime: formattedJakartaDateTime,
              museNik: museNik,
              orderId: orderId,
            },
          };
          console.log("Output Response: ", outputResponse);
          console.log("order id ", IdData);
          return outputResponse;
        } catch (error) {
          console.error("Error inserting data:", error);
          throw new Error("Failed to insert data into trx_meal_orders");
        }
      } else {
        var outputResponse = {
          status: false,
          data: data.rows,
          message: "Already tapped for today!",
        };
        return outputResponse;
      }
    } else {
      var outputResponse = {
        status: false,
        data: data.rows,
        message: "The employee is not registered.",
      };
      return outputResponse;
    }
  } catch (e) {
    var outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function syncData(param) {
  try {
    // Check ke user kalo ada data yang usernya masuk di transaksi tapi ga ada data tabel nya disini //
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function store() {
  try {
    const data = await pool.query(
      `select mcdb_store_code, mcdb_flag_source from mst_canteen_db;`
    );
    console.log(data.rows);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// FUNCTION LIST //

async function f_check_hour(param) {
  console.log(param);
  try {
    const validQuery = await pool.query(`
    select 
      mu.muse_id,
      tmo.muse_rfid,
      tmo.tmor_order_date,
      tmo.tmor_order_datetime
    from mst_user mu
    join trx_meal_orders tmo
    on mu.muse_id = tmo.muse_id
    where 
      tmo.muse_rfid = '${param}'
      and tmor_order_date = CURRENT_DATE 
      and tmor_order_datetime >= CURRENT_TIMESTAMP - interval '8 hours';
    `);

    const validateOrders = validQuery.rowCount;
    return validateOrders;
  } catch (error) {
    console.error("Error validating data:", error);
    return false;
  }
}

async function addPrintLog() {
  try {
    const insertQuery = `
    insert into trx_print_log (
      tplo_user_id,
      tmor_order_id,
      tplo_print_status,
      tplo_print_sequence,
      tplo_store,
      tplo_created_at,
      tplo_updated_at
      )
    select 
      tmo.muse_id,
      tmo.tmor_order_id,
      true,
      count(tmo.muse_id) as print_sequence,
      tmo.tmor_store_code,
      tmo.tmor_created_at,
      tmo.tmor_updated_at
    from trx_meal_orders tmo
    where tmo.tmor_order_date = current_date
    group by tmo.muse_id, tmo.tmor_order_id, tmo.tmor_store_code, tmo.tmor_created_at, tmo.tmor_updated_at;
  `;

    try {
      await pool.query(insertQuery);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

module.exports = {
  tap,
  store,
  addPrintLog,
};
