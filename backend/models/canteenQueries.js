const async = require("async");
const Pool = require("pg").Pool;

const moment = require("moment-timezone");
const jakartaDateTime = moment().tz("Asia/Jakarta");
const formattedJakartaDateTime = jakartaDateTime.format("YYYY-MM-DD HH:mm:ss");

const fs = require("fs");
const fastcsv = require("fast-csv");
const { date } = require("joi");

const pool = new Pool({
  user: process.env.DEV_DATABASE_USERNAME_CANTEEN,
  host: process.env.DEV_DATABASE_HOST_CANTEEN,
  database: process.env.DEV_DATABASE_NAME_CANTEEN,
  password: process.env.DEV_DATABASE_PASSWORD_CANTEEN,
  port: process.env.DEV_DATABASE_PORT_CANTEEN,
});

// USER TYPE

async function showUserType() {
  try {
    const data = await pool.query(`select * from mst_user_type;`);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function addUserType(param) {
  try {
    const checkQuery = `
      SELECT muty_type_id
      FROM mst_user_type
      WHERE muty_type_name = $1
    `;

    const checkResult = await pool.query(checkQuery, [param.muty_type_name]);

    if (checkResult.rows.length > 0) {
      const outputResponse = {
        status: false,
        message:
          "User Type " + "( " + param.muty_type_name + " )" + " already exists",
      };
      return outputResponse;
    }

    const insertQuery = `
      INSERT INTO mst_user_type(
        muty_type_name,
        muty_created_at,
        muty_updated_at
      )
      VALUES($1, NOW(), NOW())
    `;

    const values = [param.muty_type_name];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
      throw new Error("Failed to insert data into mst_user_type");
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function deleteUserType(id) {
  try {
    const data = await pool.query(
      `delete from mst_user_type where muty_type_id = '${id}';`
    );
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function updateUserType(id, name) {
  try {
    // Cek apakah entri dengan id yang diberikan ada di dalam tabel
    const checkQuery = `
      SELECT *
      FROM mst_user_type
      WHERE muty_type_id = $1
    `;
    const checkResult = await pool.query(checkQuery, [id]);

    if (checkResult.rows.length === 0) {
      const outputResponse = {
        status: false,
        message: `Data with muty_type_id ${id} not found`,
      };
      return outputResponse;
    }

    // Jika entri ada, lakukan pembaruan
    const data = await pool.query(
      `UPDATE mst_user_type
      SET muty_type_name = '${name}',
      muty_updated_at = DATE(NOW())
      WHERE muty_type_id = '${id}';`
    );
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Update successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// VENDOR
async function showVendor() {
  try {
    const data = await pool.query(`select 
    mv.mven_vendor_id, 
    mv.mven_store,
    mcd.mcdb_flag_source as store_name, 
    mv.mven_vendor_name, 
    mv.mven_valid_from, 
    mv.mven_valid_to, 
    mv.mven_meal_price, 
    mv.mven_vendor_contract, 
    mv.mven_created_at, 
    mv.mven_updated_at, 
    mv.mven_isactive
    from mst_vendor mv
    join mst_canteen_db mcd
    on mv.mven_store = mcd.mcdb_store_code;`);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// async function addVendor(param) {
//   console.log(param[0]);
//   try {
//     const insertQuery = `
//     INSERT INTO mst_vendor(
//       mven_store,
//       mven_vendor_name,
//       mven_valid_from,
//       mven_valid_to,
//       mven_meal_price,
//       mven_vendor_contract,
//       mven_created_at,
//       mven_updated_at,
//       mven_isactive
//     )
//     VALUES($1, $2, $3, $4, $5, $6, NOW(), NOW(), true)
//   `;

//     const values = [
//       param[0].mven_store,
//       param[0].mven_vendor_name,
//       param[0].mven_valid_from,
//       param[0].mven_valid_to,
//       param[0].mven_meal_price,
//       param[0].mven_vendor_contract,
//     ];

//     try {
//       await pool.query(insertQuery, values);
//       const outputResponse = {
//         status: true,
//         message: "Data has been saved",
//       };
//       return outputResponse;
//     } catch (error) {
//       console.error("Error inserting data:", error);
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

async function addVendor(param) {
  console.log(param[0]);
  try {
    const insertQuery = `
    INSERT INTO mst_vendor(
      mven_store,
      mven_vendor_name,
      mven_valid_from,
      mven_valid_to,
      mven_meal_price,
      mven_vendor_contract,
      mven_created_at,
      mven_updated_at,
      mven_isactive
    )
    VALUES($1, $2, $3, $4, $5, $6, NOW(), NOW(), true)
  `;

    const values = [
      param[0].mven_store,
      param[0].mven_vendor_name,
      param[0].mven_valid_from,
      param[0].mven_valid_to,
      param[0].mven_meal_price,
      param[0].mven_vendor_contract,
    ];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// async function updateVendor(param) {
//   console.log(param[0]);
//   try {
//     const insertQuery = `
//     UPDATE mst_vendor
//     SET
//     mven_store = $2,
//     mven_vendor_name = $3,
//     mven_valid_from = $4,
//     mven_valid_to = $5,
//     mven_meal_price = $6,
//     mven_vendor_contract = $7,
//     mven_updated_at = NOW(),
//     mven_isactive = true
//     WHERE mven_vendor_id = $1
//   `;

//     const values = [
//       param[0].mven_vendor_id,
//       param[0].mven_store,
//       param[0].mven_vendor_name,
//       param[0].mven_valid_from,
//       param[0].mven_valid_to,
//       param[0].mven_meal_price,
//       param[0].mven_vendor_contract,
//     ];

//     try {
//       await pool.query(insertQuery, values);
//       const outputResponse = {
//         status: true,
//         message: "Data has been saved",
//       };
//       return outputResponse;
//     } catch (error) {
//       console.error("Error inserting data:", error);
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

async function updateVendor(param) {
  try {
    console.log(param[0]);
    const vendorId = param[0].mven_vendor_id;
    // Cek apakah entri dengan id yang diberikan ada di dalam tabel
    const checkQuery = `
      SELECT *
      FROM mst_vendor
      WHERE mven_vendor_id = $1
    `;
    const checkResult = await pool.query(checkQuery, [vendorId]);

    if (checkResult.rows.length === 0) {
      const outputResponse = {
        status: false,
        message: `Data with mven_vendor_id ${vendorId} not found`,
      };
      return outputResponse;
    }

    const updateQuery = `
    UPDATE mst_vendor
    SET
    mven_store = $2,
    mven_vendor_name = $3,
    mven_valid_from = $4,  
    mven_valid_to = $5,    
    mven_meal_price = $6,
    mven_vendor_contract = $7,  
    mven_updated_at = NOW(),
    mven_isactive = true
    WHERE mven_vendor_id = $1
  `;

    const values = [
      param[0].mven_vendor_id,
      param[0].mven_store,
      param[0].mven_vendor_name,
      param[0].mven_valid_from,
      param[0].mven_valid_to,
      param[0].mven_meal_price,
      param[0].mven_vendor_contract,
    ];

    await pool.query(updateQuery, values);
    const outputResponse = {
      status: true,
      message:
        "Update for Vendor Name: " +
        param[0].mven_vendor_name +
        " is successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function deleteVendor(param) {
  try {
    const insertQuery = `
    UPDATE mst_vendor
    SET
    mven_isactive = false
    WHERE mven_vendor_id  = $1;
  `;

    const values = [param.mven_vendor_id];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// MEAL ORDER
async function showMealOrder() {
  try {
    const data = await pool.query(`
    select 
    tmo.tmor_order_id ,
    tmo.muse_id,
    mu.muse_name,
    mu.muse_user_nik,
    tmo.mven_vendor_id,
    mv.mven_vendor_name, 
    mv.mven_meal_price,
    tmo.muse_rfid,
    mcd.mcdb_flag_source as flag_source,
    tmo.tmor_order_date,
    tmo.tmor_order_datetime,
    tmo.tmor_created_at,
    tmo.tmor_updated_at
  from trx_meal_orders tmo
  join mst_vendor mv
  on tmo.mven_vendor_id = mv.mven_vendor_id
  join mst_user mu
  on tmo.muse_id = mu.muse_id
  join mst_canteen_db mcd
  on tmo.tmor_store_code = mcd.mcdb_store_code;`);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function searchMealOrder(param) {
  console.log("Parameters:", param);
  try {
    const insertQuery = `
    select 
        tmo.tmor_order_id ,
        tmo.muse_id,
        mu.muse_name,
        mu.muse_user_nik,
        tmo.mven_vendor_id,
        mv.mven_vendor_name, 
        mv.mven_meal_price,
        tmo.muse_rfid,
        mcd.mcdb_flag_source as flag_source,
        tmo.tmor_order_date,
        tmo.tmor_order_datetime,
        tmo.tmor_created_at,
        tmo.tmor_updated_at
    from trx_meal_orders tmo
    join mst_vendor mv
    on tmo.mven_vendor_id = mv.mven_vendor_id
    join mst_user mu
    on tmo.muse_id = mu.muse_id
    join mst_canteen_db mcd
    on tmo.tmor_store_code = mcd.mcdb_store_code
    where 
        tmo.tmor_order_date >= $1 
        and tmo.tmor_order_date <= $2
        and tmo.tmor_store_code = left($3, 4);
  `;
    const values = [
      param.tmor_order_from,
      param.tmor_order_to,
      param.tmor_store_code,
    ];

    const { rows } = await pool.query(insertQuery, values); // Fetch data from the database
    console.log("Data retrieved from database:", rows);
    return rows; // Return the retrieved data
  } catch (error) {
    console.error("Error retrieving data:", error);
    throw error;
  }
}

async function validateOrder(param) {
  console.log(param);
  try {
    const validQuery = await pool.query(`
    select 
      mu.muse_id,
      mv.mven_vendor_id,
      tmo.muse_rfid,
      mv.mven_store,
      tmo.tmor_order_date,
      tmo.tmor_order_datetime
    from mst_user mu
    join trx_meal_orders tmo
    on mu.muse_id = tmo.muse_id
    join mst_vendor mv
    on tmo.mven_vendor_id = mv.mven_vendor_id
    where 
      tmo.muse_rfid = '${param.muse_rfid}'
      and tmor_order_date = CURRENT_DATE 
      and tmor_order_datetime >= CURRENT_TIMESTAMP - interval '8 hours';
    `);

    const validateOrders = validQuery.rowCount;
    return validateOrders;
  } catch (error) {
    console.error("Error validating data:", error);
    return false;
  }
}

async function addPrintLog() {
  try {
    const insertQuery = `
    insert into trx_print_log (
      tplo_user_id,
      tmor_order_id,
      tplo_print_status,
      tplo_print_sequence,
      tplo_store,
      tplo_created_at,
      tplo_updated_at
      )
    select 
      tmo.muse_id,
      tmo.tmor_order_id,
      true,
      count(tmo.muse_id) as print_sequence,
      tmo.tmor_store_code,
      tmo.tmor_created_at,
      tmo.tmor_updated_at
    from trx_meal_orders tmo
    where tmo.tmor_order_date = current_date
    group by tmo.muse_id, tmo.tmor_order_id, tmo.tmor_store_code, tmo.tmor_created_at, tmo.tmor_updated_at;
  `;

    try {
      await pool.query(insertQuery);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function updateStatusLog() {
  try {
    const insertQuery = `
    update trx_print_log 
    set 
      tplo_print_status = false
    where 
    tplo_print_sequence = 0
    or tplo_print_sequence > 2
    and tplo_created_at = CURRENT_DATE;
  `;

    try {
      await pool.query(insertQuery);
      const outputResponse = {
        status: true,
        message: "Data has been updated",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error updating data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function addMealOrder(param) {
  console.log(param);
  try {
    const insertQuery = `
    INSERT INTO trx_meal_orders(
      muse_id,
      mven_vendor_id,
      muse_rfid,
      tmor_store_code,
      tmor_order_date,
      tmor_order_datetime,
      tmor_created_at,
      tmor_updated_at
    )
    select
      mu.muse_id,
      $1,
      CAST($2 AS TEXT),
      CAST($3 AS TEXT),
      CURRENT_DATE,
      CURRENT_TIMESTAMP,
      CURRENT_TIMESTAMP,
      null
    FROM
      mst_user mu
    where mu.muse_rfid = CAST($2 AS TEXT);
  `;

    const values = [
      param.mven_vendor_id,
      param.muse_rfid,
      param.tmor_store_code,
    ];

    // const selectOrder = await pool.query(selectQuery, values);
    const isValidateOrder = await validateOrder(param);

    try {
      console.log(isValidateOrder);
      if (isValidateOrder === 0) await pool.query(insertQuery, values);
      if (isValidateOrder > 0) console.log("Can't place order before 8 hours!");

      await addPrintLog();
      await updateStatusLog();

      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// USER
async function showUser() {
  try {
    const data = await pool.query(`
    select 
      mu.muse_id, 
      mu.muse_user_nik,
      mcd.mcdb_flag_source, 
      mu.muse_store,
      mut.muty_type_name, 
      mu.muty_type_id,
      mu.muse_rfid, 
      mu.muse_name, 
      mu.muse_created_at, 
      mu.muse_updated_at, 
      mu.muse_isactive 
    from mst_user mu
    join mst_user_type mut 
    on mu.muty_type_id = mut.muty_type_id
    join mst_canteen_db mcd
    on mu.muse_store = mcd.mcdb_store_code;`);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// async function addUser(param) {
//   try {
//     console.log(param[0]);
//     const insertQuery = `
//   INSERT INTO mst_user(
//     muse_user_nik,
//     muty_type_id,
//     muse_rfid,
//     muse_name,
//     muse_created_at,
//     muse_updated_at,
//     muse_isactive,
//     muse_store
//   )
//   VALUES($1, $2, $3, $4, NOW(), NOW(), true, left($5, 4))
// `;

//     const values = [
//       param[0].muse_user_nik,
//       param[0].muty_type_id,
//       param[0].muse_rfid,
//       param[0].muse_name,
//       param[0].muse_store,
//     ];

//     try {
//       await pool.query(insertQuery, values);
//       const outputResponse = {
//         status: true,
//         message: "Data has been saved",
//       };
//       return outputResponse;
//     } catch (error) {
//       console.error("Error inserting data:", error);
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

async function addUser(param) {
  try {
    console.log(param[0]);
    const userNik = param[0].muse_user_nik;
    const userRfid = param[0].muse_rfid;

    const checkQuery = `
    SELECT muse_id
    FROM mst_user
    WHERE muse_user_nik = $1 AND muse_rfid = $2
  `;

    const checkResult = await pool.query(checkQuery, [userNik, userRfid]);

    if (checkResult.rows.length > 0) {
      const outputResponse = {
        status: false,
        message: "User NIK and/ or RFID already exists",
      };
      console.log(outputResponse);
      return outputResponse;
    }

    const insertQuery = `
    INSERT INTO mst_user(
      muse_user_nik,
      muty_type_id,
      muse_rfid,
      muse_name,
      muse_created_at,
      muse_updated_at,
      muse_isactive,
      muse_store
    )
    VALUES($1, $2, $3, $4, NOW(), NOW(), true, left($5, 4))
  `;

    const values = [
      param[0].muse_user_nik,
      param[0].muty_type_id,
      param[0].muse_rfid,
      param[0].muse_name,
      param[0].muse_store,
    ];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
      throw new Error("Failed to insert data into mst_user");
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// async function updateUser(param) {
//   console.log(param[0]);
//   try {
//     const insertQuery = `
//     UPDATE mst_user
//     SET
//     muse_user_nik = $2,
//     muty_type_id = $3,
//     muse_rfid = $4,
//     muse_name = $5,
//     muse_updated_at = NOW(),
//     muse_isactive = $6,
//     muse_store = $7
//     WHERE muse_id = $1
//   `;

//     const values = [
//       param[0].muse_id,
//       param[0].muse_user_nik,
//       param[0].muty_type_id,
//       param[0].muse_rfid,
//       param[0].muse_name,
//       param[0].muse_isactive,
//       param[0].muse_store,
//     ];

//     try {
//       await pool.query(insertQuery, values);
//       const outputResponse = {
//         status: true,
//         message: "Data has been saved",
//       };
//       return outputResponse;
//     } catch (error) {
//       console.error("Error inserting data:", error);
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

async function updateUser(param) {
  try {
    console.log(param[0]);
    const userId = param[0].muse_id;

    const checkQuery = `
    SELECT *
    FROM mst_user
    WHERE muse_id = $1
  `;

    const checkResult = await pool.query(checkQuery, [userId]);

    if (checkResult.rows.length === 0) {
      const outputResponse = {
        status: false,
        message: `Data with muse_id ${userId} not found`,
      };
      return outputResponse;
    }

    const updateQuery = `
    UPDATE mst_user
    SET
    muse_user_nik = $2, 
    muty_type_id = $3,
    muse_rfid = $4,
    muse_name = $5,
    muse_updated_at = NOW(),
    muse_isactive = $6,
    muse_store = $7
    WHERE muse_id = $1
  `;

    const values = [
      param[0].muse_id,
      param[0].muse_user_nik,
      param[0].muty_type_id,
      param[0].muse_rfid,
      param[0].muse_name,
      param[0].muse_isactive,
      param[0].muse_store,
    ];

    await pool.query(updateQuery, values);
    const outputResponse = {
      status: true,
      message:
        "Update for User Name: " +
        "( " +
        param[0].muse_name +
        ")" +
        " is successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function deleteUser(param) {
  try {
    const insertQuery = `
    UPDATE mst_user
    SET
    muse_isactive = false
    WHERE muse_id  = $1;
  `;

    const values = [param.muse_id];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function addCSVData(csvFilePath) {
  let client;
  try {
    console.log("Connecting to the database...");
    client = await pool.connect();
    console.log("Connected to the database successfully.");

    await client.query("BEGIN");
    const stream = fs.createReadStream(csvFilePath);
    const csvData = [];

    const parser = fastcsv
      .parseStream(stream, { headers: true, delimiter: ";" })
      .on("error", (error) => {
        console.error("CSV parsing error:", error);
        throw error;
      })
      .on("data", (data) => {
        csvData.push(data);
      })
      .on("end", async () => {
        try {
          for (const row of csvData) {
            const {
              muse_user_nik,
              muty_type_id,
              muse_rfid,
              muse_name,
              muse_store,
            } = row;
            await client.query(
              `INSERT INTO mst_user (muse_user_nik, muty_type_id, muse_rfid, muse_name, muse_created_at, muse_updated_at, muse_isactive, muse_store)
              VALUES ($1, (SELECT muty_type_id FROM mst_user_type WHERE muty_type_name = $2), $3, $4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, true, $5);`,
              [muse_user_nik, muty_type_id, muse_rfid, muse_name, muse_store]
            );
          }

          await client.query("COMMIT");
          console.log("CSV data inserted successfully!");
        } catch (error) {
          await client.query("ROLLBACK");
          console.error("Error inserting CSV data:", error);
          throw error;
        }
      });

    await new Promise((resolve, reject) => {
      parser.on("finish", resolve);
      parser.on("error", reject);
    });
  } catch (error) {
    console.error("Error connecting to the database:", error);
  } finally {
    if (client) {
      client.release();
    }
  }
}

async function showBranch() {
  try {
    const data = await pool.query(
      `select mcdb_store_code, mcdb_flag_source from mst_canteen_db;`
    );
    console.log(data.rows);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// STORE TAP

async function showTap() {
  try {
    const data = await pool.query(`select * from mst_store_tap;`);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function addTap(param) {
  const branch = param.userBranch.split("-");

  try {
    const checkQuery = `
      SELECT mst_code_store
      FROM mst_store_tap
      WHERE mst_code_store = $1
    `;

    const checkResult = await pool.query(checkQuery, [branch[0]]);

    if (checkResult.rows.length > 0) {
      const outputResponse = {
        status: false,
        message: "Data with the provided already exists",
      };
      return outputResponse;
    }

    const insertQuery = `
    INSERT INTO public.mst_store_tap
    (mst_name_store, mst_code_store, mst_url_store, mst_ip_address, mst_port_printer, status)
    VALUES($1, $2, $3, $4, $5, $6)
    `;

    const values = [
      branch[1],
      branch[0],
      param.userStore,
      param.ipAddressPrinter,
      param.portPrinter,
      param.userStatus,
    ];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
      throw new Error("Failed to insert data into mst_user_type");
    }
  } catch (e) {
    console.log("error", e.toString());
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function deleteTap(id) {
  try {
    const data = await pool.query(
      `delete from mst_store_tap where muty_type_id = '${id}';`
    );
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function updateTap(param) {
  try {
    const branch = param.userBranch.split("-");

    // console.log("param", param);
    // return;

    // param {
    //   userBranch: '1001-BSD',
    //   userStore: 'askdkqwlwnkn',
    //   ipAddressPrinter: '10.123.38.13',
    //   portPrinter: '3388',
    //   userStatus: false,
    //   editID: editID
    // }

    const checkQuery = `
      SELECT *
      FROM mst_store_tap
      WHERE mst_id = $1
    `;
    const checkResult = await pool.query(checkQuery, [param.editID]);

    if (checkResult.rows.length === 0) {
      const outputResponse = {
        status: false,
        message: `Data with muty_type_id ${param.editID} not found`,
      };
      return outputResponse;
    }

    // Jika entri ada, lakukan pembaruan
    const data = await pool.query(
      `UPDATE public.mst_store_tap
      SET mst_name_store='${branch[1]}', 
      mst_code_store='${branch[0]}', 
      mst_url_store='${param.userStore}', 
      mst_ip_address='${param.ipAddressPrinter}', 
      mst_port_printer='${param.portPrinter}', 
      status='${param.userStatus}' where mst_id = '${param.editID}';`
    );
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Update successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// addCSVData("C:/Users/steff/OneDrive/Desktop/format-user-csv-insert.csv").catch(
//   (error) => console.error("Error:", error)
// );

// BANNER
async function showBanner() {
  try {
    const data = await pool.query(`select * from trx_ads;`);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

// SEARCH SCAN
// async function searchScan(param) {
//   try {
//     const data = await pool.query(
//       `SELECT * FROM mst_user WHERE muse_rfid = $1`,
//       [param.muse_rfid]
//     );

//     if (data.rows.length > 0) {
//       // Check if data exists in trx_meal_orders
//       const orderData = await pool.query(
//         `SELECT * FROM trx_meal_orders WHERE muse_rfid = $1`,
//         [param.muse_rfid]
//       );

//       const outputResponse = {
//         status: true,
//         data: data.rows,
//         message: "Success",
//       };
//       return outputResponse;
//     } else {
//       const outputResponse = {
//         status: false,
//         message: "User not found",
//       };
//       return outputResponse;
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

// async function searchScan(param) {
//   try {
//     const userData = await pool.query(
//       `SELECT * FROM mst_user WHERE muse_rfid = $1`,
//       [param.muse_rfid]
//     );

//     if (userData.rows.length > 0) {
//       // Check if data exists in trx_meal_orders
//       const orderData = await pool.query(
//         `SELECT * FROM trx_meal_orders WHERE muse_rfid = $1`,
//         [param.muse_rfid]
//       );

//       if (orderData.rows.length > 0) {
//         const outputResponse = {
//           status: true,
//           data: orderData.rows,
//           message: "RFID found in trx_meal_orders",
//         };
//         return outputResponse;
//       } else {
//         const outputResponse = {
//           status: true,
//           data: userData.rows,
//           message: "RFID not found in trx_meal_orders",
//         };
//         return outputResponse;
//       }
//     } else {
//       const outputResponse = {
//         status: false,
//         message: "RFID not found in mst_user",
//       };
//       return outputResponse;
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

// async function searchScan(param) {
//   try {
//     // Validate input length

//     var paramRFID = param.muse_rfid.length === 10 ? "rfid" : "nik";
//     if (param.muse_rfid.length >= 7 && param.muse_rfid.length <= 8) {
//       // Check NIK
//       const userData = await pool.query(
//         `SELECT * FROM mst_user WHERE muse_user_nik = $1`,
//         [param.muse_user_nik]
//       );

//       if (userData.rows.length > 0) {
//         const userId = userData.rows[0].param.muse_id;

//         const orderData = await pool.query(
//           `SELECT * FROM trx_meal_orders WHERE muse_id = $1`,
//           [userId]
//         );

//         if (orderData.rows.length > 0) {
//           const outputResponse = {
//             status: true,
//             data: orderData.rows,
//             message: "NIK found in trx_orders",
//           };
//           return outputResponse;
//         } else {
//           const outputResponse = {
//             status: true,
//             data: userData.rows,
//             message: "NIK not found in trx_orders",
//           };
//           return outputResponse;
//         }
//       } else {
//         const outputResponse = {
//           status: false,
//           message: "NIK not found in mst_user",
//         };
//         return outputResponse;
//       }
//     } else {
//       // Check RFID
//       const userData = await pool.query(
//         `SELECT * FROM mst_user WHERE muse_rfid = $1`,
//         [param.muse_rfid]
//       );

//       if (userData.rows.length > 0) {
//         // Check if data exists in trx_meal_orders
//         const orderData = await pool.query(
//           `SELECT * FROM trx_meal_orders WHERE muse_rfid = $1`,
//           [param.muse_rfid]
//         );

//         if (orderData.rows.length > 0) {
//           const outputResponse = {
//             status: true,
//             data: orderData.rows,
//             message: "RFID found in trx_meal_orders",
//           };
//           return outputResponse;
//         } else {
//           const outputResponse = {
//             status: true,
//             data: userData.rows,
//             message: "RFID not found in trx_meal_orders",
//           };
//           return outputResponse;
//         }
//       } else {
//         const outputResponse = {
//           status: false,
//           message: "RFID not found in mst_user",
//         };
//         return outputResponse;
//       }
//     }
//   } catch (e) {
//     const outputResponse = {
//       status: false,
//       message: e.toString(),
//     };
//     return outputResponse;
//   }
// }

async function searchScan(param) {
  try {
    // Validate input length
    const paramRFID = param.muse_rfid.length === 10 ? "rfid" : "nik";

    if (paramRFID === "rfid") {
      // Check RFID
      const userData = await pool.query(
        `SELECT * FROM mst_user WHERE muse_rfid = $1`,
        [param.muse_rfid]
      );

      if (userData.rows.length > 0) {
        const orderData = await pool.query(
          `SELECT * FROM trx_meal_orders WHERE muse_rfid = $1`,
          [param.muse_rfid]
        );

        if (orderData.rows.length > 0) {
          const outputResponse = {
            status: true,
            data: orderData.rows,
            message: "RFID (found) in trx_meal_orders",
          };
          return outputResponse;
        } else {
          const outputResponse = {
            status: true,
            data: userData.rows,
            message: "RFID (not found) in trx_meal_orders",
          };
          return outputResponse;
        }
      } else {
        const outputResponse = {
          status: false,
          message: "RFID not found in (mst_user)",
        };
        return outputResponse;
      }
    } else {
      // Check NIK
      const userData = await pool.query(
        `SELECT * FROM mst_user WHERE muse_user_nik = $1`,
        [param.muse_user_nik]
      );

      if (userData.rows.length > 0) {
        // Check if data exists in trx_meal_orders
        const orderData = await pool.query(
          `SELECT mu.muse_user_nik
          FROM trx_meal_orders tm
          INNER JOIN mst_user mu ON tm.muse_id = mu.muse_id
          WHERE tm.muse_id = (SELECT muse_id FROM mst_user WHERE muse_user_nik = $1);
          `,
          [param.muse_user_nik]
        );

        if (orderData.rows.length > 0) {
          const outputResponse = {
            status: true,
            data: orderData.rows,
            message: "NIK found in trx_meal_orders",
          };
          return outputResponse;
        } else {
          const outputResponse = {
            status: true,
            data: userData.rows,
            message: "NIK not found in trx_meal_orders",
          };
          return outputResponse;
        }
      } else {
        const outputResponse = {
          status: false,
          message: "NIK not found in mst_user",
        };
        return outputResponse;
      }
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function insertBanner(param) {
  console.log(param[0]);
  try {
    const insertQuery = `
    INSERT INTO public.trx_ads
    (mcdb_store_code, 
    muse_user_nik, 
    tads_upload_by, 
    tads_filename, 
    tads_filepath, 
    tads_isactive, 
    tads_upload_at, 
    tads_created_at, 
    tads_updated_at)
    VALUES($1, $2, $3, $4, './backend/public/upload$5', true, NOW(), NOW(), NULL);
  `;

    const values = [
      param[0].mcdb_store_code,
      param[0].muse_user_nik,
      param[0].tads_upload_by,
      param[0].tads_filename,
      param[0].tads_filepath,
    ];

    try {
      await pool.query(insertQuery, values);
      const outputResponse = {
        status: true,
        message: "Data has been saved",
      };
      return outputResponse;
    } catch (error) {
      console.error("Error inserting data:", error);
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      message: e.toString(),
    };
    return outputResponse;
  }
}

module.exports = {
  showUserType,
  addUserType,
  deleteUserType,
  updateUserType,
  showVendor,
  addVendor,
  updateVendor,
  deleteVendor,
  showMealOrder,
  searchMealOrder,
  addMealOrder,
  showUser,
  addUser,
  updateUser,
  deleteUser,
  updateStatusLog,
  addPrintLog,
  validateOrder,
  addCSVData,
  showBranch,
  showTap,
  addTap,
  deleteTap,
  updateTap,
  showBanner,
  searchScan,
  insertBanner,
};
