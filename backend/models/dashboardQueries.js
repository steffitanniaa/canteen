const async = require("async");
const Pool = require("pg").Pool;

const moment = require("moment-timezone");
const jakartaDateTime = moment().tz("Asia/Jakarta");
const formattedJakartaDateTime = jakartaDateTime.format("YYYY-MM-DD HH:mm:ss");

const pool = new Pool({
  user: process.env.DEV_DATABASE_USERNAME_CANTEEN,
  host: process.env.DEV_DATABASE_HOST_CANTEEN,
  database: process.env.DEV_DATABASE_NAME_CANTEEN,
  password: process.env.DEV_DATABASE_PASSWORD_CANTEEN,
  port: process.env.DEV_DATABASE_PORT_CANTEEN,
});

function transformData(input) {
  const value = input || [];
  const transformedData = {
    datetime: [],
    order: [],
  };

  value.forEach((entry) => {
    transformedData.datetime.push(convertDateTimeToHour(entry.datetime));
    transformedData.order.push(entry.order);
  });

  return transformedData;
}

function transformDataAltTotal(input) {
  const value = input || []; // Access the 'data' property if it exists, or default to an empty array
  const transformedData = {
    x: [],
    y: 0,
  };

  value.forEach((entry) => {
    transformedData.x.push(entry.x);
    transformedData.y += parseInt(entry.y);
  });

  return transformedData;
}

function transformDataAlt(input) {
  const value = input || []; // Access the 'data' property if it exists, or default to an empty array
  const transformedData = {
    x: [],
    y: [],
  };

  value.forEach((entry) => {
    transformedData.x.push(entry.x);
    transformedData.y.push(entry.y);
  });

  return transformedData;
}

function convertDateTimeToHour(inputDateTime) {
  const date = new Date(inputDateTime);
  const hour = date.getHours();
  const minute = date.getMinutes();

  const formattedTime = `${hour.toString().padStart(2, "0")}.${minute
    .toString()
    .padStart(2, "0")}`;

  return formattedTime;
}

async function store() {
  try {
    const data = await pool.query(`
      select 
          mcdb_flag_source as store
      from mst_canteen_db
      order by mcdb_store_code;
    `);

    const res = transformDataAlt(data.rows);
    const outputResponse = {
      status: true,
      data: data.rows,
      message: "Get data successful",
    };
    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function graph(store) {
  try {
    // Get all store
    const allStore = await pool.query(`
      select 
          mcdb_flag_source as store
      from mst_canteen_db
      order by mcdb_store_code;
    `);

    // console.log(allStore.rows);

    const defaultData = [];

    if (store.length > 0) {
      for (let i = 0; i < store.length; i++) {
        defaultData.push("'" + store[i].store + "'");
      }
    } else {
      for (let i = 0; i < allStore.rows.length; i++) {
        defaultData.push("'" + allStore.rows[i].store + "'");
      }
    }

    // console.log(defaultData);

    // Order by Hour
    const queryOrderByHour = await pool.query(`
    SELECT date_trunc('hour', tmor_order_datetime) + interval '1 hour' AS datetime,
        count(tmor_order_id) AS order
    FROM trx_meal_orders tmo
    join mst_canteen_db mcd
    on tmo.tmor_store_code = mcd.mcdb_store_code
    where 
      mcd.mcdb_flag_source in (${defaultData})
      and tmo.tmor_order_date = CURRENT_DATE
    GROUP BY date_trunc('hour', tmor_order_datetime)
    ORDER BY 1 ASC;
    `);

    // Order by Store
    const queryOrderByStore = await pool.query(`
    SELECT 
        mcd.mcdb_flag_source AS x,
        count(tmo.tmor_order_id) AS y
    FROM 
        trx_meal_orders tmo
    JOIN 
        mst_canteen_db mcd ON tmo.tmor_store_code = mcd.mcdb_store_code
    where 
      mcd.mcdb_flag_source in (
        ${defaultData}
      ) and
      tmo.tmor_order_date = CURRENT_DATE
    GROUP BY 
        mcd.mcdb_flag_source
    ORDER BY 
        mcd.mcdb_flag_source ASC;
    `);

    const queryEmployeeRole = await pool.query(`
    select 
        mut.muty_type_name as x,
        count(mu.muse_id) as y
    from mst_user mu
    join mst_user_type mut
    on mu.muty_type_id = mut.muty_type_id
    join mst_canteen_db mcd
    on mu.muse_store = mcd.mcdb_store_code
    where mcd.mcdb_flag_source in (${defaultData})
    group by mut.muty_type_name;
    `);

    // Total Order
    const queryOrderTotal = await pool.query(`
    select 
        date_trunc('hours', tmor_order_datetime) as x, 
        count(tmor_order_id) as y 
    from trx_meal_orders tmo
    join mst_canteen_db mcd
    on tmo.tmor_store_code = mcd.mcdb_store_code
    where 
      mcd.mcdb_flag_source in (${defaultData})
      and tmo.tmor_order_date = CURRENT_DATE
    group by date_trunc('hours', tmor_order_datetime);
    `);

    // Total Vendor
    const queryVendorTotal = await pool.query(`
    select 
        date_trunc('months', mven_created_at) as x, 
        count(mven_vendor_id) as y 
    from mst_vendor mv
    join mst_canteen_db mcd
    on mv.mven_store = mcd.mcdb_store_code
    where 
      mcd.mcdb_flag_source in (${defaultData}) 
      and mv.mven_isactive = true
    group by date_trunc('months', mven_created_at);
    `);

    // Total User
    const queryUserTotal = await pool.query(`
    select 
        date_trunc('months', muse_created_at) as x, 
        count(muse_id) as y
    from mst_user mu
    join mst_canteen_db mcd
    on mu.muse_store = mcd.mcdb_store_code
    where 
      mcd.mcdb_flag_source in (${defaultData}) 
      and mu.muse_isactive = true
    group by date_trunc('months', muse_created_at);
    `);

    const dataOrderByHour = transformData(queryOrderByHour.rows);
    const dataOrderByStore = transformDataAlt(queryOrderByStore.rows);
    const dataEmployeeByRole = transformDataAlt(queryEmployeeRole.rows);
    const dataOrderTotal = transformDataAltTotal(queryOrderTotal.rows);
    const dataUserTotal = transformDataAltTotal(queryUserTotal.rows);
    const dataVendorTotal = transformDataAltTotal(queryVendorTotal.rows);

    // console.log(dataOrderByHour);
    // console.log(dataEmployeeByRole);

    const outputResponse = {
      status: true,
      timeOrderByHour: dataOrderByHour.datetime,
      amountOrderByHour: dataOrderByHour.order,
      valueStoreOrderByHour: dataOrderByStore.x,
      amountStoreOrderByHour: dataOrderByStore.y,
      valueEmployeeRole: dataEmployeeByRole.x,
      nameEmployeeRole: dataEmployeeByRole.y,
      totalOrderDaily: dataOrderTotal.y,
      totalUsers: dataUserTotal.y,
      totalVendor: dataVendorTotal.y,
      message: "Get data successful",
    };

    return outputResponse;
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

module.exports = {
  store,
  graph,
};
