const async = require("async");
const Pool = require("pg").Pool;

const moment = require("moment-timezone");
const jakartaDateTime = moment().tz("Asia/Jakarta");
const formattedJakartaDateTime = jakartaDateTime.format("YYYY-MM-DD HH:mm:ss");

const fs = require("fs");
const fastcsv = require("fast-csv");

const pool = new Pool({
  user: process.env.DEV_DATABASE_USERNAME_CANTEEN,
  host: process.env.DEV_DATABASE_HOST_CANTEEN,
  database: process.env.DEV_DATABASE_NAME_CANTEEN,
  password: process.env.DEV_DATABASE_PASSWORD_CANTEEN,
  port: process.env.DEV_DATABASE_PORT_CANTEEN,
});

async function getAll() {
  try {
    const data = await pool.query(`SELECT * FROM fn_wow_menus_get_all_v2()`);
    if (data.rows.length > 0) {
      const beginProcessmenu = await processMenu(data.rows);

      var outputResponse = {
        status: true,
        message:
          beginProcessmenu.length > 0
            ? "Get data successful"
            : "Get data not successful",
        data: beginProcessmenu.length > 0 ? beginProcessmenu : [],
      };

      return outputResponse;
    } else {
      var outputResponse = {
        status: true,
        message: "Get data not successful",
        data: [],
      };

      return outputResponse;
    }
  } catch (e) {
    const outputResponse = {
      status: false,
      data: null,
      message: e.toString(),
    };
    return outputResponse;
  }
}

async function getMenuChildren(parentId) {
  const query = `SELECT * FROM fn_wow_menus_get_all_children_v2(${parentId})`;
  const results = await pool.query(query);
  return results.rows;
}

async function processMenu(menu) {
  const menuWithChildren = [];

  for (const item of menu) {
    const children = await getMenuChildren(item.mmen_id);
    const menuWithChild = { ...item, children };
    menuWithChildren.push(menuWithChild);
  }

  return menuWithChildren;
}

module.exports = {
  getAll,
};
