var express = require("express");
var router = express.Router();
var path = require("path");

// All RNP //
var canteenRoute = require("../routes/canteenRoute");

// Android //
var androidRoute = require("../routes/androidRoute");

// Default Route //
var accessController = require("../controllers/accessController");
// var menuController = require('../controllers/menuController');

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "PT. AEON INDONESIA" });
});

router.use("/access", accessController);
// router.use('/menus', menuController);

router.use("/canteen", canteenRoute);
router.use("/android", androidRoute);

module.exports = router;
