var express = require("express");
var router = express.Router();
var dotenv = require("dotenv");
var jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
var HttpStatus = require("http-status-codes");
var ActiveDirectory = require("activedirectory2");

var jwt = require("../libs/jwt");

var multer = require("multer");
var upload = multer({ dest: "public/uploads/" });
var canteenController = require("../controllers/canteenController");
var menuController = require("../controllers/menuController");
var dashboardController = require("../controllers/dashboardController");
// var dashboardController = require("../controllers/purchaseController");

// var nonPurchaseController = require('../../controllers/rnp/nonPurchaseController');
// var approveController = require('../../controllers/rnp/approveController');
// var toolController = require('../../controllers/rnp/toolController');
// var notificationController = require('../../controllers/notificationController');

require("dotenv").config();

process.on("uncaughtException", function (err) {
  console.log(err);
});

const detectBearerToken = async (req, res, next) => {
  const authHeader = req.headers["authorization"];
  if (authHeader && authHeader.startsWith("Bearer ")) {
    const token = authHeader.split(" ")[1];
    if (!token) {
      var output = {
        token: "Bearer token not found",
        status: false,
        message: "Error",
        data: [],
      };

      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }

    const checkToken = await jwt.ConfigurationToken(token);

    if (checkToken.status) {
      next();
    } else {
      var output = {
        token: "Bearer token not found",
        status: false,
        message: "Error",
        data: [],
      };

      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } else {
    var output = {
      token: "Bearer token not found",
      status: false,
      message: "Error",
      data: [],
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  }
};

router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

router.use(bodyParser.json());
router.use(detectBearerToken);

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "PT. AEON INDONESIA" });
});

// MENU
router.post("/menus/get-all", menuController.getAll);

// DASHBOARD
router.post("/store", dashboardController.store);
router.post("/graph", dashboardController.graph);

// USER TYPE
router.post("/show-user-type", canteenController.showUserType);
router.post("/add-user-type", canteenController.addUserType);
router.post("/delete-user-type", canteenController.deleteUserType);
router.post("/update-user-type", canteenController.updateUserType);

// VENDOR
router.post("/show-vendor", canteenController.showVendor);
router.post("/add-vendor", canteenController.addVendor);
router.post("/update-vendor", canteenController.updateVendor);
router.post("/delete-vendor", canteenController.deleteVendor);

// MEAL ORDER
router.post("/show-meal-order", canteenController.showMealOrder);
router.post("/update-meal-order", canteenController.updateMealOrder);
router.post("/add-meal-order", canteenController.addMealOrder);

// USER
router.post("/show-user", canteenController.showUser);
router.post("/add-user", canteenController.addUser);
router.post("/update-user", canteenController.updateUser);
router.post("/delete-user", canteenController.deleteUser);

router.post("/add-csv-data", canteenController.addCSVData);
router.post("/show-branch", canteenController.showBranch);

router.post("/search-meal-order", canteenController.searchMealOrder);

// TAP
router.post("/show-tap", canteenController.showTap);
router.post("/add-tap", canteenController.addTap);
router.post("/delete-tap", canteenController.deleteTap);
router.post("/update-tap", canteenController.updateTap);

// BANNER
router.post("/show-banner", canteenController.showBanner);
router.post("/insert-banner", canteenController.insertBanner);

// ELECTRON
router.post("/show-scan", canteenController.showScan);

// LOGIN
router.post("/user-login", canteenController.userLogin);

// SEARCH SCAN
router.post("/search-scan", canteenController.searchScan);

// UPLOAD
router.post(
  "/purchase/save-attachment",
  upload.single("file"),
  canteenController.saveAttachment
);

module.exports = router;
