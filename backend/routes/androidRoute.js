var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");

var androidController = require("../controllers/androidController");

require("dotenv").config();

process.on("uncaughtException", function (err) {
  console.log(err);
});

router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

router.use(bodyParser.json());

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "PT. AEON INDONESIA" });
});

// MENU
router.post("/dashboard/tap", androidController.tap);
router.post("/dashboard/store", androidController.store);

module.exports = router;
