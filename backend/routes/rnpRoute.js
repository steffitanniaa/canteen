var express = require("express");
var router = express.Router();
var dotenv = require("dotenv");
var jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
var HttpStatus = require("http-status-codes");
var ActiveDirectory = require("activedirectory2");

var jwt = require("../../libs/jwt");

var multer = require("multer");
var upload = multer({ dest: "public/uploads/" });
var purchaseController = require("../../controllers/rnp/purchaseController");
var nonPurchaseController = require("../../controllers/rnp/nonPurchaseController");
var approveController = require("../../controllers/rnp/approveController");
var toolController = require("../../controllers/rnp/toolController");
var notificationController = require("../../controllers/notificationController");

require("dotenv").config();

process.on("uncaughtException", function (err) {
  console.log(err);
});

const detectBearerToken = async (req, res, next) => {
  const authHeader = req.headers["authorization"];
  if (authHeader && authHeader.startsWith("Bearer ")) {
    const token = authHeader.split(" ")[1];
    if (!token) {
      var output = {
        token: "Bearer token not found",
        status: false,
        message: "Error",
        data: [],
      };

      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }

    const checkToken = await jwt.ConfigurationToken(token);

    if (checkToken.status) {
      next();
    } else {
      var output = {
        token: "Bearer token not found",
        status: false,
        message: "Error",
        data: [],
      };

      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } else {
    var output = {
      token: "Bearer token not found",
      status: false,
      message: "Error",
      data: [],
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  }
};

router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

router.use(bodyParser.json());
router.use(detectBearerToken);

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "PT. AEON INDONESIA" });
});

// Purchase
router.post("/purchase/list", purchaseController.allData);
router.post("/purchase/view", purchaseController.viewDetail);
router.post("/purchase/submit", purchaseController.submit);
router.post("/purchase/save-header", purchaseController.saveHeader);
router.post("/purchase/save-detail", purchaseController.saveDetail);
router.post("/purchase/save-dept", purchaseController.saveDept);
router.post(
  "/purchase/save-attachment",
  upload.single("file"),
  purchaseController.saveAttachment
);
router.post("/purchase/get-header", purchaseController.getHeaderData);
router.post("/purchase/get-detail", purchaseController.getDetailData);
router.post("/purchase/get-dept", purchaseController.getRelatedDeptData);
router.post("/purchase/get-attachment", purchaseController.getAttachmentData);
router.post("/purchase/update-rnp", purchaseController.updateDataPurchase);
router.post("/purchase/check-loa", purchaseController.checkLoa);
router.post("/purchase/related-dept", purchaseController.getRelatedDept);

// Non Purchase
router.post("/non-purchase/list", nonPurchaseController.allData);
// router.post('/purchase/upload', upload.single('attachment'), formController.purchaseUpload)

// Approve
router.post("/approve/list", approveController.allData);

// Tools
router.post("/tool/currency", toolController.currency);
router.post("/tool/currencyCount", toolController.currencyCount);
router.post("/tool/type", toolController.type);
router.post("/tool/all-employee", toolController.allEmployee);
router.post("/tool/related-department", toolController.relatedDepartment);
router.post("/tool/method-payment", toolController.methodPayment);
router.post("/tool/tax", toolController.tax);
router.post("/tool/cost-center", toolController.getCostCenter);
router.post("/tool/supervisor", toolController.getSupervisor);

// Notification
router.post("/notification/email", notificationController.sendingEmail);

module.exports = router;
