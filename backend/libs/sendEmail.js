const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');

async function sendEmail(params) {

  process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

  try {
    // console.log(params)
    const transporter = nodemailer.createTransport({
      host: 'smtp.aeonindonesia.co.id',
      // port: 587,
      port: 465,
      secure: true,
      // tls: {
        // rejectUnauthorized: false
        // ciphers: 'SSLv3'
      // },
      auth: {
        user : 'noreply@aeonindonesia.co.id',
        pass : '+kopi-gula'      
      },
    });

    const emailTemplateSource = fs.readFileSync('../backend/views/email-template.hbs', 'utf-8');
    const emailTemplate = handlebars.compile(emailTemplateSource);

    const emailHtml = emailTemplate(params[0]);
    const mailOptions = {
      from: 'Workflow<noreply@aeonindonesia.co.id>',
      to: params[0].email,
      subject: params[0].subject,
      html: emailHtml,
    };

    const info = await transporter.sendMail(mailOptions);
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
}

module.exports = {
  sendEmail,
};

// const transporter = nodemailer.createTransport({
//   host: 'smtp.aeonindonesia.co.id',
//   port: 587,
//   secure: false,
//   auth: {
//     user: 'application.support@aeonindonesia.co.id',
//     pass: 'AEONCyber2019',
//   },
// });

// module.exports = transporter;