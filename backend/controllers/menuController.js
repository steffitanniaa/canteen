var HttpStatus = require("http-status-codes");
var db = require("../models/menuQueries");
var output;

async function getAll(req, res, next) {
  try {
    const data = await db.getAll();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "Get data successfull",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

module.exports = {
  getAll,
};
