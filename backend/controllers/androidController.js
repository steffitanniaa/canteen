var HttpStatus = require("http-status-codes");
var db = require("../models/androidQueries");
var output;

async function tap(req, res, next) {
  try {
    const param = req.body;
    const data = await db.tap(param);

    // const response = await axios.post(
    //   "https://google.com/your-endpoint",
    //   req.body
    // );
    // res.status(response.status).json(response.data);

    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function emergencySync(req, res, next) {
  try {
    const param = req.body;
    const data = await db.tap(param);

    // Store
    // Tanggal & waktu saat ini
    // path // 1001202405071701
    // 1001202405071701.txt

    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function syncData(req, res, next) {
  try {
    const param = req.body;
    const data = await db.syncData(param);

    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function reportProblem(req, res, next) {
  try {
    const param = req.body;

    const response = await axios.post(
      "https://google.com/your-endpoint",
      req.body
    );

    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function store(req, res, next) {
  try {
    const param = req.body;
    const data = await db.store(param);

    // const response = await axios.post(
    //   "https://google.com/your-endpoint",
    //   req.body
    // );
    // res.status(response.status).json(response.data);

    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };

    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

module.exports = {
  tap,
  reportProblem,
  emergencySync,
  syncData,
  store,
};
