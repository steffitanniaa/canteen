var HttpStatus = require("http-status-codes");
var db = require("../models/dashboardQueries");
var output;

async function store(req, res, next) {
  try {
    const data = await db.store();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "success",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function graph(req, res, next) {
  try {
    // const storeFilter = req.body.store;
    console.log("request store", req.body.store);
    const data = await db.graph(req.body.store);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "success",
        graph_1_axis: data.timeOrderByHour,
        graph_1_ordinate: data.amountOrderByHour,
        graph_2_axis: data.valueStoreOrderByHour,
        graph_2_ordinate: data.amountStoreOrderByHour,
        graph_3_axis: data.valueEmployeeRole,
        graph_3_ordinate: data.nameEmployeeRole,
        // graph_3_value: data.valueEmployeeRole,
        graph_4_value: data.totalOrderDaily,
        graph_5_value: data.totalUsers,
        graph_6_value: data.totalVendor,
      };

      // console.log(output)
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

module.exports = {
  store,
  graph,
};
