var HttpStatus = require("http-status-codes");
var db = require("../models/canteenQueries");
var output;

async function showScan(req, res, next) {
  try {
    // const data = await db.showUserType();
    // if (data.status == true) {
    //   output = {
    //     status: data.status,
    //     message: "show scan",
    //     data: data.data,
    //   };
    //   res.contentType("application/json").status(200);
    //   var valued = JSON.stringify(output);
    //   res.send(valued);
    // }
    output = {
      status: true,
      message: "show scan",
      data: [],
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showUserType(req, res, next) {
  try {
    const data = await db.showUserType();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show user type data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function addUserType(req, res, next) {
  try {
    const data = await db.addUserType(req.body);
    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function updateUserType(req, res, next) {
  const { muty_type_id, muty_type_name } = req.body;
  try {
    const data = await db.updateUserType(muty_type_id, muty_type_name);
    if (data.status == true) {
      output = {
        status: data.status,
        message: `${muty_type_name} succesfully updated`,
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function deleteUserType(req, res, next) {
  const { id } = req.body;
  try {
    const data = await db.deleteUserType(id);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "delete user type data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showVendor(req, res, next) {
  try {
    const data = await db.showVendor();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show vendor data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function addVendor(req, res, next) {
  try {
    const data = await db.addVendor(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function updateVendor(req, res, next) {
  try {
    const data = await db.updateVendor(req.body);
    output = {
      status: data.status,
      message: data.message,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function deleteVendor(req, res, next) {
  try {
    const data = await db.deleteVendor(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "delete vendor data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showMealOrder(req, res, next) {
  try {
    const data = await db.showMealOrder();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show meal order data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function searchMealOrder(req, res, next) {
  try {
    const data = await db.searchMealOrder(req.body); // Assuming parameters are passed in the request body
    const outputResponse = {
      status: true,
      message: "Data retrieved successfully",
      data: data,
    };
    res.status(200).json(outputResponse); // Send the retrieved data in the response
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({
      status: 500,
      message: "Internal server error",
      error: error.toString(),
    });
  }
}

async function updateMealOrder(req, res, next) {
  try {
    const data = await db.updateMealOrder(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "update meal order data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function addMealOrder(req, res, next) {
  try {
    const data = await db.addMealOrder(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "scan data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showUser(req, res, next) {
  try {
    const data = await db.showUser();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show user data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function addUser(req, res, next) {
  try {
    const data = await db.addUser(req.body);
    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function updateUser(req, res, next) {
  try {
    const data = await db.updateUser(req.body);
    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function deleteUser(req, res, next) {
  try {
    const data = await db.deleteUser(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "delete user data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function addCSVData(req, res, next) {
  try {
    const data = await db.addCSVData(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "add csv data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showBranch(req, res, next) {
  try {
    const data = await db.showBranch();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show branch data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showBranch(req, res, next) {
  try {
    const data = await db.showBranch();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show branch data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function showTap(req, res, next) {
  try {
    const data = await db.showTap();
    if (data.status == true) {
      output = {
        status: data.status,
        message: "show store tap data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function addTap(req, res, next) {
  try {
    const data = await db.addTap(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    } else {
      output = {
        status: data.status,
        message: data.message,
        data: [],
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function updateTap(req, res, next) {
  try {
    const data = await db.updateTap(req.body);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "update store tap data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function deleteTap(req, res, next) {
  const { id } = req.body;
  try {
    const data = await db.deleteTap(id);
    if (data.status == true) {
      output = {
        status: data.status,
        message: "delete store tap data",
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

const saveAttachment = async (req, res, next) => {
  try {
    const { requestId, filePath, uploader } = req.body;

    if (req.file) {
      const tempPath = req.file.path;
      const destinationPath = path.join(
        "public/sharing-file/",
        requestId + "-" + req.file.originalname
      );

      fs.rename(tempPath, destinationPath, async (err) => {
        if (err) {
          console.error("Error renaming file", err);
          return res.status(500).json({ message: "Internal server error" });
        }
      });
    }

    const data = await db.saveAttachment(
      requestId,
      req.file.originalname,
      filePath + requestId + "-" + req.file.originalname,
      uploader
    );

    if (data.status == true) {
      output = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      res.contentType("application/json").status(200);
      var valued = JSON.stringify(output);
      res.send(valued);
    }
  } catch (error) {
    output = {
      status: false,
      message: "Input failed",
    };
    res.contentType("application/json").status(500);
    var valued = JSON.stringify(output);
    res.send(valued);
  }
};

async function showBanner(req, res, next) {
  try {
    const data = await db.showBanner(req.body);
    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function userLogin(req, res, next) {
  try {
    // const data = await db.showUserType();
    // if (data.status == true) {
    //   output = {
    //     status: data.status,
    //     message: "show scan",
    //     data: data.data,
    //   };
    //   res.contentType("application/json").status(200);
    //   var valued = JSON.stringify(output);
    //   res.send(valued);
    // }
    output = {
      status: true,
      message: "show scan",
      data: [],
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function searchScan(req, res, next) {
  try {
    const data = await db.searchScan(req.body);
    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

async function insertBanner(req, res, next) {
  try {
    const data = await db.insertBanner(req.body);
    output = {
      status: data.status,
      message: data.message,
      data: data.data,
    };
    res.contentType("application/json").status(200);
    var valued = JSON.stringify(output);
    res.send(valued);
  } catch (error) {
    console.log("err");
    return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      error: HttpStatus.getReasonPhrase(
        HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
      ),
    });
  }
}

module.exports = {
  showUserType,
  addUserType,
  updateUserType,
  deleteUserType,
  showVendor,
  addVendor,
  updateVendor,
  deleteVendor,
  showMealOrder,
  searchMealOrder,
  addMealOrder,
  updateMealOrder,
  showUser,
  addUser,
  updateUser,
  deleteUser,
  addCSVData,
  showBranch,
  showTap,
  addTap,
  updateTap,
  deleteTap,
  saveAttachment,
  showScan,
  showBanner,
  userLogin,
  searchScan,
  insertBanner,
};
