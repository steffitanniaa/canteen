import Navigation from "@/components/navigation/Navigation";
import Head from "next/head";
import { useEffect, useState } from "react";
import CheckAuth from "../utils/checkAuth";
import LayoutTapPage from "@/layouts/LayoutTap";

const UserTypePage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const checkAuth = CheckAuth();

  useEffect(() => {
    if (checkAuth) {
      setIsLoading(false);
    } else {
      setIsLoading(true);
    }
  }, [checkAuth]);

  return (
    <div className="bg-white">
      <Head>
        <title>Canteen - PT. AEON Indonesia</title>
      </Head>

      {!isLoading && <Navigation content={<LayoutTapPage />} />}
    </div>
  );
};

export default UserTypePage;
