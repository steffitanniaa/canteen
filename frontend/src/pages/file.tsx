import ButtonTest from "@/components/button/ButtonTest";

const DataPage = () => {
  return (
    <div className="flex w-screen h-screen bg-red-100 justify-center items-center">
      <ButtonTest />
    </div>
  );
};

export default DataPage;
