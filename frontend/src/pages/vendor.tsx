import Navigation from "@/components/navigation/Navigation";
import Head from "next/head";
import { useEffect, useState } from "react";
import CheckAuth from "../utils/checkAuth";
import LayoutVendorPage from "@/layouts/LayoutVendor";

const VendorPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const checkAuth = CheckAuth();

  useEffect(() => {
    if (checkAuth) {
      setIsLoading(false);
    } else {
      setIsLoading(true);
    }
  }, [checkAuth]);

  return (
    <div className="bg-white">
      <Head>
        <title>Canteen - PT. AEON Indonesia</title>
      </Head>

      {!isLoading && <Navigation content={<LayoutVendorPage />} />}
    </div>
  );
};

export default VendorPage;
