import LayoutScan from "@/layouts/LayoutScan";
import Head from "next/head";

const DashboardPage = () => {
  return (
    <div className="bg-white">
      <Head>
        <title>Canteen - PT. AEON Indonesia</title>
      </Head>

      {<LayoutScan />}
    </div>
  );
};

export default DashboardPage;
