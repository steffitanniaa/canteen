import Navigation from "@/components/navigation/Navigation";
import Head from "next/head";
import { useEffect, useState } from "react";
import CheckAuth from "../utils/checkAuth";
import LayoutBannerPage from "@/layouts/LayoutBanner";

const BannerPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const checkAuth = CheckAuth();

  useEffect(() => {
    if (checkAuth) {
      setIsLoading(false);
    } else {
      setIsLoading(true);
    }
  }, [checkAuth]);

  return (
    <div className="bg-white">
      <Head>
        <title>Canteen - PT. AEON Indonesia</title>
      </Head>
      {!isLoading && <Navigation content={<LayoutBannerPage />} />}
    </div>
  );
};

export default BannerPage;
