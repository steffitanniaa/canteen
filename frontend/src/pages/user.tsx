import Navigation from "@/components/navigation/Navigation";
import Head from "next/head";
import { useEffect, useState } from "react";
import CheckAuth from "../utils/checkAuth";
import LayoutUserPage from "@/layouts/LayoutUser";

const UserPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const checkAuth = CheckAuth();

  useEffect(() => {
    if (checkAuth) {
      setIsLoading(false);
    } else {
      setIsLoading(true);
    }
  }, [checkAuth]);

  return (
    <div className="bg-white">
      <Head>
        <title>Canteen - PT. AEON Indonesia</title>
      </Head>
      {!isLoading && <Navigation content={<LayoutUserPage />} />}
    </div>
  );
};

export default UserPage;
