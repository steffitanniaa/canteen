import TextSubtitle from "@/components/text/TextSubtitleSecondary";
import Compact from "@/utils/compact";
import axios from "axios";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { BsCheckCircleFill, BsFillExclamationCircleFill } from "react-icons/bs";
import PulseLoader from "react-spinners/PulseLoader";
// import Confetti from "react-confetti";

const LayoutScan = (props: any) => {
  const endpoint = process.env.NEXT_PUBLIC_EP;

  const router = useRouter();
  // const {store} = router.query
  const [store, setStore] = useState<any>(null);
  const mobile = Compact();

  const [storeData, setStoreData] = useState<string | null>(null);
  const [rfidCode, setRfidCode] = useState("");
  const [showName, setShowName] = useState(false);
  const [user, setUser] = useState("");
  const [count, setCount] = useState(0);

  // NOTIF
  const [showNotification, setShowNotification] = useState(false);
  const [statNotif, setStatNotif] = useState(false);
  const [msgNotif, setMsgNotif] = useState("");

  const dummyCode = "0011663915";
  const primaryColor = "#ad2181";

  useEffect(() => {
    const canteenConfig = localStorage.getItem("canteenConfig");
    if (canteenConfig) {
      setStore(JSON.parse(canteenConfig));
    }
  }, []);

  useEffect(() => {
    if (typeof window !== "undefined") {
      const storedData = localStorage.getItem("storeData");
      setCount(parseInt(localStorage.getItem("visitorCount") ?? "") || 0);
      if (storedData) {
        setStoreData(storedData);
      }
    }
  }, []);

  const checkData = (code: string) => {
    if (code == dummyCode) {
      setShowName(true);
      setCount(count + 1);
      setStatNotif(true);
      setMsgNotif("");
      setShowNotification(true);

      setTimeout(() => {
        setShowName(false);
        setShowNotification(false);
        setRfidCode("");
      }, 4000);
    } else {
      setStatNotif(false);
      setMsgNotif("Data Not Found!");
      setShowNotification(true);
      setTimeout(() => {
        setShowNotification(false);
        setRfidCode("");
      }, 3000);
    }
  };

  const handleRfidChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRfidCode(event.target.value);
    checkData(event.target.value);
  };

  // const printContent = () => {
  //   const printWindow = window.open("", "_blank");

  //   if (!printWindow) {
  //     console.error("Failed to open print window");
  //     return;
  //   }

  //   printWindow.document.write("<html><head><title>Print</title></head><body>");
  //   printWindow.document.write("<h1>Printable Content</h1>");
  //   printWindow.document.write("<p>This is the content you want to print.</p>");
  //   printWindow.document.write("</body></html>");
  //   printWindow.document.close();
  //   printWindow.print();
  //   printWindow.close();
  // };

  // printContent();

  // printContent();

  return (
    <div className="flex flex-col h-screen w-screen justify-center items-center">
      {showNotification && (
        <div className={`absolute max-w-[80%] pt-2 top-10`}>
          {statNotif ? (
            <div className="flex flex-row bg-success justify-center items-center gap-3 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white text-[40px]">
                <BsCheckCircleFill />
              </span>
              <span className={`flex flex-col justify-center items-center`}>
                <div className="flex font-bold text-2xl text-white">
                  {"Tria Cahya Ramadhan"}
                </div>
                <div className="flex text-xl text-white">
                  {"25 March 2024 15:40"}
                </div>
              </span>
            </div>
          ) : (
            <div className="flex flex-row bg-error justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white text-[40px]">
                <BsFillExclamationCircleFill />
              </span>
              <span className={`flex flex-col justify-center items-center`}>
                <div className="flex font-bold text-2xl text-white">
                  {msgNotif}
                </div>
              </span>
            </div>
          )}
        </div>
      )}

      <div className="absolute w-full justify-between top-3 items-center">
        <div className="flex w-full justify-between px-5 items-center">
          <div className="text-lg text-secondary font-bold">
            CANTEEN MANAGEMENT SYSTEM
          </div>
          <div className="text-lg text-primary font-bold">
            STORE : {store && store[0].store}
          </div>
        </div>
      </div>

      <div className="flex flex-col w-2/3 justify-center items-center">
        <div className="flex flex-col w-full">
          <div className=" text-md text-gray-500">Employee RFID</div>
          <div className="flex border-2 border-gray-200 rounded-md flex-row p-1 mb-2">
            <input
              // onKeyDown={handleScanEnter}
              value={rfidCode}
              onChange={handleRfidChange}
              autoFocus
              className="appearance-none rounded text-sm w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              placeholder={`Enter employee RFID`}
            />
          </div>
        </div>

        <div className="flex flex-col w-full justify-center items-center mt-3">
          <div className=" text-[25px] font-bold text-primary">
            Today's Visitors
          </div>
          <div className=" font-bold text-[140px] text-primary -mt-10">
            {count}
          </div>
        </div>
      </div>
    </div>
  );
};

export default LayoutScan;
