import TextSubtitle from "@/components/text/TextSubtitleSecondary";
import Compact from "@/utils/compact";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { BsCheckCircleFill, BsFillExclamationCircleFill } from "react-icons/bs";
import PulseLoader from "react-spinners/PulseLoader";
import Confetti from "react-confetti";
import ReactEcharts from "echarts-for-react";
import Multiselect from "multiselect-react-dropdown";
// import { MultiSelect } from "react-multi-select-component";
import CountUp from "react-countup";

function getDate() {
  const today = new Date();
  const month = today.getMonth() + 1;
  const year = today.getFullYear();
  const date = today.getDate();
  return `${month}/${date}/${year}`;
}

const LayoutDashboard = (props: any) => {
  // const totalEmployees = 100;
  // const totalOrders = 500;
  // const totalVendors = 50;

  const options = [
    { name: "Option 1️⃣", id: 1 },
    { name: "Option 2️⃣", id: 2 },
  ];

  const endpoint = process.env.NEXT_PUBLIC_EP;
  const config = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
    },
  };

  const [store, setStore] = useState([]);
  console.log("Data Store:", store);
  const [storeSelect, setStoreSelect] = useState([]);

  const [x1, setX1] = useState([]);
  const [y1, setY1] = useState([]);

  const [x2, setX2] = useState([]);
  const [y2, setY2] = useState([]);

  const [x3, setX3] = useState([]);
  const [y3, setY3] = useState([]);

  const [totalEmployee, setTotalEmployees] = useState();

  const [totalOrder, setTotalOrders] = useState();

  const [totalVendor, setTotalVendors] = useState();

  const fetchAllData = useCallback(async () => {
    const storeSelected = {
      store: storeSelect,
    };
    console.log("storeSelected", storeSelected);

    try {
      const storeQuery = "canteen/store";
      const graphQuery = "canteen/graph";

      const storeResponse = await axios.post(endpoint + storeQuery, {}, config);
      const queryResponse = await axios.post(
        endpoint + graphQuery,
        storeSelected,
        config
      );

      console.log("storeResponse", storeResponse);
      console.log("query", queryResponse);

      if (storeResponse.data.data !== undefined) {
        setStore(storeResponse.data.data);
      }

      console.log("queryResponse", queryResponse);

      setX1(queryResponse.data.graph_1_axis);
      setY1(queryResponse.data.graph_1_ordinate);

      setX2(queryResponse.data.graph_2_axis);
      setY2(queryResponse.data.graph_2_ordinate);

      setX3(queryResponse.data.graph_3_axis);
      setY3(queryResponse.data.graph_3_ordinate);

      // setDataRole(queryResponse.data.graph_3_value)

      setTotalOrders(queryResponse.data.graph_4_value);

      setTotalEmployees(queryResponse.data.graph_5_value);

      setTotalVendors(queryResponse.data.graph_6_value);
    } catch (error) {
      // Handle error here
    }
  }, [storeSelect]);

  useEffect(() => {
    fetchAllData();
  }, [fetchAllData]);

  const option = {
    visualMap: [
      {
        show: false,
        type: "continuous",
        color: ["#ab2181"],
        seriesIndex: 0,
        min: 0,
        max: 20000,
      },
    ],
    title: {
      text: "Total Order by Hours",
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      data: x1,
      axisLabel: {
        fontSize: 15,
      },
    },
    yAxis: {
      type: "value",
      axisLabel: {
        fontSize: 15,
      },
    },
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "cross",
        label: {
          backgroundColor: "#ab2181",
        },
      },
    },

    series: [
      {
        name: "Orders",
        data: y1,
        type: "line",
        areaStyle: {
          color: "#ab2181",
          opacity: 0.4,
        },
        label: {
          show: true,
          fontSize: 10,
          formatter: function (params: any) {
            if (params.value >= 1000) {
              return (params.value / 1000).toFixed(0) + "K";
            }
          },
        },
        smooth: true,
      },
    ],
  };

  const option2 = {
    title: {
      text: "Order by Store",
      left: "left",
      top: "5%",
    },
    yAxis: {
      type: "category",
      data: x2,
    },
    xAxis: {
      type: "value",
      axisLabel: {
        formatter: function (params: any) {
          if (params.value >= 1000) {
            return (params.value / 1000).toFixed(0) + "K";
          }
        },
      },
    },
    tooltip: {
      trigger: "item",
    },
    series: [
      {
        color: "#ab2181",
        data: y2,
        type: "bar",
        label: {
          show: true,
          formatter: function (params: any) {
            if (params.value >= 1000) {
              return (params.value / 1000).toFixed(0) + "K";
            }
          },
          position: "right",
        },
      },
    ],
  };

  const option3 = {
    title: {
      text: "Employee Role",
      left: "left",
    },
    tooltip: {
      trigger: "item",
      formatter(param: any) {
        // correct the percentage
        return param.name + ": " + param.value + " (" + param.percent + "%)";
      },
    },
    legend: {
      top: "10%",
      orient: "horizontal",
      left: "center",
    },
    series: [
      {
        name: "Transaction From",
        type: "pie",
        radius: ["30%", "50%"],
        labelLine: {
          show: true,
        },
        data: [
          { value: y3[0], name: x3[0] },
          { value: y3[1], name: x3[1] },
          { value: y3[2], name: x3[2] },
          { value: y3[3], name: x3[3] },
          { value: y3[4], name: x3[4] },
          { value: y3[5], name: x3[5] },
          { value: y3[6], name: x3[6] },
          { value: y3[7], name: x3[7] },
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
        },
      },
    ],
  };

  return (
    // Total Data
    <div className="flex flex-col w-full mx-10 mb-5 mt-3">
      <div className="title space-y-4">
        <TextSubtitle text={"DAILY CANTEEN DASHBOARD"} />
      </div>
      <div className="flex item-center">
        <Multiselect
          placeholder={storeSelect.length > 0 ? "" : "Select Store"}
          options={store}
          selectedValues={storeSelect}
          onSelect={(e) => setStoreSelect(e)}
          onRemove={(e) => setStoreSelect(e)}
          displayValue="store"
          className={`text-sm my-3 item-center text-center text-gray-500 border border-1 border-primary rounded-md`}
          style={{
            chips: { background: "#ab2181" },
            searchBox: { border: "none" },
            inputField: {
              margin: "5px",
            },
          }}
        />

        {/* <MultiSelect
          options={store}
          value={storeSelect}
          onChange={setStoreSelect}
          className={`text-sm my-3 item-center text-gray-500 rounded-md w-[255px]`}
          labelledBy="Select Store"
        /> */}
      </div>

      <div className="shadow border-b border-gray-200 rounded-lg bg-neutral-50">
        <div className="text-center items-center mb-7 mt-7">
          <div className="flex flex-row justify-center gap-20 mx-1 mt-3 items-center text-gray-500">
            <div className="uppercase">
              <p className="text-sm font-medium mb-1">Total Employees</p>
              {totalEmployee !== undefined ? (
                <p className="text-4xl font-bold text-primary">
                  <CountUp end={totalEmployee} />
                </p>
              ) : (
                <p className="text-sm font-bold text-primary">Loading...</p>
              )}
              {/* <p className="text-3xl font-bold text-primary">{totalEmployee}</p> */}
            </div>

            <div className="uppercase">
              <p className="text-sm font-medium mb-1">Total Orders</p>
              {totalOrder !== undefined ? (
                <p className="text-4xl font-bold text-primary">
                  <CountUp end={totalOrder} />
                </p>
              ) : (
                <p className="text-sm font-bold text-primary">Loading...</p>
              )}
              {/* <p className="text-3xl font-bold text-primary">{totalOrder}</p> */}
            </div>

            <div className="uppercase">
              <p className="text-sm font-medium mb-1">Total Vendors</p>
              {totalVendor !== undefined ? (
                <p className="text-4xl font-bold text-primary">
                  <CountUp end={totalVendor} duration={2.5} />
                </p>
              ) : (
                <p className="text-sm font-bold text-primary">Loading...</p>
              )}
              {/* <p className="text-3xl font-bold text-primary">{totalVendor}</p> */}
            </div>
          </div>
        </div>
      </div>

      <div className="flex flex-wrap mt-7">
        <div className="w-full md:w-1/2 pr-7">
          <div id="g-1" className="mb-5">
            <ReactEcharts
              option={option}
              style={{ height: "276px", width: "100%" }}
              className={"border border-gray-200 rounded-xl p-2"}
            />
          </div>
          <div id="g-2">
            <ReactEcharts
              option={option2}
              style={{ height: "276px", width: "100%" }}
              className={"border border-gray-200 rounded-xl p-2"}
            />
          </div>
        </div>
        <div id="g-3" className="w-full md:w-1/2">
          <ReactEcharts
            option={option3}
            style={{ height: "570px", width: "100%" }}
            className={"border border-gray-200 rounded-xl p-2"}
          />
        </div>
      </div>
    </div>
  );
};

export default LayoutDashboard;
