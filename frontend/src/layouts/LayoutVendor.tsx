// @ts-nocheck
import TextSubtitle from "@/components/text/TextSubtitleSecondary";
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight,
} from "react-icons/fi";
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";
import Compact from "@/utils/compact";
import { BsFillPencilFill, BsTrashFill } from "react-icons/bs";
import { MdCancel } from "react-icons/md";
import { FaMagnifyingGlass } from "react-icons/fa6";
import router from "next/router";
import { useCallback, useEffect, useRef, useState } from "react";
import { BsCheckCircleFill, BsFillExclamationCircleFill } from "react-icons/bs";
import axios from "axios";
import { utils, writeFile } from "xlsx";

const LayoutVendorPage = (props: any) => {
  const config = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
    },
  };

  const [vendorId, setVendorId] = useState("");
  const [storeCode, setStoreCode] = useState("");
  const [vendorName, setVendorName] = useState("");
  const [vendorValidFrom, setVendorValidFrom] = useState("");
  const [vendorValidTo, setVendorValidTo] = useState("");
  const [vendorContract, setVendorContract] = useState("");
  const [mealPrice, setMealPrice] = useState("");

  const [branchOptions, setBranchOptions] = useState([]);

  const clear = () => {
    setVendorId("");
    setStoreCode("");
    setVendorName("");
    setVendorValidFrom("");
    setVendorValidTo("");
    setVendorContract("");
    setMealPrice("");
  };

  const { menu, sub } = router.query;
  const type = props.type;
  const mobile = Compact();
  const endpoint = process.env.NEXT_PUBLIC_EP;

  const dateFormatAlt = (dateString: any) => {
    const originalDate = new Date(dateString);
    originalDate.setDate(originalDate.getDate() + 1);

    const formattedDate = originalDate.toISOString().split("T")[0];
    return formattedDate;
  };

  function formatDateTime(date: any, time: any) {
    const dateOpt = { year: "numeric", month: "2-digit", day: "2-digit" };
    const timeOpt = {
      hour12: false,
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    };

    const formatDate = date
      .toLocaleDateString("en-US", dateOpt)
      .replace(/\//g, "");
    const formatTime = time
      .toLocaleTimeString("en-US", timeOpt)
      .replace(/:/g, "");

    const year = formatDate.slice(4, 8);
    const month = formatDate.slice(0, 2);
    const day = formatDate.slice(2, 4);

    const hour = formatTime.slice(0, 2);
    const minute = formatTime.slice(2, 4);
    const second = formatTime.slice(4, 6);

    // return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    return year + month + day + hour + minute + second;
  }

  // Variable fetch
  const [data, setData] = useState([]);
  const [allData, setAllData] = useState([]);

  // Variable search
  const [search, setSearch] = useState("");

  // Variable pagination
  const pageNumbers = [];
  const maxPageLinks = 5;
  const [currentPage, setCurrentPage] = useState(0);
  const [dataPerPage, setDataPerPage] = useState(5);
  const startIndex = currentPage * dataPerPage;
  const endIndex = startIndex + dataPerPage;
  const pageData = allData.slice(startIndex, endIndex);
  const totalPages = Math.ceil(allData.length / dataPerPage);
  let startPage = currentPage - Math.floor(maxPageLinks / 2);

  // Variable sorting
  const [sortOrder, setSortOrder] = useState("asc");
  const [sortColumn, setSortColumn] = useState("name");

  // Variable notification
  const [showNotification, setShowNotification] = useState(false);
  const [statNotif, setStatNotif] = useState(true);
  const [msgNotif, setMsgNotif] = useState("");

  const [userData, setUserData] = useState(
    JSON.parse(localStorage.getItem("canteenDataDetail")!)
  );

  const initialState = () => {};

  function convertTime(time: any) {
    const utcDate = new Date(time);

    const localOffset = utcDate.getTimezoneOffset();

    const gmtPlus7Offset = 420;

    const localTime = new Date(
      utcDate.getTime() + (localOffset + gmtPlus7Offset) * 60000
    );

    const year = localTime.getFullYear();
    const month = localTime.getMonth() + 1;
    const day = localTime.getDate();
    const hours = localTime.getHours();
    const minutes = localTime.getMinutes();
    const seconds = localTime.getSeconds();

    const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${day
      .toString()
      .padStart(2, "0")}`;
    const formattedTime = `${hours.toString().padStart(2, "0")}:${minutes
      .toString()
      .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;

    const formattedDateTime = `${formattedDate} ${formattedTime}`;

    return formattedDateTime;
  }

  const createNewRequest = () => {
    console.log("Create New Request");
  };

  const fetchAllData = useCallback(async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
        },
      };

      const reqId = {
        id: router.query.id,
      };

      const dataVendor = await axios.post(
        endpoint + "canteen/show-vendor",
        {},
        config
      );

      setData(dataVendor.data.data);
      // console.log(dataVendor.data.data);
      // console.log("ok");
    } catch (error) {
      // Handle error here
    }
  }, [router.query, endpoint]);

  useEffect(() => {
    fetchAllData();
  }, [fetchAllData]);

  // Pagination
  const changePageRow = (curPage: any) => {
    setDataPerPage(curPage);
    setCurrentPage(0);
  };

  if (startPage < 1) {
    startPage = 1;
  }

  let endPage = startPage + maxPageLinks - 1;
  if (endPage > totalPages) {
    endPage = totalPages;
    startPage = endPage - maxPageLinks + 1;
    if (startPage < 1) {
      startPage = 1;
    }
  }

  for (let i = startPage; i <= endPage; i++) {
    pageNumbers.push(i);
  }

  useEffect(() => {
    let sortedData = [...data];
    if (sortColumn && sortOrder) {
      sortedData = sortedData.sort(sortData(sortColumn, sortOrder));
    }
    setAllData(sortedData);
  }, [data, sortColumn, sortOrder]);

  // Sorting
  function sortData(column: string, order: string) {
    return function (a: any, b: any) {
      const aValue = a[column];
      const bValue = b[column];

      if (aValue < bValue) {
        return order === "asc" ? -1 : 1;
      } else if (aValue > bValue) {
        return order === "asc" ? 1 : -1;
      } else {
        return 0;
      }
    };
  }
  function handleSort(column: string) {
    const newSortOrder =
      column === sortColumn ? (sortOrder === "asc" ? "desc" : "asc") : "asc";
    setSortOrder(newSortOrder);
    setSortColumn(column);
  }

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await axios.post(
          endpoint + "canteen/show-branch",
          data,
          config
        );
        if (response.data.status) {
          setBranchOptions(response.data.data);
        } else {
          console.error("Failed to fetch branch data:", response.data.message);
        }
      } catch (error) {
        console.error("Error fetching branch data:", error);
      }
    }

    fetchData();
  }, []);

  const [searchCriteria, setSearchCriteria] = useState({
    storeCode: "",
    vendorValidFrom: "",
    vendorValidTo: "",
  });

  const handleSearchStore = () => {
    // Ambil nilai dari input
    const { storeCode, vendorValidFrom, vendorValidTo } = searchCriteria;

    // Filter data berdasarkan kriteria pencarian
    const filteredData = branchOptions.filter((branch) => {
      let isValid = true;
      if (storeCode && branch.mcdb_store_code !== storeCode) {
        isValid = false;
      }
      if (vendorValidFrom && branch.mven_valid_from !== vendorValidFrom) {
        isValid = false;
      }
      if (vendorValidTo && branch.mven_valid_to !== vendorValidTo) {
        isValid = false;
      }
      return isValid;
    });

    // Update data yang ditampilkan sesuai dengan hasil filter
    setFilteredBranchOptions(filteredData);
  };

  // SUBMIT
  const submit = async () => {
    if (
      (storeCode,
      vendorName,
      vendorValidFrom,
      vendorValidTo,
      mealPrice,
      vendorContract)
    ) {
      const data = [
        {
          mven_vendor_id: vendorId,
          mven_store: storeCode,
          mven_vendor_name: vendorName,
          mven_valid_from: vendorValidFrom,
          mven_valid_to: vendorValidTo,
          mven_meal_price: mealPrice,
          mven_vendor_contract: vendorContract,
        },
      ];

      if (vendorId) {
        const response = await axios.post(
          endpoint + "canteen/update-vendor",
          data,
          config
        );

        if (response && response.data && response.data.status) {
          setStatNotif(true);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);
        } else {
          setStatNotif(false);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);
        }
      } else {
        try {
          console.log("add vendor", data);
          const response = await axios.post(
            endpoint + "canteen/add-vendor",
            data,
            config
          );
        } catch (e) {
          console.log("err", e);
        }
        setStatNotif(true);
        setMsgNotif("Data Added Successfully!");
        setShowNotification(true);

        setTimeout(() => {
          setShowNotification(false);
        }, 1000);
      }
    } else {
      setStatNotif(false);
      setMsgNotif("Incomplete Data!");
      setShowNotification(true);

      setTimeout(() => {
        setShowNotification(false);
      }, 1000);
    }

    clear();
    fetchAllData();
  };

  // Search
  useEffect(() => {
    const fetchData = async () => {
      const res = await data;
      if (search) {
        const searchKeywords = search.toLowerCase().split(" ");
        const filteredData = res.filter((item) => {
          //   return searchKeywords.every((keyword) => {
          return searchKeywords.some((keyword) => {
            const objectValues = Object.values(item).join(" ").toLowerCase();
            return objectValues.includes(keyword);
          });
        });
        setAllData(filteredData);
      } else {
        setAllData(res);
      }
      setCurrentPage(0);
    };

    fetchData();
  }, [data, search]);

  const handleNotification = () => {
    setShowNotification(true);
    setTimeout(() => {
      setShowNotification(false);
    }, 3000);
  };

  const editShow = (i: any) => {
    initialState();
  };

  const updateData = (param: any) => {
    clear();
    console.log(param);
    setVendorId(param.mven_vendor_id);
    setStoreCode(param.mven_store);
    setVendorName(param.mven_vendor_name);
    setVendorValidFrom(param.mven_valid_from);
    setVendorValidTo(param.mven_valid_to);
    setVendorContract(param.mven_vendor_contract);
    setMealPrice(param.mven_meal_price);
  };

  function getInitials(name: string) {
    const names = name.split(" ");
    const initials = names.map((name) => name.charAt(0).toUpperCase());
    return initials.join("");
  }

  const TextInitial = (text: any) => {
    var getInitials = function (text: string) {
      var names = text.split(" "),
        initials = names[0].substring(0, 1).toUpperCase();

      if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
      }
      return initials;
    };

    return getInitials(text);
  };
  const [detail, setDetail] = useState<any[]>([]);

  const deleteItem = (index: number) => {
    const updatedBuffer = detail.slice();
    updatedBuffer.splice(index, 1);
    setDetail(updatedBuffer);
    clear();
  };

  const exportToExcel = useCallback(() => {
    const now = new Date();
    const formattedDateTime = formatDateTime(now, now);
    const ws = utils.json_to_sheet(allData);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, "Data");
    writeFile(wb, "Data-Vendors " + formattedDateTime + ".xlsx");
  }, [allData]);

  return (
    <div className="flex flex-col w-full h-full px-5">
      {showNotification && (
        <div className={`absolute max-w-[80%] pt-2 right-5`}>
          {statNotif ? (
            <div className="flex flex-row bg-success justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white">
                <BsCheckCircleFill />
              </span>
              <span className={`${mobile && "text-xs"} text-white`}>
                {msgNotif}
              </span>
            </div>
          ) : (
            <div className="flex flex-row bg-error justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white">
                <BsFillExclamationCircleFill />
              </span>
              <span className={`${mobile && "text-xs"} text-white`}>
                {msgNotif}
              </span>
            </div>
          )}
        </div>
      )}
      <div className="flex flex-row justify-between mx-3 mt-3 mb-5">
        <TextSubtitle text={"VENDOR TABLE"} />
        <div className="justify-end mt-3 mb-5">
          <label
            onClick={clear}
            htmlFor="modal-add-vendor"
            className="mr-2 rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
          >
            Add Vendor
          </label>
          <label
            onClick={exportToExcel}
            className="rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
          >
            Export Data
          </label>
        </div>
      </div>

      <div
        className={`flex ${
          mobile
            ? "flex-col gap-2 items-center mt-3"
            : "flex-row justify-between"
        } mx-3 mb-3`}
      >
        <div className="flex flex-row items-center gap-2">
          <h1 className="text-sm text-secondary">Show</h1>
          <select
            className="select select-primary select-sm w-16 text-secondary font-bold"
            defaultValue={dataPerPage}
            onChange={(e) => changePageRow(e.target.value)}
          >
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={25}>25</option>
            <option value={50}>50</option>
          </select>
          <h1 className="text-sm text-secondary">entries</h1>
        </div>

        <input
          type="text"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          className={`text-sm ${
            mobile
              ? "input-md text-secondary w-full max-w-full"
              : "input-sm w-full max-w-sm"
          } input-primary input border border-1 text-primary border-primary`}
        />
      </div>
      <div className="shadow overflow-hidden border-b border-gray-200 rounded-lg flex flex-1 mx-3">
        <table className="min-w-full divide-y divide-gray-200 table-fixed">
          <thead className="bg-gray-50 sticky top-0">
            <tr className={`${!mobile && "hidden"}`}>
              <th
                onClick={() => handleSort("rnp_id")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Request
                  {sortColumn === "rnp_id" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>
            </tr>
            <tr className={`${mobile && "hidden"}`}>
              {/* # */}

              <th
                scope="col"
                className={`w-[20px] cursor-pointer px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  #
                </div>
              </th>

              {/* STORE CODE */}

              <th
                onClick={() => handleSort("mven_store")}
                scope="col"
                className={`w-[20px] cursor-pointer px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Store Code
                  {sortColumn === "mven_store" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* VENDOR NAME */}

              <th
                onClick={() => handleSort("mven_vendor_name")}
                scope="col"
                className={`w-[150px] cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Vendor Name
                  {sortColumn === "mven_vendor_name" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* PRICE */}

              <th
                onClick={() => handleSort("mven_meal_price")}
                scope="col"
                className={`w-[150px] cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Meal Price
                  {sortColumn === "mven_meal_price" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* VALID FROM */}

              <th
                onClick={() => handleSort("mven_valid_from")}
                scope="col"
                className={`w-[150px] cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Valid From
                  {sortColumn === "mven_valid_from" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* VALID TO */}

              <th
                onClick={() => handleSort("mven_valid_to")}
                scope="col"
                className={`w-[150px] cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Valid To
                  {sortColumn === "mven_valid_to" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* VENDOR CONTRACT */}

              <th
                onClick={() => handleSort("mven_vendor_contract")}
                scope="col"
                className={`w-[150px] cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Contract
                  {sortColumn === "mven_vendor_contract" &&
                  sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* ACTION */}

              <th
                scope="col"
                className={`w-[100px] cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex text-xs font-medium text-gray-500 uppercase">
                  Action
                </div>
              </th>
            </tr>
          </thead>

          <tbody className="bg-white divide-y divide-gray-200">
            {pageData.map((item, index) => (
              <tr
                className="transition ease-in-out hover:bg-neutral-50"
                key={index}
              >
                <td className="px-6 py-4 text-sm text-gray-500 w-[20px]">
                  {startIndex + index + 1}
                </td>
                <td className="items-center flex flex-row justify-between px-6 py-4 text-sm text-gray-500">
                  {item.store_name}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500">
                  {item.mven_vendor_name}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500">
                  <div>
                    Rp. {""}
                    {item?.mven_meal_price.toLocaleString(undefined, {
                      maximumFractionDigits: 2,
                      minimumFractionDigits: 2,
                    })}
                  </div>
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500">
                  {dateFormatAlt(item.mven_valid_from)}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500">
                  {dateFormatAlt(item.mven_valid_to)}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500">
                  {item.mven_vendor_contract}
                </td>

                <td>
                  <div className="false flex flex-row gap-1 mr-5 w-[75px]">
                    <label
                      onClick={() => {
                        updateData(item);
                      }}
                      for="modal-add-vendor"
                      className="flex w-8 h-8 bg-primary rounded border border-1 border-primary cursor-pointer justify-center items-center text-white"
                    >
                      <BsFillPencilFill />
                    </label>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="flex flex-row justify-between mx-3 mt-3 items-center text-gray-500">
        <p className="text-sm text-secondary-focus">
          Showing data {(currentPage + 1) * dataPerPage - dataPerPage + 1} -{" "}
          {allData.length / ((currentPage + 1) * dataPerPage) >= 1
            ? (currentPage + 1) * dataPerPage
            : (allData.length % dataPerPage) + currentPage * dataPerPage}{" "}
          of {allData.length}
        </p>

        <div className="flex btn-group justify-end items-center gap-3">
          <p className={`text-sm text-secondary-focus`}>Page :</p>
          <input
            type="number"
            value={currentPage + 1}
            min={1}
            max={Math.ceil(allData.length / dataPerPage)}
            onChange={(e) => {
              parseInt(e.target.value) <=
                Math.ceil(allData.length / dataPerPage) &&
                e.target.value &&
                setCurrentPage(parseInt(e.target.value) - 1);
            }}
            className={`text-sm ${
              mobile ? "input-md" : "input-sm"
            } w-16 input-primary input border border-1 border-primary h-8`}
          />
          <p className={`text-sm text-secondary-focus`}>
            of {Math.ceil(allData.length / dataPerPage)}
          </p>
        </div>
      </div>
      <div className="flex m-3 w-full items-center justify-center gap-1">
        <FiChevronsLeft
          onClick={() => setCurrentPage(0)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronLeft
          onClick={() => currentPage > 0 && setCurrentPage(currentPage - 1)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        {pageNumbers.map((number) => (
          <button
            key={number}
            onClick={() => {
              setCurrentPage(number - 1);
            }}
            className={`${
              number - 1 === currentPage
                ? "bg-primary text-white"
                : "bg-white text-primary"
            } btn btn-sm border rounded-md w-10 border-gray-200 hover:bg-primary-focus hover:text-white hover:border-primary-focus`}
          >
            {number}
          </button>
        ))}
        <FiChevronRight
          onClick={() =>
            currentPage < Math.ceil(allData.length / dataPerPage) - 1 &&
            setCurrentPage(currentPage + 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronsRight
          onClick={() =>
            setCurrentPage(Math.ceil(allData.length / dataPerPage) - 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
      </div>
      <input type="checkbox" id="modal-add-vendor" className="modal-toggle" />
      <div className="modal" role="dialog">
        <div className="modal-box">
          <div className="flex flex-row justify-between items-center mb-3">
            <h1 className="text-sm font-bold text-secondary">
              {vendorId ? "UPDATE VENDOR" : "CREATE NEW VENDOR"}
            </h1>
            <div className="flex flex-row items-center gap-2">
              <label
                className="text-primary cursor-pointer"
                htmlFor="modal-add-vendor"
              >
                <MdCancel className="w-7 h-7" />
              </label>
            </div>
          </div>

          <select
            className="select select-sm select-bordered w-full mb-3 text-xs text-secondary"
            id="store"
            name="store"
            value={storeCode}
            onChange={(e) => setStoreCode(e.target.value)}
          >
            <option value="">Select Store</option>
            {branchOptions.map((branch) => (
              <option
                key={branch.mcdb_store_code}
                value={branch.mcdb_store_code}
              >
                {`${branch.mcdb_store_code} - ${branch.mcdb_flag_source}`}
              </option>
            ))}
          </select>

          <div className="flex">
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="vendor-name"
              >
                Vendor Name
              </label>
              <input
                type="text"
                name="vendor-name"
                id="vendor-name"
                placeholder="Vendor Name"
                className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
                value={vendorName}
                onChange={(e) => setVendorName(e.target.value)}
              ></input>
            </div>
          </div>

          <div className="flex">
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="meal-price"
              >
                Meal Price
              </label>
              <input
                type="number"
                name="meal-price"
                id="meal-price"
                placeholder="Meal Price"
                className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
                value={mealPrice}
                onChange={(e) => setMealPrice(e.target.value)}
              ></input>
            </div>
          </div>

          <div className="flex gap-3">
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="valid-from"
              >
                Valid From
              </label>
              <input
                type="date"
                name="valid-from"
                id="valid-from"
                className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
                value={vendorValidFrom}
                onChange={(e) => setVendorValidFrom(e.target.value)}
              ></input>
            </div>
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="valid-to"
              >
                Valid To
              </label>
              <input
                type="date"
                name="valid-from"
                id="valid-from"
                className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
                value={vendorValidTo}
                onChange={(e) => setVendorValidTo(e.target.value)}
              ></input>
            </div>
          </div>

          <div className="flex flex-col w-full">
            <label
              className="block text-secondary text-xs text mb-1"
              htmlFor="contract"
            >
              Contract
            </label>
            <input
              type="text-area"
              name="valid-from"
              id="valid-from"
              className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
              value={vendorContract}
              onChange={(e) => setVendorContract(e.target.value)}
            ></input>
          </div>
          <label
            htmlFor="modal-add-vendor"
            className="btn btn-md w-full text-white bg-primary border-transparent hover:bg-secondary"
            onClick={submit}
          >
            Submit
          </label>
        </div>
      </div>
    </div>
  );
};

export default LayoutVendorPage;
