// @ts-nocheck

import TextSubtitle from "@/components/text/TextSubtitleSecondary";
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight,
} from "react-icons/fi";
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";
import Compact from "@/utils/compact";
import { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { BsFillPencilFill, BsTrashFill } from "react-icons/bs";
import { FaMagnifyingGlass } from "react-icons/fa6";
import Link from "next/link";
import router from "next/router";
import userTypeData from "@/data/UserTypeData";
import { BsCheckCircleFill, BsFillExclamationCircleFill } from "react-icons/bs";

const LayoutUserTypePage = (props: any) => {
  const [userDesc, setUserDesc] = useState("");
  const [userTypeID, setUserTypeID] = useState("");
  const [statusEdit, setStatusEdit] = useState(false);

  const { menu, sub } = router.query;
  const type = props.type;
  const mobile = Compact();
  const endpoint = process.env.NEXT_PUBLIC_EP;
  const [userData, setUserData] = useState(
    JSON.parse(localStorage.getItem("canteenDataDetail")!)
  );

  // Variable fetch
  const [data, setData] = useState([]);
  const [allData, setAllData] = useState([]);

  // Variable search
  const [search, setSearch] = useState("");

  // Variable pagination
  const pageNumbers = [];
  const maxPageLinks = 5;
  const [currentPage, setCurrentPage] = useState(0);
  const [dataPerPage, setDataPerPage] = useState(5);
  const startIndex = currentPage * dataPerPage;
  const endIndex = startIndex + dataPerPage;
  const pageData = allData.slice(startIndex, endIndex);
  const totalPages = Math.ceil(allData.length / dataPerPage);
  let startPage = currentPage - Math.floor(maxPageLinks / 2);

  // Variable sorting
  const [sortOrder, setSortOrder] = useState("asc");
  const [sortColumn, setSortColumn] = useState("name");

  // Variable notification
  const [showNotification, setShowNotification] = useState(false);
  const [statNotif, setStatNotif] = useState(true);
  const [msgNotif, setMsgNotif] = useState("");

  const initialState = () => {};

  const fetchAllData = useCallback(async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
        },
      };

      const reqId = {
        id: router.query.id,
      };

      const dataVendor = await axios.post(
        endpoint + "canteen/show-user-type",
        {},
        config
      );

      setData(dataVendor.data.data);
    } catch (error) {
      // Handle error here
    }
  }, [router.query, endpoint]);

  useEffect(() => {
    fetchAllData();
  }, [fetchAllData]);

  // Pagination
  const changePageRow = (curPage: any) => {
    setDataPerPage(curPage);
    setCurrentPage(0);
  };

  if (startPage < 1) {
    startPage = 1;
  }

  let endPage = startPage + maxPageLinks - 1;
  if (endPage > totalPages) {
    endPage = totalPages;
    startPage = endPage - maxPageLinks + 1;
    if (startPage < 1) {
      startPage = 1;
    }
  }

  for (let i = startPage; i <= endPage; i++) {
    pageNumbers.push(i);
  }

  useEffect(() => {
    let sortedData = [...data];
    if (sortColumn && sortOrder) {
      sortedData = sortedData.sort(sortData(sortColumn, sortOrder));
    }
    setAllData(sortedData);
  }, [data, sortColumn, sortOrder]);

  // Sorting
  function sortData(column: string, order: string) {
    return function (a: any, b: any) {
      const aValue = a[column];
      const bValue = b[column];

      if (aValue < bValue) {
        return order === "asc" ? -1 : 1;
      } else if (aValue > bValue) {
        return order === "asc" ? 1 : -1;
      } else {
        return 0;
      }
    };
  }

  function handleSort(column: string) {
    const newSortOrder =
      column === sortColumn ? (sortOrder === "asc" ? "desc" : "asc") : "asc";
    setSortOrder(newSortOrder);
    setSortColumn(column);
  }

  // Search
  useEffect(() => {
    const fetchData = async () => {
      const res = await data;
      if (search) {
        const searchKeywords = search.toLowerCase().split(" ");
        const filteredData = res.filter((item) => {
          //   return searchKeywords.every((keyword) => {
          return searchKeywords.some((keyword) => {
            const objectValues = Object.values(item).join(" ").toLowerCase();
            return objectValues.includes(keyword);
          });
        });
        setAllData(filteredData);
      } else {
        setAllData(res);
      }
      setCurrentPage(0);
    };

    fetchData();
  }, [data, search]);

  const editShow = (i: any) => {
    initialState();
  };

  const showDetailUser = async (param) => {
    try {
      setUserDesc(param.muty_type_name);
      setUserTypeID(param.muty_type_id);
      setStatusEdit(true);
    } catch (error) {
      // Tangani kesalahan di sini
      console.error("Error fetching data:", error);
      throw new Error("Failed to fetch data from API");
    }
  };

  const submitData = async () => {
    try {
      if (statusEdit) {
        const config = {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
          },
        };

        const reqId = {
          muty_type_name: userDesc.trim(),
          muty_type_id: userTypeID.trim(),
        };

        const response = await axios.post(
          `${endpoint}canteen/update-user-type`,
          reqId,
          config
        );

        if (response.data.status) {
          setStatNotif(true);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);

          fetchAllData();

          setUserDesc("");
          setUserTypeID("");
        } else {
          setStatNotif(false);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);
        }
      } else {
        if (!userDesc.trim()) {
          setStatNotif(false);
          setMsgNotif("userDesc must be fill");
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);
          return;
        }

        const config = {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
          },
        };

        const reqId = {
          muty_type_name: userDesc.trim(),
        };

        const response = await axios.post(
          `${endpoint}canteen/add-user-type`,
          reqId,
          config
        );

        if (response.data.status) {
          setStatNotif(true);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);

          fetchAllData();

          setUserDesc("");
          setUserTypeID("");
        } else {
          setStatNotif(false);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 3000);
        }
      }
    } catch (error) {
      // Tangani kesalahan di sini
      console.error("Error fetching data:", error);
      // throw new Error("Failed to fetch data from API");
    }
  };

  return (
    <div className="flex flex-col w-full h-full px-5">
      {showNotification && (
        <div className={`absolute max-w-[80%] pt-2 right-5`}>
          {statNotif ? (
            <div className="flex flex-row bg-success justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white">
                <BsCheckCircleFill />
              </span>
              <span className={`${mobile && "text-xs"} text-white`}>
                {msgNotif}
              </span>
            </div>
          ) : (
            <div className="flex flex-row bg-error justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white">
                <BsFillExclamationCircleFill />
              </span>
              <span className={`${mobile && "text-xs"} text-white`}>
                {msgNotif}
              </span>
            </div>
          )}
        </div>
      )}

      <div className="flex flex-row justify-between mx-3 mt-3 mb-5">
        <TextSubtitle text={"USER TYPE TABLE"} />
        <label
          for="modal-add-user-type"
          className="rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
        >
          Add User Type
        </label>
      </div>

      <div
        className={`flex ${
          mobile
            ? "flex-col gap-2 items-center mt-3"
            : "flex-row justify-between"
        } mx-3 mb-3`}
      >
        <div className="flex flex-row items-center gap-2">
          <h1 className="text-sm text-secondary">Show</h1>
          <select
            className="select select-primary select-sm w-16 text-secondary font-bold"
            defaultValue={dataPerPage}
            onChange={(e) => changePageRow(e.target.value)}
          >
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={25}>25</option>
            <option value={50}>50</option>
          </select>
          <h1 className="text-sm text-secondary">entries</h1>
        </div>
        <input
          type="text"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          className={`text-sm ${
            mobile
              ? "input-md text-secondary w-full max-w-full"
              : "input-sm w-full max-w-sm"
          } input-primary input border border-1 text-primary border-primary`}
        />
      </div>

      <div className="shadow overflow-hidden border-b border-gray-200 rounded-lg overflow-y-auto flex flex-1 mx-3">
        <table className="min-w-full divide-y divide-gray-200 table-fixed">
          <thead className="bg-gray-50 sticky top-0">
            <tr className={`${!mobile && "hidden"}`}>
              <th
                onClick={() => handleSort("rnp_id")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Request
                  {sortColumn === "rnp_id" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>
            </tr>
            <tr className={`${mobile && "hidden"} justify-center items-center`}>
              <th
                className={`w-[20px] cursor-pointer px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                #
              </th>

              {/* USER TYPE DESC */}

              <th
                onClick={() => handleSort("muty_type_name")}
                scope="col"
                className={`cursor-pointer w-full py-3 px-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  User Type
                  {sortColumn === "muty_type_name" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* ACTION */}

              <th
                scope="col"
                className={`py-3 px-3 w-10 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  Action
                </div>
              </th>
            </tr>
          </thead>

          <tbody className="bg-white divide-y divide-gray-200">
            {pageData.map((item, index) => (
              <tr
                className="transition ease-in-out hover:bg-neutral-50 justify-center items-center"
                key={index}
              >
                <td className="px-6 py-4 text-sm text-gray-500 w-[20px]">
                  {startIndex + index + 1}
                </td>

                <td className="py-4 whitespace-nowrap text-sm text-gray-500">
                  {item.muty_type_name}
                </td>

                <td>
                  <div className="flex flex-row gap-1 mr-5 w-[75px] items-center justify-center">
                    <label
                      for="modal-add-user-type"
                      className="flex w-8 h-8 bg-primary rounded border border-1 border-primary cursor-pointer justify-center items-center text-white"
                      onClick={() => showDetailUser(item)}
                    >
                      <BsFillPencilFill />
                    </label>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      <div className="flex flex-row justify-between mx-3 mt-3 items-center text-gray-500">
        <p className="text-sm text-secondary-focus">
          Showing data {(currentPage + 1) * dataPerPage - dataPerPage + 1} -{" "}
          {allData.length / ((currentPage + 1) * dataPerPage) >= 1
            ? (currentPage + 1) * dataPerPage
            : (allData.length % dataPerPage) + currentPage * dataPerPage}{" "}
          of {allData.length}
        </p>

        <div className="flex btn-group justify-end items-center gap-3">
          <p className={`text-sm text-secondary-focus`}>Page :</p>
          <input
            type="number"
            value={currentPage + 1}
            min={1}
            max={Math.ceil(allData.length / dataPerPage)}
            onChange={(e) => {
              parseInt(e.target.value) <=
                Math.ceil(allData.length / dataPerPage) &&
                e.target.value &&
                setCurrentPage(parseInt(e.target.value) - 1);
            }}
            className={`text-sm ${
              mobile ? "input-md" : "input-sm"
            } w-16 input-primary input border border-1 border-primary h-8`}
          />
          <p className={`text-sm text-secondary-focus`}>
            of {Math.ceil(allData.length / dataPerPage)}
          </p>
        </div>
      </div>

      <div className="flex m-3 w-full items-center justify-center gap-1">
        <FiChevronsLeft
          onClick={() => setCurrentPage(0)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronLeft
          onClick={() => currentPage > 0 && setCurrentPage(currentPage - 1)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        {pageNumbers.map((number) => (
          <button
            key={number}
            onClick={() => {
              setCurrentPage(number - 1);
            }}
            className={`${
              number - 1 === currentPage
                ? "bg-primary text-white"
                : "bg-white text-primary"
            } btn btn-sm border rounded-md w-10 border-gray-200 hover:bg-primary-focus hover:text-white hover:border-primary-focus`}
          >
            {number}
          </button>
        ))}
        <FiChevronRight
          onClick={() =>
            currentPage < Math.ceil(allData.length / dataPerPage) - 1 &&
            setCurrentPage(currentPage + 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronsRight
          onClick={() =>
            setCurrentPage(Math.ceil(allData.length / dataPerPage) - 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
      </div>

      <input
        type="checkbox"
        id="modal-add-user-type"
        className="modal-toggle"
      />
      <div className="modal" role="dialog">
        <div className="modal-box">
          <div className="flex flex-row justify-between items-center mb-3">
            <h1 className="text-sm font-bold text-secondary">
              CREATE NEW USER TYPE
            </h1>
            <div className="flex flex-row items-center gap-2">
              <label
                className="text-primary cursor-pointer"
                htmlFor="modal-add-user-type"
              >
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  stroke-width="0"
                  viewBox="0 0 24 24"
                  className="w-7 h-7"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12C22 17.5228 17.5228 22 12 22ZM12 10.5858L9.17157 7.75736L7.75736 9.17157L10.5858 12L7.75736 14.8284L9.17157 16.2426L12 13.4142L14.8284 16.2426L16.2426 14.8284L13.4142 12L16.2426 9.17157L14.8284 7.75736L12 10.5858Z"></path>
                </svg>
              </label>
            </div>
          </div>

          <div className="flex">
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="user-desc"
              >
                User Type
              </label>
              <input
                type="text"
                name="user-type-desc"
                id="user-type-desc"
                placeholder="User Type Desc"
                className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
                value={userDesc}
                onChange={(e) => setUserDesc(e.target.value)}
              ></input>
            </div>
          </div>

          <label
            htmlFor="modal-add-user-type"
            className="btn btn-md w-full text-white bg-primary border-transparent hover:bg-secondary"
            onClick={submitData}
          >
            Submit
          </label>
        </div>
      </div>
    </div>
  );
};

export default LayoutUserTypePage;
