// @ts-nocheck
import TextSubtitle from "@/components/text/TextSubtitleSecondary";
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight,
} from "react-icons/fi";
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";
import Compact from "@/utils/compact";
import { BsCheckCircleFill, BsFillExclamationCircleFill } from "react-icons/bs";
import { BsFillPencilFill, BsTrashFill } from "react-icons/bs";
import router from "next/router";
import { useCallback, useEffect, useRef, useState } from "react";
import { MdCancel } from "react-icons/md";
import axios from "axios";
import { utils, writeFile } from "xlsx";

const LayoutBannerPage = (props: any) => {
  const config = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
    },
  };

  const [userId, setUserId] = useState("");
  const [userType, setUserType] = useState("");
  const [userRfid, setUserRfid] = useState("");
  const [userName, setUserName] = useState("");
  const [userNik, setUserNik] = useState("");
  const [userBranch, setUserBranch] = useState("");
  const [isActive, setIsActive] = useState(true);

  const [branchOptions, setBranchOptions] = useState([]);

  useEffect(() => {
    setIsActive(true);
  }, []);

  const handleToggle = () => {
    setIsActive(!isActive);
  };

  const clear = () => {
    setUserId("");
    setUserType("");
    setUserRfid("");
    setUserName("");
    setUserNik("");
    setIsActive(true);
    setUserBranch("");
  };

  const { menu, sub } = router.query;
  const type = props.type;
  const mobile = Compact();
  const endpoint = process.env.NEXT_PUBLIC_EP;
  const [userData, setUserData] = useState(
    JSON.parse(localStorage.getItem("canteenDataDetail")!)
  );

  const dateFormatAlt = (dateString: any) => {
    const originalDate = new Date(dateString);
    originalDate.setDate(originalDate.getDate() + 1);

    const formattedDate = originalDate.toISOString().split("T")[0];
    return formattedDate;
  };

  const dateTimeFormatAlt = (dateTimeString) => {
    const originalDateTime = new Date(dateTimeString);

    // Adjust the date-time format as needed
    const formattedDateTime = originalDateTime
      .toISOString()
      .replace("T", " ")
      .split(".")[0];

    return formattedDateTime;
  };

  // Variable fetch
  const [data, setData] = useState([]);
  const [allData, setAllData] = useState([]);

  // Variable search
  const [search, setSearch] = useState("");

  // Variable pagination
  const pageNumbers = [];
  const maxPageLinks = 5;
  const [currentPage, setCurrentPage] = useState(0);
  const [dataPerPage, setDataPerPage] = useState(5);
  const startIndex = currentPage * dataPerPage;
  const endIndex = startIndex + dataPerPage;
  const pageData = allData.slice(startIndex, endIndex);
  const totalPages = Math.ceil(allData.length / dataPerPage);
  let startPage = currentPage - Math.floor(maxPageLinks / 2);

  // Variable sorting
  const [sortOrder, setSortOrder] = useState("asc");
  const [sortColumn, setSortColumn] = useState("name");

  // Variable notification
  const [showNotification, setShowNotification] = useState(false);
  const [statNotif, setStatNotif] = useState(true);
  const [msgNotif, setMsgNotif] = useState("");

  const initialState = () => {};

  function convertTime(time: any) {
    const utcDate = new Date(time);

    const localOffset = utcDate.getTimezoneOffset();

    const gmtPlus7Offset = 420;

    const localTime = new Date(
      utcDate.getTime() + (localOffset + gmtPlus7Offset) * 60000
    );

    const year = localTime.getFullYear();
    const month = localTime.getMonth() + 1;
    const day = localTime.getDate();
    const hours = localTime.getHours();
    const minutes = localTime.getMinutes();
    const seconds = localTime.getSeconds();

    const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${day
      .toString()
      .padStart(2, "0")}`;
    const formattedTime = `${hours.toString().padStart(2, "0")}:${minutes
      .toString()
      .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;

    const formattedDateTime = `${formattedDate} ${formattedTime}`;

    return formattedDateTime;
  }

  const createNewRequest = () => {
    console.log("Create New Request");
  };

  const [userTypeOptions, setUserTypeOptions] = useState([]);

  const fetchAllData = useCallback(async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
        },
      };

      const reqId = {
        id: router.query.id,
      };

      const dataUser = await axios.post(
        endpoint + "canteen/show-banner",
        {},
        config
      );

      console.log("user : ", dataUser);
      setData(dataUser.data.data);
    } catch (error) {
      // Handle error here
    }
  }, [router.query, endpoint]);

  useEffect(() => {
    fetchAllData();
  }, [fetchAllData]);

  // Pagination
  const changePageRow = (curPage: any) => {
    setDataPerPage(curPage);
    setCurrentPage(0);
  };

  if (startPage < 1) {
    startPage = 1;
  }

  let endPage = startPage + maxPageLinks - 1;
  if (endPage > totalPages) {
    endPage = totalPages;
    startPage = endPage - maxPageLinks + 1;
    if (startPage < 1) {
      startPage = 1;
    }
  }

  for (let i = startPage; i <= endPage; i++) {
    pageNumbers.push(i);
  }

  useEffect(() => {
    let sortedData = [...data];
    if (sortColumn && sortOrder) {
      sortedData = sortedData.sort(sortData(sortColumn, sortOrder));
    }
    setAllData(sortedData);
  }, [data, sortColumn, sortOrder]);

  // Sorting
  function sortData(column: string, order: string) {
    return function (a: any, b: any) {
      const aValue = a[column];
      const bValue = b[column];

      if (aValue < bValue) {
        return order === "asc" ? -1 : 1;
      } else if (aValue > bValue) {
        return order === "asc" ? 1 : -1;
      } else {
        return 0;
      }
    };
  }

  function handleSort(column: string) {
    const newSortOrder =
      column === sortColumn ? (sortOrder === "asc" ? "desc" : "asc") : "asc";
    setSortOrder(newSortOrder);
    setSortColumn(column);
  }

  const updateData = (param: any) => {
    console.log(param);
    setUserId(param.muse_id);
    setUserNik(param.muse_user_nik);
    setUserType(param.muty_type_id);
    setUserRfid(param.muse_rfid);
    setUserName(param.muse_name);
    setIsActive(param.muse_isactive);
    setUserBranch(param.muse_store);
  };

  const submit = async () => {
    if ((userNik, userRfid, userName)) {
      const data = [
        {
          muse_id: userId,
          muse_user_nik: userNik,
          muty_type_id: userType,
          muse_rfid: userRfid,
          muse_name: userName,
          muse_isactive: isActive,
          muse_store: userBranch,
        },
      ];

      if (userId) {
        const response = await axios.post(
          endpoint + "canteen/update-user",
          data,
          config
        );

        if (response && response.data && response.data.status) {
          setStatNotif(true);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 15000);
        } else {
          setStatNotif(false);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 15000);
        }
      } else {
        const response = await axios.post(
          endpoint + "canteen/add-user",
          data,
          config
        );

        if (response && response.data && response.data.status) {
          setStatNotif(true);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 15000);

          fetchAllData();
        } else {
          setStatNotif(false);
          setMsgNotif(response.data.message);
          setShowNotification(true);

          setTimeout(() => {
            setShowNotification(false);
          }, 15000);
        }
      }
    } else {
      setStatNotif(false);
      setMsgNotif("Incomplete Data!");
      setShowNotification(true);
    }

    setTimeout(() => {
      setShowNotification(false);
    }, 2000);

    await clear();
    fetchAllData();
  };

  // Search

  useEffect(() => {
    const fetchData = async () => {
      const res = await data;
      if (search) {
        const searchKeywords = search.toLowerCase().split(" ");
        const filteredData = res.filter((item) => {
          //   return searchKeywords.every((keyword) => {
          return searchKeywords.some((keyword) => {
            const objectValues = Object.values(item).join(" ").toLowerCase();
            return objectValues.includes(keyword);
          });
        });
        setAllData(filteredData);
      } else {
        setAllData(res);
      }
      setCurrentPage(0);
    };

    fetchData();
  }, [data, search]);

  const handleNotification = () => {
    setShowNotification(true);
    setTimeout(() => {
      setShowNotification(false);
    }, 15000);
  };

  const editShow = (i: any) => {
    initialState();
  };

  const exportToExcel = useCallback(() => {
    const headings = [
      "user ID",
      "User Name",
      "User Branch",
      "NIK",
      "User Type",
      "RFID",
      "Created At",
      "Updated At",
      "IsActive",
    ];

    const dataRows = allData.map((item) => [
      item.muse_id,
      item.muse_name,
      item.mcdb_flag_source,
      item.muse_user_nik,
      item.muty_type_name,
      item.muse_rfid,
      dateFormatAlt(item.muse_created_at),
      dateTimeFormatAlt(item.muse_updated_at),
      item.muse_isactive,
    ]);

    const ws = utils.aoa_to_sheet([headings, ...dataRows]);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, "Data");

    const now = new Date();
    const formattedDateTime = formatDateTime(now, now);
    writeFile(wb, `Data-Users ${formattedDateTime}.xlsx`);
  }, [allData]);

  return (
    <div className="flex flex-col w-full h-full px-5">
      {showNotification && (
        <div className={`absolute max-w-[80%] pt-2 right-5`}>
          {statNotif ? (
            <div className="flex flex-row bg-success justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white">
                <BsCheckCircleFill />
              </span>
              <span className={`${mobile && "text-xs"} text-white`}>
                {msgNotif}
              </span>
            </div>
          ) : (
            <div className="flex flex-row bg-error justify-center items-center gap-2 p-3 shadow-md radius-xs rounded-lg">
              <span className="text-white">
                <BsFillExclamationCircleFill />
              </span>
              <span className={`${mobile && "text-xs"} text-white`}>
                {msgNotif}
              </span>
            </div>
          )}
        </div>
      )}
      <div className="flex flex-row justify-between mx-3 mt-3 mb-5">
        <TextSubtitle text={"BANNER TABLE"} />
        <div className="justify-end mt-3 mb-5">
          <label
            onClick={clear}
            htmlFor="modal-add-banner"
            className="mr-2 rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
          >
            Add Banner
          </label>
          <label
            onClick={exportToExcel}
            className="mr-2 rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
          >
            Export Data
          </label>
          {/* <label
            htmlFor="modal-import-user"
            className="rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
          >
            Import Data
          </label> */}
        </div>
      </div>

      <div
        className={`flex ${
          mobile
            ? "flex-col gap-2 items-center mt-3"
            : "flex-row justify-between"
        } mx-3 mb-3`}
      >
        <div className="flex flex-row items-center gap-2">
          <h1 className="text-sm text-secondary">Show</h1>
          <select
            className="select select-primary select-sm w-16 text-secondary font-bold"
            defaultValue={dataPerPage}
            onChange={(e) => changePageRow(e.target.value)}
          >
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={25}>25</option>
            <option value={50}>50</option>
          </select>
          <h1 className="text-sm text-secondary">entries</h1>
        </div>
        <input
          type="text"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          className={`text-sm ${
            mobile
              ? "input-md text-secondary w-full max-w-full"
              : "input-sm w-full max-w-sm"
          } input-primary input border border-1 text-primary border-primary`}
        />
      </div>

      <div className="shadow overflow-hidden border-b border-gray-200 rounded-lg overflow-y-auto flex flex-1 mx-3">
        <table className="min-w-full divide-y divide-gray-200 table-fixed">
          <thead className="bg-gray-50 sticky top-0">
            <tr className={`${!mobile && "hidden"}`}>
              <th
                onClick={() => handleSort("rnp_id")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Request
                  {sortColumn === "rnp_id" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>
            </tr>

            <tr className={"false"}>
              {/* # */}

              <th
                onClick={() => handleSort("user_index")}
                scope="col"
                className={`w-[20px] cursor-pointer px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  #
                </div>
              </th>

              {/* STORE */}

              <th
                onClick={() => handleSort("tads_filename")}
                scope="col"
                className={`w-[20px] cursor-pointer px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  STORE
                  {sortColumn === "tads_filename" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* BANNER */}

              <th
                onClick={() => handleSort("tads_filepath")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  BANNER
                  {sortColumn === "tads_filepath" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* STATUS */}

              <th
                onClick={() => handleSort("muse_isactive")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Status
                  {sortColumn === "muse_isactive" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* ACTION */}

              <th>
                <div className="flex text-xs font-medium text-gray-500 uppercase">
                  Action
                </div>
              </th>
            </tr>
          </thead>

          <tbody className="bg-white divide-y divide-gray-200">
            {pageData.map((item, index) => (
              <tr
                className="transition ease-in-out hover:bg-neutral-50"
                key={index}
              >
                <td className="px-6 py-4 text-sm text-gray-500 w-[20px]">
                  {startIndex + index + 1}
                </td>
                <td className="items-center flex flex-row justify-between px-6 py-4 text-sm text-gray-500">
                  {item.mcdb_store_code}
                </td>
                <td className="items-center px-6 py-4 text-sm text-gray-500">
                  {item.tads_filepath}
                </td>

                <td className="px-6 py-4 whitespace-nowrap text-base text-gray-500 text-center">
                  <input
                    type="checkbox"
                    id="is_active"
                    name="is_active"
                    checked={item.tads_isactive}
                    className={`checkbox checkbox-primary h-8 w-8`}
                  />
                </td>
                {/* <td> */}
                <div className="py-4 whitespace-nowrap text-base text-gray-500">
                  <label
                    onClick={() => {
                      updateData(item);
                    }}
                    for="modal-add-banner"
                    className="flex w-8 h-8 bg-primary rounded border border-1 border-primary cursor-pointer justify-center items-center text-white"
                  >
                    <BsFillPencilFill />
                  </label>
                </div>
                {/* </td> */}
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      <div className="flex flex-row justify-between mx-3 mt-3 items-center text-gray-500">
        <p className="text-sm text-secondary-focus">
          Showing data {(currentPage + 1) * dataPerPage - dataPerPage + 1} -{" "}
          {allData.length / ((currentPage + 1) * dataPerPage) >= 1
            ? (currentPage + 1) * dataPerPage
            : (allData.length % dataPerPage) + currentPage * dataPerPage}{" "}
          of {allData.length}
        </p>

        <div className="flex btn-group justify-end items-center gap-3">
          <p className={`text-sm text-secondary-focus`}>Page :</p>
          <input
            type="number"
            value={currentPage + 1}
            min={1}
            max={Math.ceil(allData.length / dataPerPage)}
            onChange={(e) => {
              parseInt(e.target.value) <=
                Math.ceil(allData.length / dataPerPage) &&
                e.target.value &&
                setCurrentPage(parseInt(e.target.value) - 1);
            }}
            className={`text-sm ${
              mobile ? "input-md" : "input-sm"
            } w-16 input-primary input border border-1 border-primary h-8`}
          />
          <p className={`text-sm text-secondary-focus`}>
            of {Math.ceil(allData.length / dataPerPage)}
          </p>
        </div>
      </div>

      <div className="flex m-3 w-full items-center justify-center gap-1">
        <FiChevronsLeft
          onClick={() => setCurrentPage(0)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronLeft
          onClick={() => currentPage > 0 && setCurrentPage(currentPage - 1)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        {pageNumbers.map((number) => (
          <button
            key={number}
            onClick={() => {
              setCurrentPage(number - 1);
            }}
            className={`${
              number - 1 === currentPage
                ? "bg-primary text-white"
                : "bg-white text-primary"
            } btn btn-sm border rounded-md w-10 border-gray-200 hover:bg-primary-focus hover:text-white hover:border-primary-focus`}
          >
            {number}
          </button>
        ))}
        <FiChevronRight
          onClick={() =>
            currentPage < Math.ceil(allData.length / dataPerPage) - 1 &&
            setCurrentPage(currentPage + 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronsRight
          onClick={() =>
            setCurrentPage(Math.ceil(allData.length / dataPerPage) - 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
      </div>

      <input type="checkbox" id="modal-add-banner" className="modal-toggle" />
      <div className="modal" role="dialog">
        <div className="modal-box">
          <div className="flex flex-row justify-between items-center mb-3">
            <h1 className="text-sm font-bold text-secondary">
              {userId ? "UPDATE BANNER" : "CREATE NEW BANNER"}
            </h1>
            <div className="flex flex-row items-center gap-2">
              <label
                className="text-primary cursor-pointer"
                htmlFor="modal-add-banner"
              >
                <MdCancel className="w-7 h-7" />
              </label>
            </div>
          </div>

          <div className="flex flex-col w-full">
            <select
              className="select select-sm select-bordered w-full mb-3 text-xs text-secondary"
              id="store"
              name="store"
              value={userBranch}
              onChange={(e) => setUserBranch(e.target.value)}
            >
              <option value="">Select Store</option>
              {branchOptions.map((branch) => (
                <option
                  key={branch.mcdb_store_code}
                  value={branch.mcdb_store_code}
                  selected={branch.mcdb_store_code == userBranch ? true : false}
                >
                  {`${branch.mcdb_store_code} - ${branch.mcdb_flag_source}`}
                </option>
              ))}
            </select>
          </div>

          <div className="flex gap-3">
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="start-date"
              >
                Start Date
              </label>
              <input
                type="date"
                name="start-date"
                id="start-date"
                className="input input-sm input-bordered w-full text-xs text-secondary"
                // value={vendorValidFrom}
                // onChange={(e) => setVendorValidFrom(e.target.value)}
              ></input>
            </div>
            <div className="flex flex-col w-full">
              <label
                className="block text-secondary text-xs text mb-1"
                htmlFor="end-date"
              >
                End Date
              </label>
              <input
                type="date"
                name="end-date"
                id="end-date"
                className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
                // value={vendorValidTo}
                // onChange={(e) => setVendorValidTo(e.target.value)}
              ></input>
            </div>
          </div>

          <div class="flex flex-row gap-2 items-center text-secondary mb-3">
            <div class="flex w-full">
              <div class="flex flex-col w-[367px]">
                <label
                  className="block text-secondary text-xs text"
                  htmlFor="file-upload"
                >
                  {/* <a className="btn btn-primary"> Upload Files </a> */}
                </label>
                <input id="file-upload" type="file" multiple accept="image/*" />
              </div>
            </div>
            <div class="flex items-center justify-end gap-2">
              <input
                type="checkbox"
                id="is_active"
                name="is_active"
                className="flex toggle toggle-primary toggle-sm"
                checked={isActive}
                onChange={handleToggle}
              ></input>

              <label
                htmlFor="is_active"
                className={`flex text-xs ${
                  isActive ? "text-primary" : "text-secondary"
                }`}
              >
                {isActive ? "Active" : "Inactive"}
              </label>
            </div>
          </div>

          <label
            htmlFor="modal-add-banner"
            className="btn btn-md w-full text-white bg-primary border-transparent hover:bg-secondary"
            onClick={submit}
          >
            Submit
          </label>
        </div>
      </div>
    </div>
  );
};

export default LayoutBannerPage;
