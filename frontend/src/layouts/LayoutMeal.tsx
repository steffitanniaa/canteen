// @ts-nocheck

import TextSubtitle from "@/components/text/TextSubtitleSecondary";
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight,
} from "react-icons/fi";
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";
import Compact from "@/utils/compact";
import { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { BsFillPencilFill, BsTrashFill } from "react-icons/bs";
import { VscClearAll } from "react-icons/vsc";
import { FaMagnifyingGlass } from "react-icons/fa6";
import Link from "next/link";
import router from "next/router";
import { utils, writeFile } from "xlsx";

const LayoutMealPage = (props: any) => {
  const config = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
    },
  };
  const [orderID, setOrderId] = useState("");
  const [userID, setUserID] = useState("");
  const [storeCode, setStoreCode] = useState("");
  const [vendorID, setVendorID] = useState("");
  const [orderDate, setOrderDate] = useState("");

  const [branchOptions, setBranchOptions] = useState([]);

  const [orderDateFrom, setOrderDateFrom] = useState("");
  const [orderDateTo, setOrderDateTo] = useState("");
  const [searchResult, setSearchResult] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await axios.post(
          endpoint + "canteen/show-branch",
          data,
          config
        );
        if (response.data.status) {
          setBranchOptions(response.data.data);
        } else {
          console.error("Failed to fetch branch data:", response.data.message);
        }
      } catch (error) {
        console.error("Error fetching branch data:", error);
      }
    }

    fetchData();
  }, []);

  const clear = () => {
    setOrderId("");
    setUserID("");
    setStoreCode("");
    setVendorID("");
    setOrderDate("");
  };

  const { menu, sub } = router.query;
  const type = props.type;
  const mobile = Compact();
  const endpoint = process.env.NEXT_PUBLIC_EP;
  const [userData, setUserData] = useState(
    JSON.parse(localStorage.getItem("canteenDataDetail")!)
  );

  const dateFormatAlt = (dateString: any) => {
    const originalDate = new Date(dateString);
    originalDate.setDate(originalDate.getDate() + 1);

    const formattedDate = originalDate.toISOString().split("T")[0];
    return formattedDate;
  };

  const timeFormatAlt = (timeString) => {
    const originalTime = new Date(timeString);
    const hours = originalTime.getHours().toString().padStart(2, "0");
    const minutes = originalTime.getMinutes().toString().padStart(2, "0");
    const seconds = originalTime.getSeconds().toString().padStart(2, "0");

    const formattedTime = `${hours}:${minutes}:${seconds}`;
    return formattedTime;
  };

  const dateTimeFormatAlt = (dateTimeString) => {
    const originalDateTime = new Date(dateTimeString);

    // Adjust the date-time format as needed
    const formattedDateTime = originalDateTime
      .toISOString()
      .replace("T", " ")
      .split(".")[0];

    return formattedDateTime;
  };

  // Variable fetch
  const [data, setData] = useState([]);
  const [allData, setAllData] = useState([]);

  // Variable search
  const [search, setSearch] = useState("");

  // Variable pagination
  const pageNumbers = [];
  const maxPageLinks = 5;
  const [currentPage, setCurrentPage] = useState(0);
  const [dataPerPage, setDataPerPage] = useState(5);
  const startIndex = currentPage * dataPerPage;
  const endIndex = startIndex + dataPerPage;
  const pageData = allData.slice(startIndex, endIndex);
  const totalPages = Math.ceil(allData.length / dataPerPage);
  let startPage = currentPage - Math.floor(maxPageLinks / 2);

  // Variable sorting
  const [sortOrder, setSortOrder] = useState("asc");
  const [sortColumn, setSortColumn] = useState("name");

  // Variable notification
  const [showNotification, setShowNotification] = useState(false);
  const [statNotif, setStatNotif] = useState(true);
  const [msgNotif, setMsgNotif] = useState("");

  const initialState = () => {};

  function convertTime(time: any) {
    const utcDate = new Date(time);

    const localOffset = utcDate.getTimezoneOffset();

    const gmtPlus7Offset = 420;

    const localTime = new Date(
      utcDate.getTime() + (localOffset + gmtPlus7Offset) * 60000
    );

    const year = localTime.getFullYear();
    const month = localTime.getMonth() + 1;
    const day = localTime.getDate();
    const hours = localTime.getHours();
    const minutes = localTime.getMinutes();
    const seconds = localTime.getSeconds();

    const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${day
      .toString()
      .padStart(2, "0")}`;
    const formattedTime = `${hours.toString().padStart(2, "0")}:${minutes
      .toString()
      .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;

    const formattedDateTime = `${formattedDate} ${formattedTime}`;

    return formattedDateTime;
  }

  const createNewRequest = () => {
    console.log("Create New Request");
  };

  const fetchAllData = useCallback(async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
        },
      };

      const reqId = {
        id: router.query.id,
      };

      const dataMeal = await axios.post(
        endpoint + "canteen/show-meal-order",
        {},
        config
      );

      setData(dataMeal.data.data);
      // console.log(dataMeal.data.data);
      // console.log("ok");
    } catch (error) {
      // Handle error here
    }
  }, [router.query, endpoint]);

  useEffect(() => {
    fetchAllData();
  }, [fetchAllData]);

  // Pagination
  const changePageRow = (curPage: any) => {
    setDataPerPage(curPage);
    setCurrentPage(0);
  };

  if (startPage < 1) {
    startPage = 1;
  }

  let endPage = startPage + maxPageLinks - 1;
  if (endPage > totalPages) {
    endPage = totalPages;
    startPage = endPage - maxPageLinks + 1;
    if (startPage < 1) {
      startPage = 1;
    }
  }

  for (let i = startPage; i <= endPage; i++) {
    pageNumbers.push(i);
  }

  useEffect(() => {
    let sortedData = [...data];
    if (sortColumn && sortOrder) {
      sortedData = sortedData.sort(sortData(sortColumn, sortOrder));
    }
    setAllData(sortedData);
  }, [data, sortColumn, sortOrder]);

  // Sorting
  function sortData(column: string, order: string) {
    return function (a: any, b: any) {
      const aValue = a[column];
      const bValue = b[column];

      if (aValue < bValue) {
        return order === "asc" ? -1 : 1;
      } else if (aValue > bValue) {
        return order === "asc" ? 1 : -1;
      } else {
        return 0;
      }
    };
  }

  function handleSort(column: string) {
    const newSortOrder =
      column === sortColumn ? (sortOrder === "asc" ? "desc" : "asc") : "asc";
    setSortOrder(newSortOrder);
    setSortColumn(column);
  }

  const handleSearch = async () => {
    try {
      const response = await axios.post(
        endpoint + "/canteen/search-meal-order",
        {
          tmor_order_from: orderDateFrom,
          tmor_order_to: orderDateTo,
          tmor_store_code: storeCode,
        },
        config
      );

      console.log("Search response:", response.data);

      setData(response.data.data);
    } catch (error) {
      console.error("Error searching meal order:", error);
      setSearchResult([]);
    }
  };

  const handleClearAll = () => {
    setStoreCode("");
    setOrderDateFrom("");
    setOrderDateTo("");
    fetchAllData();
  };

  useEffect(() => {
    if (searchResult && searchResult.length > 0) {
      console.log("Search results:", searchResult);
    }
  }, [searchResult]);

  // Search
  useEffect(() => {
    const fetchData = async () => {
      const res = await data;
      if (search) {
        const searchKeywords = search.toLowerCase().split(" ");
        const filteredData = res.filter((item) => {
          //   return searchKeywords.every((keyword) => {
          return searchKeywords.some((keyword) => {
            const objectValues = Object.values(item).join(" ").toLowerCase();
            return objectValues.includes(keyword);
          });
        });
        setAllData(filteredData);
      } else {
        setAllData(res);
      }
      setCurrentPage(0);
    };

    fetchData();
  }, [data, search]);

  const handleNotification = () => {
    setShowNotification(true);
    setTimeout(() => {
      setShowNotification(false);
    }, 3000);
  };

  const editShow = (i: any) => {
    initialState();
  };

  function formatDateTime(date: any, time: any) {
    const dateOpt = { year: "numeric", month: "2-digit", day: "2-digit" };
    const timeOpt = {
      hour12: false,
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    };

    const formatDate = date
      .toLocaleDateString("en-US", dateOpt)
      .replace(/\//g, "");
    const formatTime = time
      .toLocaleTimeString("en-US", timeOpt)
      .replace(/:/g, "");

    const year = formatDate.slice(4, 8);
    const month = formatDate.slice(0, 2);
    const day = formatDate.slice(2, 4);

    const hour = formatTime.slice(0, 2);
    const minute = formatTime.slice(2, 4);
    const second = formatTime.slice(4, 6);

    // return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    return year + month + day + hour + minute + second;
  }

  const exportToExcel = useCallback(() => {
    const headings = [
      "Order ID",
      "User Name",
      "NIK",
      "Vendor Name",
      "Meal Price",
      "Store Code",
      "Order Date",
      "Order Time",
      "Created At",
      "Updated At",
    ];

    const dataRows = allData.map((item) => [
      item.tmor_order_id,
      item.muse_name,
      item.muse_user_nik,
      item.mven_vendor_name,
      item.mven_meal_price,
      item.flag_source,
      dateFormatAlt(item.tmor_order_date),
      timeFormatAlt(item.tmor_order_datetime),
      dateTimeFormatAlt(item.tmor_created_at),
      item.tmor_updated_at,
    ]);

    const ws = utils.aoa_to_sheet([headings, ...dataRows]);

    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, "Data");

    const now = new Date();
    const formattedDateTime = formatDateTime(now, now);
    writeFile(wb, `Data-Meal-Orders ${formattedDateTime}.xlsx`);
  }, [allData]);

  return (
    <div className="flex flex-col w-full h-full px-5">
      <div className="flex flex-row justify-between mx-3 mt-3 mb-5">
        <TextSubtitle text={"MEAL ORDER TABLE"} />
        <label
          onClick={exportToExcel}
          className="rounded-md bg-white border-primary border-1 border px-3.5 py-2.5 text-sm text-primary shadow-sm hover:bg-secondary hover:border-secondary hover:text-white cursor-pointer"
        >
          Export Data
        </label>
      </div>

      {/* SEARCH */}
      <div className="flex flex-col mx-3 items-center">
        <div className="justify-start">
          <select
            className="select select-sm select-bordered w-[300px] mb-3 text-xs text-secondary"
            id="store"
            name="store"
            value={storeCode}
            onChange={(e) => setStoreCode(e.target.value)}
          >
            <option value="">Select Store</option>
            {branchOptions.map((branch) => (
              <option
                key={branch.mcdb_store_code}
                value={branch.mcdb_store_code}
              >
                {`${branch.mcdb_store_code} - ${branch.mcdb_flag_source}`}
              </option>
            ))}
          </select>
        </div>

        <div className="flex gap-3 ">
          <div className="">
            <label
              className="block text-secondary text-xs text mb-1"
              htmlFor="order-date"
            >
              Order Date From
            </label>
            <input
              type="date"
              name="order-date-from"
              id="order-date-from"
              className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
              value={orderDateFrom}
              onChange={(e) => setOrderDateFrom(e.target.value)}
            ></input>
          </div>

          <div className="">
            <label
              className="block text-secondary text-xs text mb-1"
              htmlFor="order-date"
            >
              Order Date To
            </label>
            <input
              type="date"
              name="order-date-to"
              id="order-date-to"
              className="input input-sm input-bordered w-full mb-3 text-xs text-secondary"
              value={orderDateTo}
              onChange={(e) => setOrderDateTo(e.target.value)}
            ></input>
          </div>
        </div>

        <div class="flex">
          <div class="flex w-full">
            <div class="flex flex-col w-[242px]">
              <label
                onClick={handleSearch}
                className="btn btn-md text-white bg-primary border-transparent hover:bg-secondary mb-5 mr-2"
              >
                Search
              </label>
            </div>
          </div>
          <div class="flex items-center justify-end gap-2">
            <label
              onClick={handleClearAll}
              className="btn btn-md text-primary bg-white border-primary hover:bg-secondary hover:text-white mb-5"
            >
              <VscClearAll className="w-6 h-6" />
            </label>
          </div>
        </div>
      </div>

      <div
        className={`flex ${
          mobile
            ? "flex-col gap-2 items-center mt-3"
            : "flex-row justify-between"
        } mx-3 mb-3`}
      >
        <div className="flex flex-row items-center gap-2">
          <h1 className="text-sm text-secondary">Show</h1>
          <select
            className="select select-primary select-sm w-16 text-secondary font-bold"
            defaultValue={dataPerPage}
            onChange={(e) => changePageRow(e.target.value)}
          >
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={25}>25</option>
            <option value={50}>50</option>
          </select>
          <h1 className="text-sm text-secondary">entries</h1>
        </div>
        <input
          type="text"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          className={`text-sm ${
            mobile
              ? "input-md text-secondary w-full max-w-full"
              : "input-sm w-full max-w-sm"
          } input-primary input border border-1 text-primary border-primary`}
        />
      </div>
      <div className="shadow overflow-hidden border-b border-gray-200 rounded-lg overflow-y-auto flex flex-1 mx-3">
        <table className="min-w-full divide-y divide-gray-200 table-fixed">
          <thead className="bg-gray-50 sticky top-0">
            <tr className={`${!mobile && "hidden"}`}>
              <th
                onClick={() => handleSort("rnp_id")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Request
                  {sortColumn === "rnp_id" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>
            </tr>
            <tr className={`${mobile && "hidden"} `}>
              {/* # */}

              <th
                onClick={() => handleSort("user_index")}
                scope="col"
                className={`w-[20px] cursor-pointer px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  #
                </div>
              </th>

              {/* USER NIK */}

              <th
                onClick={() => handleSort("muse_user_nik")}
                scope="col"
                className={`w-[20px] cursor-pointer px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-start">
                  NIK
                  {sortColumn === "muse_user_nik" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* USER NAME */}

              <th
                onClick={() => handleSort("muse_name")}
                scope="col"
                className={`cursor-pointer pl-16 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  User Name
                  {sortColumn === "muse_name" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* STORE CODE */}

              <th
                onClick={() => handleSort("flag_source")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Store Code
                  {sortColumn === "flag_source" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* VENDOR ID */}

              <th
                onClick={() => handleSort("mven_vendor_name")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Vendor Name
                  {sortColumn === "mven_vendor_name" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* MEAL PRICE */}

              <th
                onClick={() => handleSort("mven_meal_price")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Meal Price
                  {sortColumn === "mven_meal_price" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* ORDER DATE */}

              <th
                onClick={() => handleSort("tmor_order_date")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Order Date
                  {sortColumn === "tmor_order_date" && sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>

              {/* ORDER DATE TIME*/}

              <th
                onClick={() => handleSort("tmor_order_datetime")}
                scope="col"
                className={`cursor-pointer py-3 text-xs font-medium text-gray-500 uppercase tracking-wider`}
              >
                <div className="flex flex-row text-xs items-center justify-center">
                  Order Time
                  {sortColumn === "tmor_order_datetime" &&
                  sortOrder === "asc" ? (
                    <TiArrowSortedUp />
                  ) : (
                    <TiArrowSortedDown />
                  )}
                </div>
              </th>
            </tr>
          </thead>

          <tbody className="bg-white divide-y divide-gray-200">
            {/* Render hasil pencarian jika ada */}
            {searchResult.length > 0 &&
              searchResult.map((item, index) => (
                <tr
                  className="transition ease-in-out hover:bg-neutral-50"
                  key={index}
                >
                  <td className="px-6 py-4 text-sm text-gray-500 w-[20px]">
                    {startIndex + index + 1}
                  </td>
                  <td className="items-center flex flex-row justify-between px-6 py-4 text-sm text-gray-500">
                    {item.muse_user_nik}
                  </td>
                  <td className="items-center px-6 py-4 text-sm text-gray-500">
                    {item.muse_name}
                  </td>
                  <td className="w-[200px] py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {item.flag_source}
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {item.mven_vendor_name}
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    <div>
                      Rp. {""}
                      {item?.mven_meal_price.toLocaleString(undefined, {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2,
                      })}
                    </div>
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {dateFormatAlt(item.tmor_order_date)}
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {timeFormatAlt(item.tmor_order_datetime)}
                  </td>
                </tr>
              ))}
            {/* Render semua data jika tidak ada hasil pencarian */}
            {searchResult.length === 0 &&
              pageData.map((item, index) => (
                <tr
                  className="transition ease-in-out hover:bg-neutral-50"
                  key={index}
                >
                  <td className="px-6 py-4 text-sm text-gray-500 w-[20px]">
                    {startIndex + index + 1}
                  </td>
                  <td className="items-center flex flex-row justify-between px-6 py-4 text-sm text-gray-500">
                    {item.muse_user_nik}
                  </td>
                  <td className="items-center px-6 py-4 text-sm text-gray-500">
                    {item.muse_name}
                  </td>
                  <td className="w-[200px] py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {item.flag_source}
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {item.mven_vendor_name}
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    <div>
                      Rp. {""}
                      {item?.mven_meal_price.toLocaleString(undefined, {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2,
                      })}
                    </div>
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {dateFormatAlt(item.tmor_order_date)}
                  </td>
                  <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                    {timeFormatAlt(item.tmor_order_datetime)}
                  </td>
                </tr>
              ))}
            {/* {pageData.map((item, index) => (
              <tr
                className="transition ease-in-out hover:bg-neutral-50"
                key={index}
              >
                <td className="px-6 py-4 text-sm text-gray-500 w-[20px]">
                  {startIndex + index + 1}
                </td>
                <td className="items-center flex flex-row justify-between px-6 py-4 text-sm text-gray-500">
                  {item.muse_user_nik}
                </td>
                <td className="items-center px-6 py-4 text-sm text-gray-500">
                  {item.muse_name}
                </td>
                <td className="w-[200px] py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                  {item.flag_source}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                  {item.mven_vendor_name}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                  <div>
                    Rp. {""}
                    {item?.mven_meal_price.toLocaleString(undefined, {
                      maximumFractionDigits: 2,
                      minimumFractionDigits: 2,
                    })}
                  </div>
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                  {dateFormatAlt(item.tmor_order_date)}
                </td>
                <td className="py-4 whitespace-nowrap text-sm text-gray-500 text-center">
                  {timeFormatAlt(item.tmor_order_datetime)}
                </td>
              </tr>
            ))} */}
          </tbody>
        </table>
      </div>
      <div className="flex flex-row justify-between mx-3 mt-3 items-center text-gray-500">
        <p className="text-sm text-secondary-focus">
          Showing data {(currentPage + 1) * dataPerPage - dataPerPage + 1} -{" "}
          {allData.length / ((currentPage + 1) * dataPerPage) >= 1
            ? (currentPage + 1) * dataPerPage
            : (allData.length % dataPerPage) + currentPage * dataPerPage}{" "}
          of {allData.length}
        </p>

        <div className="flex btn-group justify-end items-center gap-3">
          <p className={`text-sm text-secondary-focus`}>Page :</p>
          <input
            type="number"
            value={currentPage + 1}
            min={1}
            max={Math.ceil(allData.length / dataPerPage)}
            onChange={(e) => {
              parseInt(e.target.value) <=
                Math.ceil(allData.length / dataPerPage) &&
                e.target.value &&
                setCurrentPage(parseInt(e.target.value) - 1);
            }}
            className={`text-sm ${
              mobile ? "input-md" : "input-sm"
            } w-16 input-primary input border border-1 border-primary h-8`}
          />
          <p className={`text-sm text-secondary-focus`}>
            of {Math.ceil(allData.length / dataPerPage)}
          </p>
        </div>
      </div>
      <div className="flex m-3 w-full items-center justify-center gap-1">
        <FiChevronsLeft
          onClick={() => setCurrentPage(0)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronLeft
          onClick={() => currentPage > 0 && setCurrentPage(currentPage - 1)}
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        {pageNumbers.map((number) => (
          <button
            key={number}
            onClick={() => {
              setCurrentPage(number - 1);
            }}
            className={`${
              number - 1 === currentPage
                ? "bg-primary text-white"
                : "bg-white text-primary"
            } btn btn-sm border rounded-md w-10 border-gray-200 hover:bg-primary-focus hover:text-white hover:border-primary-focus`}
          >
            {number}
          </button>
        ))}
        <FiChevronRight
          onClick={() =>
            currentPage < Math.ceil(allData.length / dataPerPage) - 1 &&
            setCurrentPage(currentPage + 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
        <FiChevronsRight
          onClick={() =>
            setCurrentPage(Math.ceil(allData.length / dataPerPage) - 1)
          }
          className=" text-primary hover:text-secondary w-7 h-7 cursor-pointer"
        />
      </div>
    </div>
  );
};

export default LayoutMealPage;
