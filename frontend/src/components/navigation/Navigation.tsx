import { useCallback, useEffect, useState } from "react";
import {
  MdArrowBack,
  MdClose,
  MdMenu,
  MdSearch,
  MdViewCompact,
} from "react-icons/md";
import Compact from "@/utils/compact";
import router from "next/router";
import Link from "next/link";
import axios from "axios";
import * as Icons from "react-icons/ri";
import Image from "next/image";

interface Menu {
  mmen_id: number;
  mmen_name: string;
  mmen_link: string;
  mmen_icon: string;
  mmen_status: boolean;
  mmen_parent_id: number;
  children?: Menu[];
}

interface GroupedData {
  parent: string;
  menus: Menu[];
}

const Navigation = (props: any) => {
  const { menu, sub } = router.query;

  const mobile = Compact();
  const [token, setToken] = useState<string | null>(null);
  const endpointArena = process.env.NEXT_PUBLIC_EP_ARENA;
  const endpoint = process.env.NEXT_PUBLIC_EP;

  const [openMenu, setOpenMenu] = useState(false);
  const [openArena, setOpenArena] = useState(false);
  const [openProfile, setOpenProfile] = useState(false);

  const [userData, setUserData] = useState(
    JSON.parse(localStorage.getItem("canteenDataDetail")!)
  );

  const [menuCategory, setMenuCategory] = useState<any[]>([]);
  const [menuCategoryActive, setMenuCategoryActive] = useState<any[]>([]);
  const [menus, setMenus] = useState<any[]>([]);

  const [allData, setAllData] = useState<any[]>([]);

  const [search, setSearch] = useState("");

  const storedToken = localStorage.getItem("canteenToken");

  type IconNames = keyof typeof Icons;

  const [groupedData, setGroupedData] = useState<GroupedData[]>([]);

  const generateGroupedData = (data: Menu[]): GroupedData[] => {
    const groupedData: GroupedData[] = [];

    data.forEach((menuItem) => {
      const parentIndex = groupedData.findIndex(
        (group) => group.parent === menuItem.mmen_name
      );
      if (parentIndex === -1) {
        // Create a new parent
        groupedData.push({
          parent: menuItem.mmen_name,
          menus: menuItem.children || [],
        });
      } else {
        // Add to existing parent
        groupedData[parentIndex].menus.push(...(menuItem.children || []));
      }
    });

    return groupedData;
  };

  const fetchAllMenu = useCallback(async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("canteenToken")}`,
        },
      };
      const response = await axios.post(
        endpointArena + "canteen/menus/get-all",
        {},
        config
      );
      setMenuCategory(response.data.data);

      const responseActive = await axios.post(
        endpointArena + "menus/get-active",
        {},
        config
      );
      setMenuCategoryActive(responseActive.data.data);

      const responseMenu = await axios.post(
        endpoint + "canteen/menus/get-all",
        {},
        config
      );
      const grouped = generateGroupedData(responseMenu.data.data);
      setGroupedData(grouped);
      setMenus(responseMenu.data.data);
    } catch (error) {
      // Handle error here
    }
  }, []);

  useEffect(() => {
    if (!storedToken) {
      router.push("/login");
    } else {
      setToken(storedToken);
      fetchAllMenu();
    }
  }, [router, fetchAllMenu]);

  const DynamicIcon = ({ icon }: { icon: IconNames | string }) => {
    const isValidIcon = Object.keys(Icons).includes(icon as string);

    if (!isValidIcon) {
      // Handle the case where icon is not a valid icon name
      return <Icons.RiLoaderFill />;
    }

    const IconComponent = Icons[icon as IconNames];

    if (!IconComponent) {
      // Handle the case where the IconComponent is not found
      return <Icons.RiLoaderFill />;
    }

    return <IconComponent />;
  };

  const handleSearch = (inputText: any) => {
    setSearch(inputText);
  };

  useEffect(() => {
    if (mobile) {
      setOpenMenu(false);
    }
  }, [mobile]);

  useEffect(() => {
    const fetchData = async () => {
      const res = await menuCategory;
      {
        search
          ? setAllData(
              res.filter((item: any) =>
                Object.values(item)
                  .join(" ")
                  .toLowerCase()
                  .includes(search.toLowerCase())
              )
            )
          : setAllData(res);
      }
    };
    fetchData();
  }, [search]);

  function cekOpen(index: any) {
    var isOpen = false;
    if (index == router.query.menu) {
      isOpen = true;
    }
    return isOpen;
  }

  return (
    <div className="flex flex-row w-screen h-screen">
      <div
        id="sidebar"
        className={`flex-col h-screen overflow-x-hidden overflow-y-hidden ${
          openMenu ? (mobile ? "w-[300px]" : "w-[320px]") : "w-0"
        } duration-700 bg-primary ${mobile ? "absolute z-10" : "relative"}`}
      >
        <div
          className={`flex mt-4 mb-5 mx-5 items-center ${
            mobile ? " justify-between" : "justify-center"
          }`}
        >
          <Link href={"/dashboard"}>
            <img
              src={"/aeon-white.png"}
              width={mobile ? 100 : 150}
              height={10}
              alt="Arena"
              className={` ${
                openMenu ? "duration-[2000ms]" : " opacity-0"
              } items-start`}
            />
          </Link>
          <MdArrowBack
            className={`text-white ${!mobile && "hidden"}`}
            onClick={() => setOpenMenu(!openMenu)}
          />
        </div>

        <div
          className={`flex flex-col h-full  gap-1 overflow-y-auto ${
            openMenu ? "duration-[2000ms]" : "opacity-0"
          }`}
        >
          <label
            className={`flex flex-row text-xs text-white mx-1 p-3 rounded-lg items-center h-5 mt-2 mb-0 uppercase font-semibold opacity-50`}
          >
            Menu
          </label>
          {/* {
                    groupedData.map((parent:any, index:number) => (
                      <ul key={parent.parent} className="menu menu-xs w-full">
                          <li>
                            <details open={cekOpen(index)}>
                              <summary className="text-white ml-3 hover:text-white hover:bg-primary-focus">
                                <div className="py-1 ">{parent.parent}</div>
                              </summary>
                              <ul>
                              {
                                parent.menus.map((menu:any,indexSub:number) => (
                                  <li key={menu.id}>
                                    <a href={menu.url + '?menu=' + index + '&sub=' + indexSub} className={`flex flex-row gap-2 text-xs cursor-pointer text-white p-2 hover:bg-primary-focus rounded-md ${indexSub == sub && 'active'}`}>
                                      <DynamicIcon icon={menu.icon}/>
                                      {menu.menu}
                                    </a>
                                  </li>
                                ))
                              }
                              </ul>
                            </details>
                          </li>
                      </ul>
                    ))
                  }   */}

          {groupedData.map((parent: GroupedData, index: number) => (
            <ul key={parent.parent} className="menu menu-xs w-full -mb-4">
              <li>
                <details open={cekOpen(index)}>
                  <summary className="text-white ml-3 hover:text-white hover:bg-primary-focus">
                    <div className="py-1 ">{parent.parent}</div>
                  </summary>
                  <ul>
                    {parent.menus.map((menu: Menu, indexSub: any) => (
                      <li key={menu.mmen_id}>
                        <a
                          href={`${menu.mmen_link}?menu=${index}&sub=${indexSub}`}
                          className={`flex flex-row gap-2 text-xs cursor-pointer text-white p-2 hover:bg-primary-focus rounded-md ${
                            indexSub === sub && "active"
                          }`}
                        >
                          {/* Assuming you have a DynamicIcon component that accepts an icon prop */}
                          {/* <DynamicIcon icon={menu.mmen_icon} /> */}
                          <DynamicIcon icon={menu.mmen_icon} />
                          {/* <span>{menu.mmen_icon}</span> */}
                          <span>{menu.mmen_name}</span>
                        </a>
                      </li>
                    ))}
                  </ul>
                </details>
              </li>
            </ul>
          ))}
        </div>
      </div>
      <div className={`flex flex-col w-full duration-1000`}>
        <div
          className={`justify-between flex flex-row w-full items-center  bg-gray-100 px-3 gap-1 py-3`}
        >
          <Image
            onClick={() => setOpenMenu(!openMenu)}
            src={"/aeon.png"}
            width={90}
            height={10}
            alt="Aeon"
            className={`mx-2 ${
              openMenu ? "w-0 hidden" : ""
            } cursor-pointer p-1`}
          />
          <MdMenu
            className={`${
              openMenu ? " w-10" : "w-0 hidden"
            } flex text-3xl text-primary rounded-3xl p-1 cursor-pointer mx-3`}
            onClick={() => setOpenMenu(!openMenu)}
          />

          <div
            className={`flex flex-row gap-1 ${
              openMenu && mobile && "opacity-5"
            }`}
          >
            <div
              onClick={() => {
                setOpenProfile(!openProfile);
                setOpenArena(false);
              }}
              className={`${
                openMenu && mobile && ""
              } duration-1000 flex max-w-sm px-4 py-2 text-xs gap-1 bg-primary rounded-2xl justify-center cursor-pointer hover:bg-primary-focus`}
            >
              <div className="text-white opacity-50">Login as</div>
              <div className="text-white font-bold">{userData[0].name}</div>
            </div>
          </div>
        </div>

        <div id="content" className={`flex w-full overflow-y-auto`}>
          {props.content}
        </div>
      </div>

      <div
        className={`${
          !openProfile && "hidden"
        } right-3 mt-[55px] flex justify-center absolute cursor-pointer`}
      >
        <Link
          href={"/logout"}
          className={`flex flex-row duration-1000 h-10 w-[250px] items-center justify-center gap-1 bg-white border-2 border-primary shadow-md shadow-gray-300 rounded-3xl p-2`}
        >
          {/* <div className={`h-full w-full bg-gray-100 rounded-[33px]`}> */}
          <Icons.RiCloseCircleFill className="text-primary" />
          <h1 className="text-primary">Logout</h1>
          {/* </div> */}
        </Link>
      </div>
    </div>
  );
};

export default Navigation;
