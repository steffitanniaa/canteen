const TextSubtitleSecondary = ({ text }:{ text:string }) => {
    return(
        <>
            <h1 className='text-lg font-semibold text-secondary mt-2 capitalize'>{text}</h1>
        </>
    )
}

export default TextSubtitleSecondary