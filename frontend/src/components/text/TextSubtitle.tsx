const TextSubtitle = ({ text }:{ text:string }) => {
    return(
        <>
            <h1 className='text-md font-semibold mt-2'>{text}</h1>
        </>
    )
}

export default TextSubtitle