import { ChangeEventHandler, useState, useEffect } from "react";

const InputSelectForm = ({
  onChange,
  id,
  label,
  classname,
  note,
  ops,
  selectText,
  selectTextAlt,
  selectValue,
  type,
  defaultValue,
  defaultText
}: {
  onChange: ChangeEventHandler;
  id: string;
  label: string;
  classname: string;
  note: string;
  ops: any;
  selectText: any;
  selectTextAlt: any;
  selectValue: any;
  type:number;
  defaultValue:any;
  defaultText:string
}) => {
  const [selected, setSelected] = useState("");

  const handleValue = (event: any) => {
    setSelected(event.target.value);
    onChange(event); // Memastikan fungsi onChange juga terpanggil jika diperlukan
    // console.log(event.target.value)
  };

  return (
    <div className={`flex flex-col ${classname}`}>
            <div className={`flex flex-col gap-1 p-2 rounded-md `}>
                <div className='text-xs text-gray-400 capitalize'>{label}</div>
        <select
          value={selected} // Gunakan nilai dari state selected
          className="input input-sm h-7 input-bordered leading-tight focus:outline-none p-1.5 rounded-md text-xs appearance-none  text-primary cursor-pointer"
          onChange={handleValue}
          id={id}
          name={id}
        >
          {
            type == 1 ? <option value={""}>Select {label}</option>
            : <option value={defaultValue}>{defaultText}</option>
          }
          {ops.map((option: any, index: any) => (
            <option
              selected
              key={index}
              value={option[`${selectText}`] + "|" + option[`${selectValue}`]}
            >
              {
                selectTextAlt ? option[`${selectText}`] + ' - ' + option[`${selectTextAlt}`] :  option[`${selectText}`]
              }
            </option>
          ))}
        </select>
      </div>
      <div className="flex w-full justify-end text-primary text-xs text capitalize">
        {note}
      </div>
    </div>
  );
};

export default InputSelectForm;
