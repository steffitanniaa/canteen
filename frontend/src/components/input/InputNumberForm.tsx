import { ChangeEventHandler } from "react"

const InputNumberForm = ({onChange, label, classParent, classChild, note, value}: {onChange:ChangeEventHandler, label:string, classParent:string, classChild:string, note:string, value:string}) => {
    return(
        <div className={`flex flex-col ${classParent}`}>
            <div className={`flex flex-col gap-1 p-2 rounded-md `}>
                <div className='text-xs text-gray-400 capitalize'>{label}</div>
                <input type='number' value={value} min={0} id={'label'} placeholder={`Enter ${label}`} onChange={onChange} className={`border-gray-200 border-[1px] leading-tight focus:outline-none p-1.5 rounded-md text-xs appearance-none text-primary ${classChild}`}/>
            </div>
            <label className="text-gray-700 text-xs text">{note}</label>
        </div>
    )
}

export default InputNumberForm