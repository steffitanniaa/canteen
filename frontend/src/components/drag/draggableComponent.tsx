import React from 'react';

interface DraggableComponentProps {
  id: string;
  content: string;
  handleDragStart: (e: React.DragEvent, id: string) => void;
}

const DraggableComponent: React.FC<DraggableComponentProps> = ({ id, content, handleDragStart }) => {
  const handleDrag = (e: React.DragEvent) => {
    e.preventDefault();
  };

  return (
    <div
      draggable
      onDragStart={(e) => handleDragStart(e, id)}
      onDrag={handleDrag}
      style={{ border: '1px solid #ccc', padding: '8px', margin: '8px' }}
    >
      {content}
    </div>
  );
};

export default DraggableComponent;
