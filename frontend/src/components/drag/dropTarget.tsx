import React, { ReactNode } from 'react';

interface DropTargetProps {
  handleDrop: (e: React.DragEvent) => void;
  children: ReactNode; // Explicitly define children prop type
}

const DropTarget: React.FC<DropTargetProps> = ({ children, handleDrop }) => {
  const handleDragOver = (e: React.DragEvent) => {
    e.preventDefault();
  };

  return (
    <div
      onDragOver={handleDragOver}
      onDrop={(e) => handleDrop(e)}
      style={{ border: '2px dashed #aaa', minHeight: '100px', padding: '16px' }}
    >
      {children}
    </div>
  );
};

export default DropTarget;
