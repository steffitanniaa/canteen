import Compact from "@/utils/compact";

const ButtonTest = () => {
  const mobile = Compact();

  return (
    <div
      className={`flex ${
        mobile ? "w-2/3" : "w-1/2"
      } h-52 bg-primary rounded-lg text-white justify-center items-center text-3xl font-bold cursor-pointer hover:bg-secondary`}
    >
      Add
    </div>
  );
};

export default ButtonTest;
