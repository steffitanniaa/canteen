import Compact from "@/utils/compact"
import { useEffect, useState } from "react"
import { AiFillWechat } from "react-icons/ai"
import { MdClose } from "react-icons/md"

const BubbleChat = () =>{
    const mobile = Compact()
    const [showChat, setShowChat] = useState(false)
    const [loading, setLoading] = useState(false)
    function showChatBubble(){
        setShowChat(!showChat)
    }

    
    const [chatWidth, setChatWidth] = useState(0)

    useEffect(() => {
      const updateChatWidth = () => {
        const chatElement = document.getElementById('bubble-chat');
        if (chatElement) {
          setChatWidth(chatElement.offsetWidth);
        }
      };
  
      updateChatWidth(); // Initial width
      window.addEventListener('resize', updateChatWidth);
  
      return () => {
        window.removeEventListener('resize', updateChatWidth);
      };
    }, []);

    const isHidden = chatWidth < 200;
    
    return(
        <div
        id="bubble-chat"
        className={`absolute bottom-5 right-5 z-50 ${
          !showChat ?  'w-12 h-12'
          : mobile ? 'h-[500px] w-full pl-10' 
          : 'h-[600px] w-[400px]'
        } duration-500 border-white shadow rounded-2xl ${isHidden && 'hidden'}`}>
          <div className={`flex w-full h-full bg-primary shadow-gray-600 shadow-sm rounded-2xl p-3`}>
              {
                showChat &&
                <div className='flex flex-col w-full h-full gap-3'>
                  <div className='flex flex-row justify-end items-center w-full h-3'>
                    {/* <div className=''>CHAT & ATTACHMENT</div> */}
                    <div className={`w-full flex text-white text-xs`}>Team Chat {isHidden.toString()}</div>
                    <div className='flex cursor-pointer' onClick={showChatBubble}>
                      <MdClose className={`text-white w-5 h-5`}/>
                    </div>
                  </div>
                  <div className='flex flex-row bg-white w-full h-full rounded-t-lg'>
                    {
                      <div className='hidden'>
 
                        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam expedita laborum aut non quidem itaque nulla a deserunt ad dolores ullam fugit in, corrupti tenetur atque repellat odit accusantium nihil?'
    
                      </div>
                    }
                  </div>
                  <div className='flex flex-row bg-white w-full h-1/6 rounded-b-lg'>

                  </div>
                </div>
              }
          <AiFillWechat className={`w-9 h-9 text-white absolute bottom-1 right-1 ${showChat ?  'opacity-0' : 'opacity-100 cursor-pointer'} duration-1000`} onClick={showChatBubble}/>
        </div>
    </div>
    )
}

export default BubbleChat