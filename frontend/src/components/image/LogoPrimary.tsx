import Image from 'next/image'

const LogoPrimary = () => {

    return(
        <>
            <img className={`mx-auto`}
                alt='Workflow'
                width={140} height={50}
                src={'/aeon.png'}
            />
        </>
    )
}

export default LogoPrimary