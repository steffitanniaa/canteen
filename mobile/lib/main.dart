import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apk/src/features/feature1/dashboard/index.dart';
import 'package:apk/src/features/feature1/settings/index.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(fontFamily: 'Poppins'),
      home: const SplashScreen(), // Mengatur splash screen sebagai halaman awal
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  static const platform = MethodChannel('com.example.apk/java_channel');

  @override
  void initState() {
    super.initState();
    _checkStoreSelected();
  }

  Future<void> _checkStoreSelected() async {
    final prefs = await SharedPreferences.getInstance();
    final selectedStore = prefs.getString('selectedStore');

    print('Selected Store: $selectedStore');

    if (selectedStore != null) {
      // Jika sudah ada selectedStore, langsung navigasi ke dashboard
      navigateToHome();
    } else {
      // Jika belum ada selectedStore, fetch data toko dan simpan ke SharedPreferences
      _fetchAndSaveStoreData();
    }
  }

  Future<void> _fetchAndSaveStoreData() async {
    final prefs = await SharedPreferences.getInstance();
    final response = await http.post(
      Uri.parse('http://10.123.34.134:3010/api/v1/android/dashboard/store'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({}),
    );

    List<dynamic> data = jsonDecode(response.body)['data'];
    List<String> storeList = data.map((store) {
      final storeName = store['mcdb_flag_source'];
      return storeName as String;
    }).toList();

    await prefs.setStringList('stores', storeList);

    // Navigate to home page after fetching data
    navigateToHome();
  }

  Future<void> navigateToHome() async {
    // Tunggu beberapa detik (misalnya 3 detik) untuk menampilkan splash screen
    await Future.delayed(const Duration(seconds: 3));

    // Lakukan navigasi ke halaman utama
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => const MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }

  Future<void> navigateToSettings() async {
    await Future.delayed(const Duration(seconds: 3));

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => SettingsPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              const Color(0xFFAB2180), // Deep Purple (#AB2180)
              const Color(0xFF541440), // Blue (#541440)
            ], // Sesuaikan warna sesuai keinginan
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              // Ganti CircularProgressIndicator dengan Image
              Expanded(
                child: FractionallySizedBox(
                  widthFactor:
                      0.5, // Menentukan faktor lebar gambar (0.5 untuk setengah dari lebar layar)
                  heightFactor:
                      0.5, // Menentukan faktor tinggi gambar (0.5 untuk setengah dari tinggi layar)
                  child: Image(
                    image: AssetImage(
                      'lib/src/features/feature1/assets/images/logo_white.png',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = MethodChannel('com.example.apk/java_channel');
  int _counter = 0;

  String? _valFriends;

  bool _isStoreSelected =
      false; // Tambahkan variabel untuk melacak pemilihan store

  List<String> _myFriends = [];

  @override
  void initState() {
    super.initState();
    _loadStoreData();
  }

  Future<void> _loadStoreData() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? storeList = prefs.getStringList('stores');
    setState(() {
      _myFriends = storeList ?? [];
      if (_myFriends.isNotEmpty) {
        _valFriends = _myFriends[0];
      }

      // Check if a store is already selected
      _isStoreSelected = _myFriends.contains(prefs.getString('selectedStore'));

      // Add "Select Store" as the initial option if not selected yet
      if (!_isStoreSelected) {
        _myFriends.insert(0, "Select Store");
        _valFriends = "Select Store";
      }
    });

    // Check if store is already selected, then navigate to dashboard
    if (_isStoreSelected) {
      navigateToDashboard();
    }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
      getJavaMessage();
    });
  }

  Future<String> getJavaMessage() async {
    String message;
    try {
      message = await platform.invokeMethod('getJavaMessage');
      print(message);
    } on PlatformException catch (e) {
      message = "Failed to get message from Java: '${e.message}'.";
    }
    return message;
  }

  Future<void> _saveSelectedStore(String selectedStore) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('selectedStore', selectedStore);
    setState(() {
      _isStoreSelected = true; // Update status to store selected
    });
    print('Store Saved: $selectedStore'); // Tambahkan debug log
  }

  Future<String?> _getSelectedStore() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('selectedStore');
  }

  Future<void> navigateToDashboard() async {
    // Lakukan navigasi ke halaman dashboard
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => DashboardPage(selectedStore: _valFriends!),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 0.0),
            padding: const EdgeInsets.all(0.0),
            child: Expanded(
              child: Align(
                alignment: Alignment.topRight,
                child: Image(
                  image: AssetImage(
                    'lib/src/features/feature1/assets/images/flower.png',
                  ),
                ),
              ),
            ),
          ),
          // Tolong posisinya bottom 0
          Expanded(
            child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 80.0, vertical: 80.0),
              alignment: Alignment.bottomCenter, // Posisi bagian bawah
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 150),
                    child: const Text.rich(
                      TextSpan(
                        text: 'Hello !',
                      ),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 60,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 5.0),
                    child: const Text.rich(
                      TextSpan(
                        text: 'Please Choose one store for setup',
                      ),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 20.0),
                    child: const Text.rich(
                      TextSpan(
                        text: 'and enjoy your workplace ',
                      ),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 20.0),
                    child: DropdownButton<String>(
                      isExpanded: true,
                      hint: const Text("Select Your Store"),
                      value: _valFriends,
                      items: _myFriends.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        if (value != "Select Store") {
                          setState(() {
                            _valFriends = value;
                            _saveSelectedStore(value!);
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () {
                        if (_valFriends != "Select Store" &&
                            _valFriends != null) {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    DashboardPage(selectedStore: _valFriends)),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Please select a store.'),
                            ),
                          );
                        }
                      },
                      child: const Text('SUBMIT',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                          )),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0xFFAB2180),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius here
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
