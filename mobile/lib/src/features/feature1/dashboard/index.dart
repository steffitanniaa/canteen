import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apk/src/features/feature1/settings/index.dart';
import 'package:apk/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const MyAppDashboard());
}

class MyAppDashboard extends StatelessWidget {
  const MyAppDashboard({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.deepPurple),
          fontFamily: 'Poppins'),
      home: DashboardPage(), // Mengatur splash screen sebagai halaman awal
    );
  }
}

class DashboardPage extends StatefulWidget {
  final String? selectedStore;

  DashboardPage({Key? key, this.selectedStore}) : super(key: key);
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

// Dialog synchronize data //
void showDialogWithLoading(BuildContext context) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: contentBoxSearching(),
      );
    },
  );
}

void showResultDialog(BuildContext context, String result, bool isSuccess) {
  print(isSuccess);
  Color backgroundColor = isSuccess
      ? const Color(0xFFB5EBCC)
      : const Color(
          0xFFFFE3E0); // Tentukan warna latar belakang sesuai isSuccess
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10), // Atur radius sudut
        ),
        child: Container(
          constraints:
              BoxConstraints(maxWidth: 300), // Atur lebar maksimal dialog
          decoration: BoxDecoration(
            color: backgroundColor, // Warna latar belakang container
            borderRadius: BorderRadius.circular(10), // Radius sudut container
            border: Border(
              bottom: BorderSide(
                color: isSuccess
                    ? const Color(0xFF13B04C)
                    : const Color(0xFFFE3E2C), // Warna border bawah
                width: 3, // Ketebalan border
              ),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0), // Padding untuk konten
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  isSuccess ? Icons.check_circle_rounded : Icons.error,
                  color: isSuccess
                      ? const Color(0xFF13B04C)
                      : const Color(0xFFFE3E2C),
                  size: 40,
                ),
                SizedBox(width: 15), // Spacing antara ikon dan teks
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        isSuccess ? 'Success' : 'Error',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: isSuccess
                              ? const Color(0xFF13B04C)
                              : const Color(0xFFFE3E2C),
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                      // SizedBox(height: 8),
                      Text(
                        result, // Teks hasil yang ingin Anda tampilkan
                        textAlign: TextAlign.start, // Teks menjadi di kiri
                        style: TextStyle(
                          fontWeight: FontWeight.bold, // Teks menjadi bold
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
  // Simulate a delay for loading
  Future.delayed(Duration(seconds: 2), () {
    Navigator.of(context).pop(); // Close the dialog
  });
}

class _DashboardPageState extends State<DashboardPage> {
  String _response = '';
  String? _selectedStore;
  static const platform = MethodChannel('com.example.apk/java_channel');
  int _counter = 0;
  double logoTopPosition = 350; // Definisikan nilai awal posisi top logo
  TextEditingController _textFieldController = TextEditingController();
  FocusNode _textFieldFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    // _textFieldController.addListener(textFieldListener);
    // _textFieldFocusNode.addListener(textFieldFocusListener);
    _loadSelectedStore();
    _loadCounter();
    getJavaMessage();
  }

  Future<void> _loadSelectedStore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _selectedStore = prefs.getString('selectedStore');
    });
  }

  Future<void> _loadCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    DateTime now = DateTime.now();
    String today = "${now.year}-${now.month}-${now.day}";
    String? savedDate = prefs.getString('date');

    if (savedDate != today) {
      await prefs.setString('date', today);
      await prefs.setInt('counter', 0);
      setState(() {
        _counter = 0;
      });
    } else {
      setState(() {
        _counter = prefs.getInt('counter') ?? 0;
      });
    }
  }

  Future<void> _incrementCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _counter++;
    });
    await prefs.setInt('counter', _counter);
  }

  Future<String> getJavaMessage() async {
    String message;
    try {
      message = await platform.invokeMethod('getJavaMessage');
      print(message);

      // print(response);
    } on PlatformException catch (e) {
      message = "Failed to get message from Java: '${e.message}'.";
    }
    return message;
  }

  Future<String> f_onTap(String param) async {
    String message;
    // message = "";

    DateTime start = DateTime.now();
    print("Start : " + start.toString());
    try {
      showDialogWithLoading(context);

      // Save the data locally
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('rfid', param);
      await prefs.setString('store', _selectedStore ?? "");

      // Retrieve the data from local storage
      String? rfid = prefs.getString('rfid');
      print('RFID: $rfid');
      String? store = prefs.getString('store');
      print('Store: $store');

      // Simulate network request delay
      await Future.delayed(Duration(seconds: 1));

      final response = await http.post(
        Uri.parse('http://10.123.34.134:3010/api/v1/android/dashboard/tap'),
        body: {
          'rfid': rfid,
          'store': store,
        },
      );

      Navigator.of(context).pop(); // Close the dialog

      setState(() {
        _response = 'POST Response: ${response.body}';
      });

      // IF ELSE CONDITION
      Map<String, dynamic> jsonData = jsonDecode(response.body);
      bool status = jsonData['status'];

      if (status) {
        message = "${jsonData['message']}";

        showResultDialog(context, message, true);
        // Map<String, dynamic> test = jsonDecode(jsonData['data']);
        // print(test['museName']);
        Map<String, dynamic> data = jsonData['data'];
        String museName = data['museName'];
        String dateTime = data['tmor_order_datetime'];
        String museNik =
            data['museNik'] != null ? data['museNik'].toString() : '';

        String orderId = jsonData['data']['orderId'].toString();

        print('orderId: $orderId');
        print('museName: $museName');
        print('tmor_order_datetime: $dateTime');
        print('muse_user_nik: $museNik');
        print('tmor_order_id: $orderId');

        await _incrementCounter();

        // message = await platform.invokeMethod('print');

        message = await platform.invokeMethod('print', {
          'rfid': rfid,
          'store': store,
          'museName': museName,
          'dateTime': dateTime,
          'museNik': museNik,
          'orderId': orderId,
        });
      } else {
        message = "${jsonData['message']}";
        showResultDialog(
            context, message, false); // Memanggil dengan isSuccess true
      }

      // message = await platform.invokeMethod('print');
      print(message);
      print(_response);
    } on PlatformException catch (e) {
      message = "Failed to get message from Java: '${e.message}'.";
    }
    DateTime end = DateTime.now();
    print("End : " + end.toString());
    print("Duration: " + (end.difference(start)).toString());

    return message;
  }

  // Fungsi listener untuk TextField
  void textFieldListener() {
    // Lakukan sesuatu ketika fokus diterapkan pada TextField
    print('TextField mendapat fokus!');
    // Panggil fungsi atau jalankan kode lainnya di sini
    setState(() {
      // Ganti nilai posisi top logo sesuai kebutuhan
      logoTopPosition = 400; // Contoh nilai baru posisi top
    });
    getJavaMessage();
  }

  // Fungsi listener untuk FocusNode TextField
  void textFieldFocusListener() {
    if (!_textFieldFocusNode.hasFocus) {
      print('TextField kehilangan fokus!');
      // Panggil fungsi atau jalankan kode lainnya di sini
      notFocus();
    }
  }

  // Fungsi yang akan dijalankan saat TextField kehilangan fokus
  void notFocus() {
    print('TextField kehilangan fokus!');
    // Tampilkan alert
    setState(() {
      // Ganti nilai posisi top logo sesuai kebutuhan
      logoTopPosition = 200; // Contoh nilai baru posisi top
    });
  }

  Future<void> sendDataToKotlin(String? rfid, String? store) async {
    try {
      await platform.invokeMethod('sendData', {'rfid': rfid, 'store': store});
    } on PlatformException catch (e) {
      print("Failed to send data to Kotlin: '${e.message}'.");
    }
  }

  void onTap(param) {
    f_onTap(param);
    print("RFID : '${param}'");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset:
          false, // Resize untuk menghindari inset di bagian bawah
      body: Stack(
        children: [
          // Widget lainnya...
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'lib/src/features/feature1/assets/images/header_index.jpg',
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 40.0, top: 0.0),
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  children: [
                    // Kotak kiri
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 40.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                ),
                                // child: IconButton(
                                //   icon: Icon(
                                //     Icons.home,
                                //     color: const Color(0xFFAB2180),
                                //   ),
                                //   onPressed: () {
                                //     Navigator.push(
                                //       context,
                                //       MaterialPageRoute(
                                //           builder: (context) => MyHomePage(
                                //               title: 'Flutter Demo Home Page')),
                                //     );
                                //   },
                                // ),
                              ),
                            ),
                            SizedBox(width: 5),
                            Text(
                              'STORE : $_selectedStore',
                              style: TextStyle(
                                color: const Color(0xFFAB2180),
                                fontSize: 16,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    // Kotak kanan
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 40.0),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: IconButton(
                                icon: Icon(
                                  Icons.more_vert,
                                  color: const Color(0xFFAB2180),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SettingsPage(
                                            selectedStore: _selectedStore)),
                                  );
                                  // Perbarui data store setelah kembali dari halaman pengaturan
                                  _loadSelectedStore();
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // CarouselSlider Widget
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Container(
                      margin: EdgeInsets.only(top: 40.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            // Kolom pertama untuk "Employee RFID"
                            SizedBox(height: 200),
                            // Input field dengan ikon
                            Padding(
                              padding: EdgeInsets.only(
                                left: 80.0,
                                top: 10.0,
                                right: 80.0,
                                bottom: 0,
                              ),
                              child: TextField(
                                controller: _textFieldController,
                                focusNode: _textFieldFocusNode,
                                autofocus: true,
                                onSubmitted: (String value) {
                                  // Panggil fungsi Anda di sini
                                  onTap(value);

                                  _textFieldController.clear();
                                },
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.credit_card,
                                    color: const Color(0xFFAB2180),
                                  ),
                                  hintText: 'Enter employee RFID',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: const Color(0xFFAB2180),
                                      width: 1.0,
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: const Color(0xFFAB2180),
                                      width:
                                          2.0, // Misalnya, lebar border saat input sedang difokus
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 100),
                            Container(
                              width: 350,
                              height: 230,
                              decoration: BoxDecoration(
                                color: const Color(0xFFAB2180).withOpacity(0.1),
                                borderRadius: BorderRadius.circular(15),
                                // boxShadow: [
                                //   BoxShadow(
                                //     color: Colors.grey.withOpacity(0.5),
                                //     spreadRadius: 1,
                                //     blurRadius: 10,
                                //     offset: Offset(0, 5),
                                //   ),
                                // ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.person,
                                    size: 50,
                                    color: const Color(0xFFAB2180),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    '$_counter',
                                    style: TextStyle(
                                      fontSize:
                                          88, // Mengurangi ukuran font untuk sesuaikan dengan Container
                                      fontWeight: FontWeight.w800,
                                      color: const Color(0xFFAB2180),
                                    ),
                                  ),
                                  SizedBox(
                                      height:
                                          5), // Mengurangi ukuran SizedBox agar tidak terlalu besar
                                  Text(
                                    'Today Visitor',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: const Color(0xFFAB2180),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          // Positioned untuk teks "aaaaa" dan "sssss"
          Positioned(
            top: logoTopPosition,
            left: MediaQuery.of(context).size.width / 2 -
                190, // Setengah dari lebar layar dikurangi setengah dari lebar gambar
            child: Padding(
              padding: EdgeInsets.only(
                  left: 20.0, top: 20.0, right: 20.0, bottom: 0),
              child: Transform.translate(
                offset: Offset(0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage(
                        'lib/src/features/feature1/assets/images/logo_bulat.png',
                      ),
                      width: 350,
                      height: 350,
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// Ini widget untuk synchronize //
Widget contentBoxSearching() {
  return Container(
    width: 200,
    height: 200,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    decoration: BoxDecoration(
      shape: BoxShape.rectangle,
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CircularProgressIndicator(
          color: const Color(0xFFAB2180),
        ),
        SizedBox(height: 20),
        Text(
          "Searching",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    ),
  );
}
