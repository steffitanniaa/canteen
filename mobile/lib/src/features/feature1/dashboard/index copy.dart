import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyAppDashboard());
}

class MyAppDashboard extends StatelessWidget {
  const MyAppDashboard({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.deepPurple),
      ),
      home: DashboardPage(), // Mengatur splash screen sebagai halaman awal
    );
  }
}

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  static const platform = MethodChannel('com.example.apk/java_channel');
  int _counter = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              'lib/src/features/feature1/assets/images/header_index.jpg',
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 40.0, top: 0.0),
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                children: [
                  // Kotak kiri
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 40.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape
                                    .circle, // Mengatur bentuk menjadi lingkaran
                                color:
                                    Colors.white, // Warna latar belakang putih
                              ),
                              child: IconButton(
                                icon: Icon(
                                  Icons.home,
                                  color: Colors.black,
                                ),
                                onPressed: () {
                                  // Tambahkan fungsi untuk menampilkan menu dot tiga jika diperlukan
                                },
                              ),
                            ),
                          ),
                          SizedBox(width: 5), // Jarak antara ikon dan teks
                          Text(
                            'Store BSD',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                      width: 20), // Tambahkan jarak antara kotak kiri dan kanan
                  // Kotak kanan
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 40.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape
                                  .circle, // Mengatur bentuk menjadi lingkaran
                              color: Colors.white, // Warna latar belakang putih
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.more_vert,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                // Tambahkan fungsi untuk menampilkan menu dot tiga jika diperlukan
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // CarouselSlider Widget
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white, // Latar belakang putih
                      borderRadius:
                          BorderRadius.circular(20.0), // Border radius
                    ),
                    width: double.infinity, // Lebar penuh
                    height: MediaQuery.of(context).size.height * 0.6,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 0.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // Kolom pertama untuk "Employee RFID"
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 20.0,
                                          top: 20.0,
                                          right: 20.0,
                                          bottom: 0),
                                      child: Text(
                                        'Employee RFID', // Label di atas input field
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                        height:
                                            8), // Tambahkan jarak antara teks dan input field
                                    // Input field dengan ikon
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 20.0,
                                          top: 10.0,
                                          right: 20.0,
                                          bottom: 0),
                                      child: TextField(
                                        decoration: InputDecoration(
                                          prefixIcon: Icon(Icons.person),
                                          hintText: 'Enter employee RFID',
                                          border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black,
                                                width: 1.0),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          filled: true,
                                          fillColor: Colors.white,
                                        ),
                                      ),
                                    ),

                                    SizedBox(
                                        height:
                                            20), // Tambahkan jarak antara kolom pertama dan kedua
                                    // Kolom kedua dengan dua kotak di dalamnya
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0),
                                      child: Row(
                                        children: [
                                          // Kotak kiri
                                          Expanded(
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  8.0), // Ubah nilai padding sesuai kebutuhan Anda
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Color(0xFFAB2181)
                                                      .withOpacity(
                                                          0.12), // Opasitas 12%// Warna #AB2181
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                                child: Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(
                                                        30.0), // Atur sesuai kebutuhan Anda
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Icon(
                                                          Icons.person,
                                                          color:
                                                              Color(0xFFAB2181),
                                                          size: 30.0,
                                                        ),
                                                        Text(
                                                          '000',
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xFFAB2181),
                                                            fontSize: 80.0,
                                                          ),
                                                        ),
                                                        Text(
                                                          'Today Visitor',
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xFFAB2181),
                                                            fontSize: 20.0,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),

                                          SizedBox(
                                              width:
                                                  20), // Tambahkan jarak antara kotak kiri dan kanan
                                          // Kotak kanan
                                          Expanded(
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  8.0), // Ubah nilai padding sesuai kebutuhan Anda
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Color(
                                                      0xFFAB2181), // Warna #AB2181
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                                child: Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(
                                                        30.0), // Atur sesuai kebutuhan Anda
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Icon(
                                                          Icons.person,
                                                          color: Colors.white,
                                                          size: 30.0,
                                                        ),
                                                        Text(
                                                          '000',
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 80.0,
                                                          ),
                                                        ),
                                                        Text(
                                                          'Today Visitor',
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 20.0,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
