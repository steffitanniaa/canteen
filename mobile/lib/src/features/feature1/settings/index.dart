import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:apk/main.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SettingsPage(),
    );
  }
}

class SettingsPage extends StatefulWidget {
  final String? selectedStore;

  SettingsPage({Key? key, this.selectedStore}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _isOfflineModeEnabled = true; // Default value

  String? _currentStore;
  List<String> _storeList = [];

  @override
  void initState() {
    super.initState();
    _loadSelectedStore();
    _fetchStoreList();
  }

  Future<void> _loadSelectedStore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _currentStore = prefs.getString('selectedStore');
    });
  }

  Future<void> _fetchStoreList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _storeList = prefs.getStringList('stores') ?? _storeList;
    });
  }

  // Fungsi untuk memilih store baru

  // void _changeSelectedStore(BuildContext context) async {
  //   String? newStore = await showDialog<String>(
  //     context: context,
  //     builder: (BuildContext context) {
  //       String? tempStore = _currentStore;
  //       return StatefulBuilder(
  //         builder: (BuildContext context, StateSetter setState) {
  //           return AlertDialog(
  //             title: Text('Select Store'),
  //             content: DropdownButton<String>(
  //               isExpanded: true,
  //               value: tempStore,
  //               items: _storeList.map((String value) {
  //                 return DropdownMenuItem<String>(
  //                   value: value,
  //                   child: Text(value),
  //                 );
  //               }).toList(),
  //               onChanged: (String? newValue) {
  //                 setState(() {
  //                   tempStore = newValue;
  //                 });
  //               },
  //             ),
  //             actions: <Widget>[
  //               TextButton(
  //                 child: Text('Cancel'),
  //                 onPressed: () {
  //                   Navigator.of(context).pop(null);
  //                 },
  //               ),
  //               TextButton(
  //                 child: Text('Save'),
  //                 onPressed: () async {
  //                   SharedPreferences prefs =
  //                       await SharedPreferences.getInstance();
  //                   await prefs.setString('selectedStore', tempStore ?? '');

  //                   Navigator.of(context).pop(tempStore);
  //                 },
  //               ),
  //             ],
  //           );
  //         },
  //       );
  //     },
  //   );

  //   if (newStore != null && newStore != _currentStore) {
  //     setState(() {
  //       _currentStore = newStore;
  //     });
  //   }
  // }

  void _changeSelectedStore(BuildContext context) async {
    String? newStore = await showModalBottomSheet<String>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        String? tempStore = _currentStore;
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return FractionallySizedBox(
              heightFactor:
                  0.4, // Mengatur tinggi modal menjadi 50% dari tinggi layar
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Select Store',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 2.0 *
                          MediaQuery.of(context).size.width /
                          100, // Mengonversi cm ke piksel
                    ),
                    Expanded(
                      child: ListView(
                        shrinkWrap: true,
                        children: _storeList.map((String value) {
                          return ListTile(
                            title: Text(
                              value,
                              style: TextStyle(color: const Color(0xFFAB2180)),
                            ),
                            trailing: tempStore == value
                                ? Icon(Icons.check,
                                    color: const Color(0xFFAB2180))
                                : null,
                            onTap: () {
                              setState(() {
                                tempStore = value;
                              });
                            },
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(height: 16.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal:
                                  10.0), // Jarak horizontal dari tepi kontainer
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors
                                  .grey, // Border color abu-abu untuk Cancel
                            ),
                            borderRadius: BorderRadius.circular(18.0),
                            color:
                                Colors.grey, // Warna fill abu-abu untuk Cancel
                          ),
                          child: TextButton(
                            onPressed: () {
                              Navigator.of(context).pop(null);
                            },
                            child: Text(
                              'Cancel',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal:
                                  10.0), // Jarak horizontal dari tepi kontainer
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: const Color(
                                  0xFFAB2180), // Border color merah untuk Save
                            ),
                            borderRadius: BorderRadius.circular(18.0),
                            color: const Color(
                                0xFFAB2180), // Warna fill merah untuk Save
                          ),
                          child: TextButton(
                            onPressed: () async {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              await prefs.setString(
                                  'selectedStore', tempStore ?? '');
                              Navigator.of(context).pop(tempStore);
                            },
                            child: Text(
                              'Save',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );

    if (newStore != null && newStore != _currentStore) {
      setState(() {
        _currentStore = newStore;
      });
    }
  }

  // Dialog synchronize data //
  void showDialogWithLoading(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: contentBoxClear(),
        );
      },
    );
  }

  // Dialog untuk Clear Data //
  Future<void> showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Clear all data?'),
          content: Text(
              "All this app's data will be deleted permanently. This includes all files, store setting, database, etc."),
          actions: <Widget>[
            TextButton(
              child: Text(
                'CANCEL',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
              style: TextButton.styleFrom(
                backgroundColor: Colors.grey
                    .withOpacity(0.5), // Set background color for cancel button
              ),
              onPressed: () {
                Navigator.of(context).pop(); // Close dialog after submission
              },
            ),
            TextButton(
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
              style: TextButton.styleFrom(
                backgroundColor: const Color(
                    0xFFAB2180), // Set background color for OK button
              ),
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                List<String> tempStoreList =
                    prefs.getStringList('stores') ?? [];

                await prefs.clear();
                // await prefs.remove('selectedStore');

                showDialogWithLoading(context);

                await prefs.setStringList('stores', tempStoreList);
                setState(() {
                  _currentStore =
                      null; // Set current store to null or any default value
                  _storeList = tempStoreList; // Restore store list
                });

                // Simulate network request delay
                await Future.delayed(Duration(seconds: 1));

                Navigator.of(context)
                    .pop(); // Tutup dialog saat tombol Close ditekan

                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          MyHomePage(title: 'Flutter Demo Home Page')),
                );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Settings',
          style: TextStyle(
            fontSize: 20.0,
            color: Colors.white, // Ukuran teks disesuaikan dengan kebutuhan
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true, // Menyatukan judul ke tengah
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Color(0xFFAB2181), // Warna app bar yang diinginkan
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text(
              'STORE : $_currentStore',
              style: TextStyle(
                color: const Color(0xFFAB2180),
                fontSize: 16,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          ListTile(
            tileColor: Colors.grey.withOpacity(0.1),
            leading: Icon(Icons.edit),
            title: Text('Edit store'),
            subtitle: Text('Store settings and details'),
            onTap: () {
              _changeSelectedStore(context);
              // Tambahkan fungsi yang ingin dijalankan saat item diklik
            },
            selectedTileColor: Colors.grey.withOpacity(0.5),
          ),
          // ListTile(
          //   tileColor: Colors.grey.withOpacity(0.1),
          //   leading: Icon(Icons.offline_bolt),
          //   title: Text('Offline mode'),
          //   subtitle: Text('Enable or disable offline mode'),
          //   trailing: Switch(
          //     value: _isOfflineModeEnabled,
          //     onChanged: (newValue) {
          //       setState(() {
          //         _isOfflineModeEnabled = newValue;
          //       });
          //     },
          //     activeColor: const Color(0xFFAB2181),
          //     inactiveThumbColor: Colors.grey,
          //     activeTrackColor: const Color(0xFFD590C0),
          //     inactiveTrackColor: Colors.grey.withOpacity(0.5),
          //   ),
          //   selectedTileColor: Colors.grey.withOpacity(0.5),
          // ),
          ListTile(
            title: Text('Data & Support'),
          ),
          ListTile(
            tileColor: Colors.grey.withOpacity(0.1),
            leading: Icon(Icons.clear),
            title: Text('Clear Data'),
            subtitle: Text('Delete all local data'),
            onTap: () {
              // Tambahkan fungsi yang ingin dijalankan saat item diklik
              showMyDialog(context);
            },
            selectedTileColor: Colors.grey.withOpacity(0.5),
          ),
          // ListTile(
          //   title: Text('Actions'),
          // ),
          // ListTile(
          //   tileColor: Colors.grey.withOpacity(0.1),
          //   leading: Icon(Icons.report),
          //   title: Text('Report a problem'),
          //   subtitle: Text('Report any issues or feedback'),
          //   onTap: () {
          //     // Tambahkan fungsi yang ingin dijalankan saat item diklik
          //   },
          //   selectedTileColor: Colors.grey.withOpacity(0.5),
          // ),
          // ListTile(
          //   tileColor: Colors.grey.withOpacity(0.1),
          //   leading: Icon(Icons.sync),
          //   title: Text('Synchronize'),
          //   subtitle: Text('Sync data with server'),
          //   onTap: () {
          //     showDialogWithLoading(context);
          //     // Tambahkan fungsi yang ingin dijalankan saat item diklik
          //   },
          //   selectedTileColor: Colors.grey.withOpacity(0.5),
          // ),
          // ListTile(
          //   tileColor: Colors.grey.withOpacity(0.1),
          //   leading: Icon(Icons.exit_to_app),
          //   title: Text('Log out'),
          //   subtitle: Text('Sign out of your account'),
          //   onTap: () {
          //     // Tambahkan fungsi yang ingin dijalankan saat item diklik
          //   },
          //   selectedTileColor: Colors.grey.withOpacity(0.5),
          // )
        ],
      ),
    );
  }
}

// Ini widget untuk synchronize //
Widget contentBoxSynchronization() {
  return Container(
    width: 200,
    height: 200,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    decoration: BoxDecoration(
      shape: BoxShape.rectangle,
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CircularProgressIndicator(
          color: const Color(0xFFAB2180),
        ),
        SizedBox(height: 20),
        Text(
          "The Synchronization process begins",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    ),
  );
}

// Ini widget untuk Clear Data //
Widget contentBoxClear() {
  return Container(
    width: 200,
    height: 200,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    decoration: BoxDecoration(
      shape: BoxShape.rectangle,
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CircularProgressIndicator(
          color: const Color(0xFFAB2180),
        ),
        SizedBox(height: 20),
        Text(
          "Data Clearance in Progress..",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    ),
  );
}

// Ini widget untuk synchronize //
Widget contentBoxSynchronizationEmergency() {
  return Container(
    width: 200,
    height: 200,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    decoration: BoxDecoration(
      shape: BoxShape.rectangle,
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CircularProgressIndicator(),
        SizedBox(height: 20),
        Text(
          "The Synchronization process begins...",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    ),
  );
}
