package com.example.apk

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbConstants
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbEndpoint
import android.hardware.usb.UsbInterface
import android.hardware.usb.UsbManager
import android.widget.Toast
import android.os.Handler

class MyJavaFunction {
    companion object {

        private lateinit var mUsbManager: UsbManager
        private lateinit var mDevice: UsbDevice
        private lateinit var mConnection: UsbDeviceConnection
        private lateinit var mInterface: UsbInterface
        private lateinit var mEndPoint: UsbEndpoint
        private lateinit var mPermissionIntent: PendingIntent
        private const val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"
        private const val forceCLaim = true
        private const val DESIRED_DEVICE_NAME = "/dev/bus/usb/002/001" // Ganti dengan Device Name yang sesuai

        @JvmStatic
        fun printText(context: Context, rfid: String?, store: String?, museName: String?, dateTime: String?, museNik: String?, orderId: String?): String {
            // Inisialisasi USB manager
            val mUsbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
            
            // Mendapatkan daftar perangkat USB
            val mDeviceList = mUsbManager.deviceList

            if (mDeviceList.isNotEmpty()) {
                // Memeriksa apakah perangkat printer ada dalam daftar
                val desiredDevice = mDeviceList.values.find { it.productName == "Printer" }
                if (desiredDevice != null) {

                    val permissionIntent = PendingIntent.getBroadcast(
                        context,
                        0,
                        Intent(ACTION_USB_PERMISSION),
                        PendingIntent.FLAG_MUTABLE
                    )
                    val filter = IntentFilter(ACTION_USB_PERMISSION)
                    context.registerReceiver(usbReceiver, filter)
                    mUsbManager.requestPermission(desiredDevice, permissionIntent)

                    val usbDevice = mUsbManager.deviceList.values.firstOrNull()
                    usbDevice?.let {
                        val usbDeviceConnection: UsbDeviceConnection? = mUsbManager.openDevice(it)
                        usbDeviceConnection?.apply {
                            val usbInterface = it.interfaceCount
                            val mInterface = it.getInterface(0)
                            if (!claimInterface(mInterface, true)) {
                                Toast.makeText(context, "Failed to claim interface", Toast.LENGTH_SHORT).show()
                                 return "Hello from Kotlin!"
                            }
                            val mEndPoint = mInterface.getEndpoint(0)
                            // val textToPrint = "Hello, this is a test print from your Android device!"

                            // Format pesan yang akan dicetak
                            val textToPrint = ("AEON Canteen Meal Ticket\r\n"
                                            + "Store Name: " + store + "\r\n"
                                            + "-----------------------------------\r\n"
                                            + "Transaction ID   : " + orderId +"\r\n"
                                            + "Employee ID      : " + museNik + "\r\n" 
                                            + "Employee Name    : " + museName + "\r\n"
                                            + "-----------------------------------\r\n"
                                            + "Print Time       : " + dateTime + "\r\n")

                            val data = textToPrint.toByteArray(Charsets.UTF_8)
                            val bytesSent = bulkTransfer(mEndPoint, data, data.size, 5000)
                            if (bytesSent > 0) {
                                // Print successful
                                Toast.makeText(context, "Print successful", Toast.LENGTH_SHORT).show()
                            } else {
                                // Failed to send data to printer
                                Toast.makeText(context, "Failed to print", Toast.LENGTH_SHORT).show()
                            }

                            val rollUpCommand = byteArrayOf(0x1B, 0x64, 0x07) // 7 dots
                            bulkTransfer(mEndPoint, rollUpCommand, rollUpCommand.size, 5000)

                            val cutCommand = byteArrayOf(0x1B, 0x69)
                            bulkTransfer(mEndPoint, cutCommand, cutCommand.size, 5000)                        

                            releaseInterface(mInterface)
                            close()
                        } ?: run {
                            Toast.makeText(context, "Permission denied to access USB device", Toast.LENGTH_SHORT).show()
                        }
                    } ?: run {
                        Toast.makeText(context, "No USB printer connected", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    // Menampilkan pesan jika perangkat printer tidak ditemukan
                    Toast.makeText(context, "Desired USB printer not found", Toast.LENGTH_SHORT).show()
                }    
            } else {
                // Menampilkan pesan jika tidak ada perangkat USB terhubung
                Toast.makeText(context, "Please attach printer via USB", Toast.LENGTH_SHORT).show()
            }
            return "Hello from Kotlin!"
        }  


        @JvmStatic
        fun sayHello(context: Context): String {
            // Inisialisasi USB manager
            val mUsbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
            
            // Mendapatkan daftar perangkat USB
            val mDeviceList = mUsbManager.deviceList

            if (mDeviceList.isNotEmpty()) {
                // Memeriksa apakah perangkat printer ada dalam daftar
                val desiredDevice = mDeviceList.values.find { it.productName == "Printer" }
                if (desiredDevice != null) {

                    val permissionIntent = PendingIntent.getBroadcast(
                        context,
                        0,
                        Intent(ACTION_USB_PERMISSION),
                        PendingIntent.FLAG_MUTABLE
                    )
                    val filter = IntentFilter(ACTION_USB_PERMISSION)
                    context.registerReceiver(usbReceiver, filter)
                    mUsbManager.requestPermission(desiredDevice, permissionIntent)

                    val usbDevice = mUsbManager.deviceList.values.firstOrNull()
                    usbDevice?.let {
                        val usbDeviceConnection: UsbDeviceConnection? = mUsbManager.openDevice(it)
                        usbDeviceConnection?.apply {
                            val usbInterface = it.interfaceCount
                            val mInterface = it.getInterface(0)
                            if (!claimInterface(mInterface, true)) {
                                Toast.makeText(context, "Failed to claim interface", Toast.LENGTH_SHORT).show()
                                return "Hello from Kotlin!"
                            }

                            releaseInterface(mInterface)
                            close()
                        } ?: run {
                            Toast.makeText(context, "Permission denied to access USB device", Toast.LENGTH_SHORT).show()
                        }
                    } ?: run {
                        Toast.makeText(context, "No USB printer connected", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    // Menampilkan pesan jika perangkat printer tidak ditemukan
                    Toast.makeText(context, "Desired USB printer not found", Toast.LENGTH_SHORT).show()
                }    
            } else {
                // Menampilkan pesan jika tidak ada perangkat USB terhubung
                Toast.makeText(context, "Please attach printer via USB", Toast.LENGTH_SHORT).show()
            }
            return "Hello from Kotlin!"
        }



        private val usbReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let {
                    if (it.action == ACTION_USB_PERMISSION) {
                        println("kesini 1111")
                        val granted: Boolean = it.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
                        if (granted) {
                            println("kesini")
                            // Permission granted, you can access USB device
                            val usbDevice = mUsbManager.deviceList.values.firstOrNull()
                            usbDevice?.let {
                                println("kesini 4444444")
                                val usbDeviceConnection: UsbDeviceConnection? = mUsbManager.openDevice(it)
                                usbDeviceConnection?.apply {
                                    mInterface = it.getInterface(0)
                                    if (!claimInterface(mInterface, true)) {
                                        Toast.makeText(context, "Failed to claim interface", Toast.LENGTH_SHORT).show()
                                        return
                                    }
                                    mEndPoint = mInterface.getEndpoint(6)

                                    val textToPrint = "Tersss"
                                    val data = textToPrint.toByteArray(Charsets.US_ASCII)
                                    val bytesSent = bulkTransfer(mEndPoint, data, data.size, 5000)
                                    if (bytesSent > 0) {
                                        // Print successful
                                        Toast.makeText(context, "Print successful", Toast.LENGTH_SHORT).show()
                                    } else {
                                        // Failed to send data to printer
                                        Toast.makeText(context, "Failed to print", Toast.LENGTH_SHORT).show()
                                    }

                                    releaseInterface(mInterface)
                                    close()
                                } ?: run {
                                    Toast.makeText(context, "Permission denied to access USB device", Toast.LENGTH_SHORT).show()
                                }
                            } ?: run {
                                Toast.makeText(context, "No USB printer connected", Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            // Permission denied, handle accordingly
                            Toast.makeText(context, "Permission denied to access USB device", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

    }
}
