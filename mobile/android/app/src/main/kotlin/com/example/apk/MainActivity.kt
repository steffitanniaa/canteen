package com.example.apk // This should match the directory structure where MainActivity.kt is located

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

// class MainActivity : FlutterActivity() {
//     private val CHANNEL = "com.example.apk/java_channel"

//     override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
//         super.configureFlutterEngine(flutterEngine)
//         MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
//             if (call.method == "getJavaMessage") {
//                 val message = MyJavaFunction.sayHello(applicationContext)
//                 result.success(message)
//             } else if (call.method == "print") {
//                 val message = MyJavaFunction.printText(applicationContext)
//                 result.success(message) // Berikan respons ke Flutter
//             }
//             else {
//                 result.notImplemented()
//             }
//         }
//     }
// }

class MainActivity : FlutterActivity() {
    private val CHANNEL = "com.example.apk/java_channel"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            when (call.method) {
                "getJavaMessage" -> {
                    val message = MyJavaFunction.sayHello(applicationContext)
                    result.success(message)
                }
                "print" -> {
                    val rfid = call.argument<String>("rfid")
                    val store = call.argument<String>("store")
                    val museName = call.argument<String>("museName")
                    val dateTime = call.argument<String>("dateTime")
                    val museNik = call.argument<String>("museNik")
                    val orderId = call.argument<String>("orderId")
                                                       
                    val message = MyJavaFunction.printText(applicationContext, rfid, store, museName, dateTime, museNik, orderId)
                    result.success(message) // Berikan respons ke Flutter
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }
}